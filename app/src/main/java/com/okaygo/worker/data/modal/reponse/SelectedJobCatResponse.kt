package com.okaygo.worker.data.modal.reponse

data class SelectedJobCatResponse(
    val code: Int?,
    val message: String?,
    val response: JobCatResponse?
)
