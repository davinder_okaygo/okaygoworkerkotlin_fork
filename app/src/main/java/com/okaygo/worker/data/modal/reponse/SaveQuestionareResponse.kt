package com.okaygo.worker.data.modal.reponse

data class SaveQuestionareResponse(
    val code: Int?,
    val message: String?,
    val response: ArrayList<QuestionareResponseItem>?
)