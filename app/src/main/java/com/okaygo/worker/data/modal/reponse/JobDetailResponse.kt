package com.okaygo.worker.data.modal.reponse

data class JobDetailResponse(
    val code: Int?,
    val message: String?,
    val response: DetailResponse?
)
