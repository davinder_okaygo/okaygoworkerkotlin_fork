package com.okaygo.worker.data.modal.reponse

data class AddExpResponse(
    val code: Int?,
    val message: String?,
    val response: AddExp?
)
