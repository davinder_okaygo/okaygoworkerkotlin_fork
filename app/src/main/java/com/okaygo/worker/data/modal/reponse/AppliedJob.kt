package com.okaygo.worker.data.modal.reponse

import android.os.Parcel
import android.os.Parcelable

data class AppliedJob(
    val acceptedBy: Int?,
    val actualScore: Double?,
    val additional_requirement: String?,
    val address: String?,
    val amount: Double?,
    val amountPer: String?,
    val assignId: Int?,
    val availabilityCutOff: Int?,
    val brandName: String?,
    val breachCutOff: Int?,
    val checkInId: Int?,
    val city: String?,
    val company: String?,
    val confirmedBy: Int?,
    val contactCandidateName: String?,
    val contactCandidateNumber: String?,
    val contactNum: String?,
    val contactPerson: String?,
    val cutoffScore: Double?,
    val distanceCutOff: Int?,
    val employee_hired: String?,
    val employee_required: String?,
    val employerId: Int?,
    val employerUserId: Int?,
    val endDate: String?,
    val expCutOff: Int?,
    val feedbackStatus: String?,
    val from_date: Long?,
    val genderCutOff: Int?,
    val insertedBy: Int?,
    val insertedOn: String?,
    val instructionsCandidate: String?,
    val interviewEndTime: String?,
    val interviewId: Int?,
    val interviewProcess: String?,
    val interviewSlot: Int?,
    val interviewStartTime: String?,
    val interviewStatus: Int?,
    val interview_mode: Int?,
    val isAccepted: Int?,
    val isConfirmed: Int?,
    val isJobOffered: Int?,
    val isOfferAccepted: Int?,
    val isRejected: Int?,
    val is_interview_otp_verified: Int?,
    val is_od_otp_verified: Int?,
    val jobDetailId: Int?,
    val jobId: Int?,
    val jobReqCutOff: Int?,
    val jobTitle: String?,
    val jobType: String?,
    val job_offered_amount: Int?,
    val joiningSpecialRequirement: String?,
    val joiningStatus: String?,
    val joining_date: String?,
    val landmark: String?,
    val locationLat: String?,
    val locationLong: String?,
    val loginTime: String?,
    val logoutTime: String?,
    val negativeRatingCutOff: Int?,
    val od_is_no_show: Int?,
    val posted_on: String?,
    val pt_ft_is_no_show: Int?,
    val qualCutOff: Int?,
    val rejectedBy: Int?,
    val resultAwaited: Int?,
    val startDate: String?,
    val status: String?,
    val to_date: Long?,
    val updatedBy: Int?,
    val updatedOn: String?,
    val userId: Int?,
    val workType: String?,
    val workdaysCount: Int?,
    val workdays_details: String?,
    val workerId: Int?,
    val workerUserId: Int?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(acceptedBy)
        parcel.writeValue(actualScore)
        parcel.writeString(additional_requirement)
        parcel.writeString(address)
        parcel.writeValue(amount)
        parcel.writeString(amountPer)
        parcel.writeValue(assignId)
        parcel.writeValue(availabilityCutOff)
        parcel.writeString(brandName)
        parcel.writeValue(breachCutOff)
        parcel.writeValue(checkInId)
        parcel.writeString(city)
        parcel.writeString(company)
        parcel.writeValue(confirmedBy)
        parcel.writeString(contactCandidateName)
        parcel.writeString(contactCandidateNumber)
        parcel.writeString(contactNum)
        parcel.writeString(contactPerson)
        parcel.writeValue(cutoffScore)
        parcel.writeValue(distanceCutOff)
        parcel.writeString(employee_hired)
        parcel.writeString(employee_required)
        parcel.writeValue(employerId)
        parcel.writeValue(employerUserId)
        parcel.writeString(endDate)
        parcel.writeValue(expCutOff)
        parcel.writeString(feedbackStatus)
        parcel.writeValue(from_date)
        parcel.writeValue(genderCutOff)
        parcel.writeValue(insertedBy)
        parcel.writeString(insertedOn)
        parcel.writeString(instructionsCandidate)
        parcel.writeString(interviewEndTime)
        parcel.writeValue(interviewId)
        parcel.writeString(interviewProcess)
        parcel.writeValue(interviewSlot)
        parcel.writeString(interviewStartTime)
        parcel.writeValue(interviewStatus)
        parcel.writeValue(interview_mode)
        parcel.writeValue(isAccepted)
        parcel.writeValue(isConfirmed)
        parcel.writeValue(isJobOffered)
        parcel.writeValue(isOfferAccepted)
        parcel.writeValue(isRejected)
        parcel.writeValue(is_interview_otp_verified)
        parcel.writeValue(is_od_otp_verified)
        parcel.writeValue(jobDetailId)
        parcel.writeValue(jobId)
        parcel.writeValue(jobReqCutOff)
        parcel.writeString(jobTitle)
        parcel.writeString(jobType)
        parcel.writeValue(job_offered_amount)
        parcel.writeString(joiningSpecialRequirement)
        parcel.writeString(joiningStatus)
        parcel.writeString(joining_date)
        parcel.writeString(landmark)
        parcel.writeString(locationLat)
        parcel.writeString(locationLong)
        parcel.writeString(loginTime)
        parcel.writeString(logoutTime)
        parcel.writeValue(negativeRatingCutOff)
        parcel.writeValue(od_is_no_show)
        parcel.writeString(posted_on)
        parcel.writeValue(pt_ft_is_no_show)
        parcel.writeValue(qualCutOff)
        parcel.writeValue(rejectedBy)
        parcel.writeValue(resultAwaited)
        parcel.writeString(startDate)
        parcel.writeString(status)
        parcel.writeValue(to_date)
        parcel.writeValue(updatedBy)
        parcel.writeString(updatedOn)
        parcel.writeValue(userId)
        parcel.writeString(workType)
        parcel.writeValue(workdaysCount)
        parcel.writeString(workdays_details)
        parcel.writeValue(workerId)
        parcel.writeValue(workerUserId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AppliedJob> {
        override fun createFromParcel(parcel: Parcel): AppliedJob {
            return AppliedJob(parcel)
        }

        override fun newArray(size: Int): Array<AppliedJob?> {
            return arrayOfNulls(size)
        }
    }
}