package com.okaygo.worker.data.modal.reponse

data class Notifications(
    val eventId: Int?,
    val eventStatus: Int?,
    val eventType: Int?,
    val insertedBy: Int?,
    val insertedOn: String?,
    val isAlert: Int?,
    val isRead: Int?,
    val isTask: Int?,
    val message: String?,
    val notificationId: Int?,
    val pushSent: Int?,
    val title: String?,
    val updatedBy: Int?,
    val updatedOn: String?,
    val userId: Int?
)