package com.okaygo.worker.data.modal.request

data class CreateSessionRequest(
    val device_registration_id: String?,
    val ip_address: String?,
    val mac_address: String?,
    val user_id: Int?,
    val last_seen_long: Double?,
    val last_seen_lat: Double?,
    val app_version: String?,
    val android_version: String?,
    val brand: String?
)