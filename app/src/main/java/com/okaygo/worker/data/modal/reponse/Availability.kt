package com.okaygo.worker.data.modal.reponse

data class Availability(
    val active_status: Int?,
    val availabilityType: Int?,
    val availablityId: Int?,
    val insertedBy: Int?,
    val insertedOn: String?,
    val loginTime: String?,
    val logoutTime: String?,
    val updatedBy: Int?,
    val updatedOn: String?,
    val workdaysDetails: String?,
    val workerId: Int?
)