package com.okaygo.worker.data.modal.reponse

data class Claim(
    val claimAmount: Int?,
    val claimPaymentDate: String?,
    val claimPaymentStatus: Any?,
    val claimStatus: String?,
    val insertedBy: Int?,
    val insertedOn: String?,
    val jobId: Int?,
    val raiseEnquiry: Any?,
    val referBy: Int?,
    val referralClaimId: Int?,
    val updatedBy: Int?,
    val updatedOn: String?,
    val userId: Int?
)