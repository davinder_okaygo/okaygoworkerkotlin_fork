package com.okaygo.worker.data.modal.reponse

import android.os.Parcel
import android.os.Parcelable

data class Youtube(
    var video_id: Int?,
    var file_name: String?,
    var title: String?,
    var desc: String?,
    var thumbnail: String?,
    var url: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(video_id)
        parcel.writeString(file_name)
        parcel.writeString(title)
        parcel.writeString(desc)
        parcel.writeString(thumbnail)
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Youtube> {
        override fun createFromParcel(parcel: Parcel): Youtube {
            return Youtube(parcel)
        }

        override fun newArray(size: Int): Array<Youtube?> {
            return arrayOfNulls(size)
        }
    }
}