package com.okaygo.worker.data.modal.reponse

data class PaymentTransectionResponse(
    val code: Int?,
    val message: String?,
    val response: PResponse?
)

data class PResponse(val content: ArrayList<PaymentResponse>?)