package com.okaygo.worker.data.modal.reponse

data class CliamResponse(
    val code: Int?,
    val message: String?,
    val response: Claim?
)
