package com.okaygo.worker.data.modal

data class DocType(
    var filePath: String?,
    var mimeType: String?,
    var cloudUrl: String?
)