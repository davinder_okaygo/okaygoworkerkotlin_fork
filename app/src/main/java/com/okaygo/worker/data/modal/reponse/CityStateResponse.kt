package com.okaygo.worker.data.modal.reponse

data class CityStateResponse(
    val code: Int?,
    val message: String?,
    val response: ArrayList<CitySearch>?
)

