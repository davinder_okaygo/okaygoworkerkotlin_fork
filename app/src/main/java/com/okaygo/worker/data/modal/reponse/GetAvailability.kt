package com.okaygo.worker.data.modal.reponse

data class GetAvailability(
    val content: ArrayList<Availability>,
    val empty: Boolean?,
    val first: Boolean?,
    val last: Boolean?,
    val number: Int,
    val numberOfElements: Int?,
    val size: Int?,
    val totalElements: Int?,
    val totalPages: Int?
)