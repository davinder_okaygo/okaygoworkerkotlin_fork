package com.okaygo.worker.data.modal.reponse

data class WhatsAppSubscriptionResponse(
    val code: Int?,
    val message: String?,
    val response: WhatsAppResponse?
)


