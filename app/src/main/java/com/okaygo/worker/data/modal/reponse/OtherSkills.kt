package com.okaygo.worker.data.api.response

data class OtherSkills(
    val code: Int?,
    val message: String?,
    val response: SkillResponse?
)