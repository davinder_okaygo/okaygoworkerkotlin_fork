package com.okaygo.worker.data.modal.reponse

data class TaskStatus(
    val availabiliity: Boolean?,
    val cv: Boolean?,
    val experience: Boolean?,
    val photo_id: Boolean?,
    val profile: Boolean?,
    val profile_photo: Boolean?,
    val qualification: Boolean?,
    val skills: Boolean?
)