package com.okaygo.worker.data.modal.reponse

data class JobResponse(
    val hotJobs: ArrayList<JobContent>?,
    val hotJobsCount: Int?,
    val nonHotJobs: ArrayList<JobContent>?,
    val nonHotJobsCount: Int?,
    val odJobs: ArrayList<JobContent>?,
    val odJobsCount: Int?,
    val totalJobs: Int?
)