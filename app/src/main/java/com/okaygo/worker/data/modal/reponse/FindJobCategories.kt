package com.okaygo.worker.data.api.response

data class FindJobCategories(
    val code: Int?,
    val message: String?,
    val response: FindJobCategoriesResponse?
)

