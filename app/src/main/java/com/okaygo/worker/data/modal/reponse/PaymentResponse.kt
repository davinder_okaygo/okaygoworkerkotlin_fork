package com.okaygo.worker.data.modal.reponse

data class PaymentResponse(
    val isWorkerPaid: String?,
    val workType: String?,
    val transaction_unique_id: String?,
    val checkInId: String?,
    val jobTitle: String?,
    val companyName: String?,
    val insertedOn: String?,
    val insertedBy: String?,
    val jobTypeId: String?,
    val brandName: String?,
    val updatedBy: String?,
    val companyLogo: String?,
    val amountPer: String?,
    val updatedOn: String?,
    val jobDetailsId: String?,
    val jobPosition: String?,
    val totalAmount: String?,
    val jobId: String?,
    val invoiceId: String?,
    val userId: Int?

)