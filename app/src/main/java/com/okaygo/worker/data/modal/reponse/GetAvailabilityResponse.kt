package com.okaygo.worker.data.modal.reponse

data class GetAvailabilityResponse(
    val code: Int?,
    val message: String?,
    val response: GetAvailability?
)

