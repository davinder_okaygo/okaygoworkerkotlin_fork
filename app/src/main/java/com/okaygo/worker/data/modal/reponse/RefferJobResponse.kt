package com.okaygo.worker.data.modal.reponse

data class RefferJobResponse(
    val code: Int?,
    val message: String?,
    val response: RefferJob?
)