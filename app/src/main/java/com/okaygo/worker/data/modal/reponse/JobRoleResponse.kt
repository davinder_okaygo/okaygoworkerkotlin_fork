package com.okaygo.worker.data.modal.reponse

data class JobRoleResponse(
    val categorySubType: String?,
    val categoryType: String?,
    val id: Int?,
    val insertedBy: Int?,
    val insertedOn: String?,
    val mapped_category: String?,
    val mapping: String?,
    val other: Any?,
    val status: Int?,
    val typeDesc: String?,
    val typeKey: String?,
    val typeValue: String?,
    val updatedBy: Int?,
    val updatedOn: String
)