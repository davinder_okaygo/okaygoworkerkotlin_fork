package com.okaygo.worker.data.modal.reponse

data class UserDetailResponse(
    val code: Int?,
    val message: String?,
    val response: UResponse?
)

data class UResponse(val content: ArrayList<UserDetail>?)