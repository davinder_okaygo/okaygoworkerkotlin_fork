package com.okaygo.worker.data.modal.reponse

data class ReferralWorkerDataResponse(
    val code: Int?,
    val message: String?,
    val response: ArrayList<ReferralWorkerData>?
)
