package com.okaygo.worker.data.modal.reponse

data class ODLocationResponse(
    val code: Int?,
    val message: String?,
    val response: LocationResponse?
)
