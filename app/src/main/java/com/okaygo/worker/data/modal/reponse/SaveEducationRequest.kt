package com.okaygo.worker.data.modal.reponse

data class SaveEducationRequest(
    val requested_by: Int?,
    val qualification_id: Int?,
    val english_known_level: Int?,
    val worker_id: Int?
)