package com.okaygo.worker.data.modal.reponse

data class CategoryResponse(
    val content: ArrayList<CategoryContent>?,
    val empty: Boolean?,
    val first: Boolean?,
    val last: Boolean?,
    val number: Int?,
    val numberOfElements: Int?,
    val size: Int?,
    val totalElements: Int?,
    val totalPages: Int?
)

