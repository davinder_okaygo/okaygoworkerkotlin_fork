package com.okaygo.worker.data.modal.request

data class UserDetailRequest(
    val new_city_id: String?,
    val date_of_birth: String?,
    val first_name: String?,
    val last_name: String?,
    val gender: String?,
    val user_id: String?,
    val email_id: String?,
    val city: String?,
    val profile_photo: String?
)