package com.okaygo.worker.data.modal.reponse

data class ReferralPaymentResponse(
    val code: Int?,
    val message: String?,
    val response: RResponse?
)

data class RResponse(
    val content: ArrayList<PaymentResponse>?,
    val empty: Boolean?,
    val first: Boolean?,
    val last: Boolean?,
    val number: Int?,
    val numberOfElements: Int?,
    val totalElements: Int?,
    val totalPages: Int?
)
//
//data class ReferralPayment(
//    val brandName: String,
//    val companyLogo: String,
//    val companyName: String,
//    val insertedBy: Int,
//    val insertedOn: String,
//    val isWorkerPaid: Int,
//    val jobId: Int,
//    val totalAmount: Int,
//    val updatedBy: Int,
//    val updatedOn: String,
//    val userId: Int
//)
