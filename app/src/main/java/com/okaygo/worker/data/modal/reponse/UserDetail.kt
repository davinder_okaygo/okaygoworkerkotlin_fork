package com.okaygo.worker.data.modal.reponse

data class UserDetail(
    val gender: String?,
    val genderName: String?,
    val userId: Int?,
    val aboutMe: String?,
    val addressLine1: String?,
    val addressLine2: String?,
    val badgeType: Int?,
    val cityId: Int?,
    val countryCode: String?,
    val countryId: Int?,
    val dateOfBirth: String?,
    val emailId: String?,
    val firstName: String?,
    val lastName: String?,
    val phoneNumber: String?,
    val postalCode: String?,
    val profilePhoto: String?,
    val referralCode: String?,
    val referredByCode: String?,
    val roleType: Int?,
    val stateId: Int?,
    val userStatus: Int?,
    val cityName: String?,
    val city: String?,
    val state: String?
)
