package com.okaygo.worker.data.modal.reponse

data class JCount(
    val applied_job_count: Int?,
    val past_job_count: Int?,
    val upcoming_job_count: Int?
)