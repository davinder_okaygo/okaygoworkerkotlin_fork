package com.okaygo.worker.data.modal.request

data class FindJobRequest(
    val user_id: Int?,
    val job_category_id: Int?,
    val filter_od: String?,
    val filter_pt: String?,
    val filter_ft: String?,
    val max_pay: String?,
    val min_pay: String?,
    val job_types: String?,
    val jobs_after: String?,
    val jobs_before: String?,
    val city: String?,
    val experience: String?,
    val page_no: Int?,
    val rows: Int?
)
