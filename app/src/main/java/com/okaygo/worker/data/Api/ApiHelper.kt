package com.openkey.guest.data.Api

import com.okaygo.worker.data.modal.ErrorResponse
import com.openkey.guest.application.OkayGo
import com.openkey.guest.help.utils.Constants
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * @author Davinder Goel.
 */
object ApiHelper {
    //    private const val appToken = "45144534-f181-4011-b142-5d53162a95c8"
    const val TIMEOUT = 60L
    private val mRetrofit: Retrofit
    private val mDirectionRetrofit: Retrofit
    private val mService: WebService
    private val mDirectionService: WebService

    // Creating Retrofit Object
    init {
        mRetrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(getClient())
            .build()
        mService = mRetrofit.create(WebService::class.java)

        mDirectionRetrofit = Retrofit.Builder()
            .baseUrl("https://maps.googleapis.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(getClient())
            .build()
        mDirectionService = mDirectionRetrofit.create(WebService::class.java)
    }

    // Creating OkHttpclient Object
    private fun getClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient().newBuilder().connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(interceptor)
            .addInterceptor(ChuckInterceptor(OkayGo.appContext))
            .build()

    }
//    // Creating OkHttpclient Object
//    private fun getClient(): OkHttpClient {
//
//        val interceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
//        val okHttpClient = OkHttpClient.Builder()
//                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
//                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
//                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
//                .addInterceptor(interceptor)
//
//        okHttpClient.addInterceptor { chain ->
//            val request = chain.request().newBuilder()
//
//            request.addHeader("Accept", "application/json")
//            request.addHeader("cache-control", "no-cache")
//            request.addHeader("x-openkey-app", Constants.APP_UUID)
//            chain.proceed(request.build())
//        }
//
//        return enableTls12OnPreLollipop(okHttpClient).build()
//    }

    fun getService(): WebService {
        return mService
    }

    fun getDirectionService(): WebService {
        return mDirectionService
    }

    //
//    fun enableTls12OnPreLollipop(client: OkHttpClient.Builder): OkHttpClient.Builder {
//        if (Build.VERSION.SDK_INT in 19..21) {
//            try {
//
//                val sc = SSLContext.getInstance("TLSv1.2")
//                sc.init(null, null, null)
//                client.sslSocketFactory(Tls12SocketFactory(sc.socketFactory))
//                val cs = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
//                        .tlsVersions(TlsVersion.TLS_1_2)
//                        .build()
//                client.connectionSpecs(ImmutableList.of(cs))
//
//
//            } catch (exc: Exception) {
//                Log.e("OkHttpTLSCompat", "Error while setting TLS 1.2", exc)
//            }
//
//        }
//
//        return client
//    }
//     Handling error messages returned by Apis
    fun handleApiError(body: ResponseBody?): String {
        val errorConverter: Converter<ResponseBody, ErrorResponse> =
            mRetrofit.responseBodyConverter(ErrorResponse::class.java, arrayOfNulls(0))
        val error: ErrorResponse? = body?.let {
            try {
                errorConverter.convert(it)
            } catch (e: Exception) {
                return "Exception In ErrorConverter"
            }
        }
        return error?.error ?: (error?.message ?: "Internal server error.")
    }
}