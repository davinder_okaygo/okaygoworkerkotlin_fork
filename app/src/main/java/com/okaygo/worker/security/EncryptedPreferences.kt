package com.openkey.guest.security

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.preference.PreferenceManager
import android.provider.Settings
import android.util.Base64
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.security.spec.InvalidKeySpecException
import java.util.*
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec

/**
 * @author Davinder Goel.
 */
class EncryptedPreferences(context: Context?) : SharedPreferences {
    private val secretKey = "0kayG0J0b3"
    private val fileName = "SecretPreferences"
    private val algorithm = "PBKDF2WithHmacSHA1"
    private val transformation = "AES"
    private val sFile: SharedPreferences
    private var sKey: ByteArray? = null

    init {
        sFile = context!!.getSharedPreferences(fileName, Context.MODE_PRIVATE)
        // Initialize encryption/decryption key
        try {
            val key = generateAesKeyName(context)
            var value = sFile.getString(key, null)
            if (value == null) {
                value = generateAesKeyValue()
                sFile.edit()?.putString(key, value)?.apply()
            }
            sKey = decode(value)
        } catch (e: Exception) {
            throw IllegalStateException(e)
        }
    }

    @SuppressLint("HardwareIds")
    @Throws(
        InvalidKeySpecException::class,
        NoSuchAlgorithmException::class,
        IllegalArgumentException::class
    )
    private fun generateAesKeyName(context: Context): String {
        val password = secretKey.toCharArray()
        val salt = Settings.Secure.getString(
            context.contentResolver,
            Settings.Secure.ANDROID_ID
        ).toByteArray()
        // Number of PBKDF2 hardening rounds to use, larger values increase
        // computation time, you should select a value that causes
        // computation to take >100ms
        val iterations = 1000
        // Generate a 256-bit key
        val keyLength = 256
        val spec = PBEKeySpec(password, salt, iterations, keyLength)
        return encode(
            SecretKeyFactory.getInstance(algorithm)
                .generateSecret(spec).encoded
        )
    }

    private fun encode(input: ByteArray): String {
        return Base64.encodeToString(input, Base64.NO_PADDING or Base64.NO_WRAP)
    }

    private fun decode(input: String): ByteArray {
        return Base64.decode(input, Base64.NO_PADDING or Base64.NO_WRAP)
    }

    @Throws(NoSuchAlgorithmException::class)
    private fun generateAesKeyValue(): String {
        // Do *not* seed secureRandom! Automatically seeded from system entropy
        val random = SecureRandom()
        // Use the largest AES key length which is supported by the OS
        val generator = KeyGenerator.getInstance(transformation)
        try {
            generator.init(256, random)
        } catch (e: Exception) {
            try {
                generator.init(192, random)
            } catch (e1: Exception) {
                generator.init(128, random)
            }

        }

        return encode(generator.generateKey().encoded)
    }


    private fun encrypt(cleartext: String): String {
        try {
            val cipher = Cipher.getInstance(transformation)
            cipher.init(Cipher.ENCRYPT_MODE, SecretKeySpec(sKey, transformation))
            return encode(cipher.doFinal(cleartext.toByteArray(charset("UTF-8"))))
        } catch (e: Exception) {
            return cleartext
        }
    }


    @SuppressLint("GetInstance")
    private fun decrypt(ciphertext: String): String {

        try {
            val cipher = Cipher.getInstance(transformation)
            cipher.init(Cipher.DECRYPT_MODE, SecretKeySpec(sKey, transformation))
            return String(cipher.doFinal(decode(ciphertext)), charset("UTF-8"))
        } catch (e: Exception) {
            return ciphertext
        }

    }

    override fun getAll(): Map<String, String> {
        val encryptedMap = sFile.all
        val decryptedMap = HashMap<String, String>(encryptedMap.size)
        for ((key, value) in encryptedMap) {
            try {
                decryptedMap.put(decrypt(key), decrypt(value.toString()))
            } catch (e: Exception) {
                // Ignore unencrypted key/value pairs
            }
        }
        return decryptedMap
    }


    override fun getString(key: String, defaultValue: String?): String? {
        val encryptedValue = sFile.getString(encrypt(key), null)
        return if (encryptedValue != null) decrypt(encryptedValue) else defaultValue
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    override fun getStringSet(key: String, defaultValues: Set<String>?): Set<String>? {
        val encryptedSet = sFile.getStringSet(encrypt(key), null)
            ?: return defaultValues
        val decryptedSet = HashSet<String>(encryptedSet.size)
        for (encryptedValue in encryptedSet) {
            decryptedSet.add(decrypt(encryptedValue))
        }
        return decryptedSet
    }

    override fun getInt(key: String, defaultValue: Int): Int {
        val encryptedValue = sFile.getString(encrypt(key), null)
            ?: return defaultValue
        try {
            val returnValue: String? = decrypt(encryptedValue)
            if (returnValue?.trim()?.equals("") == false) {
                return Integer.parseInt(returnValue)
            } else {
                return defaultValue
            }
        } catch (e: NumberFormatException) {
            throw ClassCastException(e.message)
        }

    }

    override fun getLong(key: String, defaultValue: Long): Long {
        val encryptedValue = sFile.getString(encrypt(key), null)
            ?: return defaultValue
        try {
            return java.lang.Long.parseLong(decrypt(encryptedValue))
        } catch (e: NumberFormatException) {
            throw ClassCastException(e.message)
        }

    }

    override fun getFloat(key: String, defaultValue: Float): Float {
        val encryptedValue = sFile.getString(encrypt(key), null)
            ?: return defaultValue
        try {
            return java.lang.Float.parseFloat(decrypt(encryptedValue))
        } catch (e: NumberFormatException) {
            throw ClassCastException(e.message)
        }

    }

    override fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        val encryptedValue = sFile.getString(encrypt(key), null)
            ?: return defaultValue
        try {
            return java.lang.Boolean.parseBoolean(decrypt(encryptedValue))
        } catch (e: NumberFormatException) {
            throw ClassCastException(e.message)
        }

    }

    override fun contains(key: String): Boolean {
        return sFile.contains(encrypt(key))
    }


    override fun edit(): EncryptedEditor {
        return EncryptedEditor()
    }

    override fun registerOnSharedPreferenceChangeListener(listener: SharedPreferences.OnSharedPreferenceChangeListener) {
        sFile.registerOnSharedPreferenceChangeListener(listener)
    }

    override fun unregisterOnSharedPreferenceChangeListener(listener: SharedPreferences.OnSharedPreferenceChangeListener) {
        sFile.unregisterOnSharedPreferenceChangeListener(listener)
    }


    /**
     * Wrapper for Android's [SharedPreferences.Editor].
     *
     *
     * Used for modifying values in a [SharedPreferencesEncryptionTest] object. All changes you make in an
     * editor are batched, and not copied back to the original [SharedPreferencesEncryptionTest] until you
     * call [.commit] or [.apply].
     */
    inner class EncryptedEditor : SharedPreferences.Editor {
        private val mEditor: SharedPreferences.Editor

        init {
            mEditor = sFile.edit()
        }

        override fun putString(key: String, value: String?): SharedPreferences.Editor {
            mEditor.putString(encrypt(key), encrypt(value ?: ""))
            return this
        }

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        override fun putStringSet(key: String, values: Set<String>?): SharedPreferences.Editor {
            val encryptedValues = HashSet<String>(values!!.size)
            values.mapTo(encryptedValues) { encrypt(it) }
            mEditor.putStringSet(encrypt(key), encryptedValues)
            return this
        }

        override fun putInt(key: String, value: Int): SharedPreferences.Editor {
            mEditor.putString(
                encrypt(key),
                encrypt(Integer.toString(value))
            )
            return this
        }

        override fun putLong(key: String, value: Long): SharedPreferences.Editor {
            mEditor.putString(
                encrypt(key),
                encrypt(java.lang.Long.toString(value))
            )
            return this
        }

        override fun putFloat(key: String, value: Float): SharedPreferences.Editor {
            mEditor.putString(
                encrypt(key),
                encrypt(java.lang.Float.toString(value))
            )
            return this
        }

        override fun putBoolean(key: String, value: Boolean): SharedPreferences.Editor {
            mEditor.putString(
                encrypt(key),
                encrypt(java.lang.Boolean.toString(value))
            )
            return this
        }

        override fun remove(key: String): SharedPreferences.Editor {
            mEditor.remove(encrypt(key))
            return this
        }

        override fun clear(): SharedPreferences.Editor {
            mEditor.clear()
            return this
        }

        override fun commit(): Boolean {
            return mEditor.commit()
        }

        @TargetApi(Build.VERSION_CODES.GINGERBREAD)
        override fun apply() {
            mEditor.apply()
        }
    }

    fun <T> saveValue(key: String, value: T) {
        val editor = this.edit()
        when (value) {
            is String -> edit { it.putString(key, value) }
            is Int -> edit { it.putInt(key, value) }
            is Long -> edit { it.putLong(key, value) }
            is Float -> edit { it.putFloat(key, value) }
            is Boolean -> edit { it.putBoolean(key, value) }
        }
        editor.commit()
    }


    /* this is used for save country list of aupair on filter*/
    fun saveArrayList(list: ArrayList<String?>?, key: String, context: Context) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = prefs.edit()
        val gson = Gson()
        val json = gson.toJson(list)
        editor.putString(key, json)
        editor.apply()     // This line is IMPORTANT !!!
    }


    fun clearArrayListKey(key: String, context: Context) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = prefs.edit()
        editor.putString(key, "")
        editor.apply()     // This line is IMPORTANT !!!
    }


    /* this is used for get country list of aupair on filter*/
    fun getArrayList(key: String, context: Context): ArrayList<String>? {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val gson = Gson()
        val json = prefs.getString(key, null)
        val type = object : TypeToken<ArrayList<String>>() {

        }.type
        return gson.fromJson<ArrayList<String>>(json, type)
    }


    fun clearAllValue() {
        val editor = this.edit()
        editor.clear()
        editor.apply()
    }

    fun clearValue(key: String) {
        val editor = this.edit()
        editor.putString(key, "")
        editor.commit()
    }

    fun clearValueInt(key: String) {
        val editor = this.edit()
        editor.putInt(key, 0)
        editor.apply()
    }

    /**
     * Generic Extension function to get values from the SharedPreferences
     */
    inline fun <reified T : Any> getValue(key: String, defaultValue: T?): T? {
        return when (T::class) {
            String::class -> getString(key, defaultValue as String) as T?
            Int::class -> getInt(key, defaultValue as? Int ?: -1) as T?
            Boolean::class -> getBoolean(key, defaultValue as? Boolean ?: false) as T?
            Float::class -> getFloat(key, defaultValue as? Float ?: -1f) as T?
            Long::class -> getLong(key, defaultValue as? Long ?: -1) as T?
            else -> defaultValue
        }
    }

    /*High order Inline function to store values inside the shared preferences*/
    inline fun edit(operation: (EncryptedPreferences.EncryptedEditor) -> Unit) {
        val editor = this.edit()
        operation(editor)
        editor.apply()
    }
}