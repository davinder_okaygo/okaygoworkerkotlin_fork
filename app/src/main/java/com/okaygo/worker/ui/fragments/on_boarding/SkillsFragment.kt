package com.okaygo.worker.ui.fragments.on_boarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.chip.Chip
import com.okaygo.worker.R
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.api.response.SkillResponse
import com.okaygo.worker.data.api.response.UserSkills
import com.okaygo.worker.data.modal.request.AddSkillsRequest
import com.okaygo.worker.data.modal.request.Skills
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.OnBoardingModel
import kotlinx.android.synthetic.main.fragment_skills.*

class SkillsFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModel: OnBoardingModel
    private var mSelectedCat: String? = null
    private var mUserSkills: ArrayList<Int>? = null

    //    private var isFromProfle: Boolean? = false
    private var mOtherSkills: ArrayList<SkillResponse>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(OnBoardingModel::class.java)
        attachObservers()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_skills, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        var intent: Intent? = activity?.intent
//        if (intent?.hasExtra(Constants.IS_FROM_PROFILE) == true) {
//            isFromProfle = intent?.getBooleanExtra(Constants.IS_FROM_PROFILE, false)
        if (Constants.IS_FROM_PROFILE == true) {
            lnrBottomLayout?.visibility = View.GONE
            linearBottomBtns?.visibility = View.GONE
            imgBackIcon?.visibility = View.VISIBLE
            txtEdit?.visibility = View.VISIBLE
        }
//        }
        if (Utilities.isOnline(context) == false) {
            Utilities.showToast(activity, "No Internet")
        } else {
            viewModel.getSelectedJobCat(userId, true)
        }
        mOtherSkills = ArrayList()
        setListeners()
    }

    private fun setListeners() {
        txtChipOther?.setOnClickListener(this)
        imgBack?.setOnClickListener(this)
        btnSaveContinue?.setOnClickListener(this)
        txtDiscard?.setOnClickListener(this)
        imgBackIcon?.setOnClickListener(this)
        txtAddSkill?.setOnClickListener(this)
        txtSave?.setOnClickListener(this)
        txtEdit?.setOnClickListener(this)
    }

    /**
     * handle api reposne
     */
    private fun attachObservers() {

        viewModel.responseSelectedJobCat.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    mSelectedCat = it.response?.content?.get(0)?.interested_cat ?: ""
                    viewModel.getWorkerExp(workerId, false)
                }
            }
        })

        viewModel.responseGetExp.observe(this, Observer {
            it?.let {

                if (it.code == Constants.SUCCESS) {
                    if (it.response?.content?.size ?: 0 > 0) {
                        for (i in 0..it.response?.content?.size?.minus(1)!!) {
                            if (mSelectedCat?.isEmpty() == true) {
                                mSelectedCat =
                                    it.response?.content?.get(i)?.industryType?.toString()
                            } else {
                                mSelectedCat =
                                    "$mSelectedCat,${it.response?.content?.get(i)?.industryType?.toString()}"
                            }
                        }
                    }

                    if (Constants.IS_FROM_PROFILE_SKILLS == true) {
                        viewModel.getUserSkills(userId, false)
                    } else {
                        viewModel.getAllSkills(mSelectedCat, mSelectedCat, false)
                    }
                }
            }
        })

        viewModel.responseUserSkills.observe(this, Observer {
            it?.let {

                if (it.code == Constants.SUCCESS) {
                    if (mUserSkills == null) {
                        mUserSkills = ArrayList()
                    } else {
                        mUserSkills = ArrayList()
                    }
                    chgSkills?.removeAllViews()
                    cpgOthers?.removeAllViews()
                    edtOtherJobType?.setText("")
                    if (it.response?.size ?: 0 > 0) {
                        for (i in 0..(it.response?.size ?: 0) - 1) {
                            mUserSkills?.add(it.response?.get(i)?.skillsTypeId ?: 0)
                            setChips(null, it.response?.get(i), true)
                        }
                    }
                    viewModel.getAllSkills(mSelectedCat, mSelectedCat, false)
                }
            }
        })

        viewModel.responseAllSkills.observe(this, Observer {
            it?.let {

                if (it.code == Constants.SUCCESS) {
                    if (it.response?.size ?: 0 > 0) {
                        for (i in 0..(it.response?.size ?: 0) - 1) {
                            setChips(it.response?.get(i), null, false)
                        }
                    }
                }
            }
        })

        viewModel.responseAddSkills.observe(this, Observer {
            it?.let {

                if (it.code == Constants.SUCCESS) {
                    it.response?.let { it1 -> mOtherSkills?.add(it1) }
                    cpgOthers?.removeAllViews()
                    edtOtherJobType?.setText("")
                    if (mOtherSkills?.size ?: 0 > 0) {
                        for (i in 0..(mOtherSkills?.size ?: 0) - 1) {

                            val tagName: String = mOtherSkills?.get(i)?.skills_name
                                ?: ""
                            val chip = Chip(activity)

                            chip.isCheckable = true
                            chip.isCheckedIconVisible = false

                            chip.text = tagName
                            chip.id = mOtherSkills?.get(i)?.skills_type_id ?: 0
                            chip.setChipBackgroundColorResource(R.color.bg_chips_drawable)
                            chip.setChipStrokeColorResource(R.color.theme)
                            chip.chipStrokeWidth = 1f
//                                        chip.isClickable = true
                            chip.isChipIconVisible = true

                            chip.isCloseIconVisible = true
                            chip.isCheckedIconVisible = false

                            //Added click listener on close icon to remove tag from ChipGroup
                            chip.setOnCloseIconClickListener {
                                if (mOtherSkills?.size ?: 0 > i) {
                                    mOtherSkills?.removeAt(i)
                                }
                                cpgOthers.removeView(chip)
                            }

                            cpgOthers?.addView(chip)

                        }
                    }
                }
            }
        })

        viewModel.responseSaveSkills.observe(this, Observer {
            it?.let {

                if (it.code == Constants.SUCCESS) {
                    if (Constants.IS_FROM_PROFILE_SKILLS == true) {
                        linearBottomBtns?.visibility = View.GONE
                        layoutOtherType?.visibility = View.GONE
                        txtEdit?.visibility = View.VISIBLE
                        for (i in 0 until chgSkills?.getChildCount()!!) {
                            chgSkills?.getChildAt(i)?.setEnabled(false)
                        }
                        for (i in 0 until cpgOthers?.getChildCount()!!) {
                            cpgOthers?.getChildAt(i)?.setEnabled(false)
                        }
                        txtChipOther?.isEnabled = false
                        viewModel.getUserSkills(userId)
                    } else {
                        OkayGoFirebaseAnalytics.on_boarding_skills_details()
                        attachFragment(AvailabilityFragment(), false)
                    }

                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    private fun setChips(
        response: SkillResponse?,
        userResponse: UserSkills?,
        isUserSkills: Boolean
    ) {
        var chip: Chip? = null
        if (isUserSkills == false) {
            val tagName: String = response?.skills_name
                ?: ""
            chip = Chip(activity)

            chip.isCheckable = true
            chip.isCheckedIconVisible = false

            chip.text = tagName
            chip.id = response?.skills_type_id
                ?: 0
        } else {
            val tagName: String = userResponse?.skillsName
                ?: ""
            chip = Chip(activity)

            chip.isCheckable = true
            chip.isCheckedIconVisible = false

            chip.text = tagName
            chip.id = userResponse?.skillsTypeId
                ?: 0
            chip.isChecked = true
        }
        if (Constants.IS_FROM_PROFILE_SKILLS == true) {
            chip.isEnabled = false
            txtChipOther?.isEnabled = false
        }
        chip?.setChipBackgroundColorResource(R.color.bg_chips_drawable)
        chip?.setChipStrokeColorResource(R.color.theme)
        chip?.chipStrokeWidth = 1f
        chip?.isClickable = true
        chip?.isChipIconVisible = true

        chip?.isCloseIconVisible = false
        chip?.isCheckedIconVisible = false

        chgSkills?.addView(chip)
        if (isUserSkills == false) {
            if (mUserSkills?.isNotEmpty() == true && mUserSkills?.contains(response?.skills_type_id) == true) {
                chgSkills?.removeView(chip)
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtChipOther -> {
                mOtherSkills?.clear()
                layoutOtherType?.visibility = View.VISIBLE
            }
            R.id.txtAddSkill -> {
                if (!Utilities.isOnline(activity)) {
                    Utilities.showToast(activity, resources.getString(R.string.no_internet))
                    return
                }
                if (edtOtherJobType?.text?.toString()?.trim()
                        ?.isEmpty() == false && mOtherSkills?.size ?: 0 < 3
                ) {
                    viewModel.addSkills(userId, edtOtherJobType?.text?.toString()?.trim())
                } else if (edtOtherJobType?.text?.toString()?.trim()?.isEmpty() == true) {
                    edtOtherJobType?.setError("Enter skill name first.")
                } else {
                    edtOtherJobType?.setError("You can add maximum 3 other skills.")
                }
            }
            R.id.txtSave,
            R.id.btnSaveContinue -> {
                saveSkills()
            }
            R.id.txtDiscard -> {
                layoutOtherType?.visibility = View.GONE
                linearBottomBtns?.visibility = View.GONE
                txtEdit?.visibility = View.VISIBLE
                txtChipOther?.setEnabled(false)
                for (i in 0 until chgSkills?.getChildCount()!!) {
                    chgSkills?.getChildAt(i)?.setEnabled(false)
                }
                for (i in 0 until cpgOthers?.getChildCount()!!) {
                    cpgOthers?.getChildAt(i)?.setEnabled(false)
                }

                cpgOthers?.removeAllViews()
                edtOtherJobType?.setText("")
            }

            R.id.imgBack,
            R.id.imgBackIcon -> activity?.onBackPressed()

            R.id.txtEdit -> {
                linearBottomBtns?.visibility = View.VISIBLE
                txtEdit?.visibility = View.GONE
                chgSkills?.setEnabled(true)
                cpgOthers?.setEnabled(true)
                txtChipOther?.setEnabled(true)
                for (i in 0 until chgSkills?.getChildCount()!!) {
                    chgSkills?.getChildAt(i)?.setEnabled(true)
                }
                for (i in 0 until cpgOthers?.getChildCount()!!) {
                    cpgOthers?.getChildAt(i)?.setEnabled(true)
                }
            }
        }
    }

    private fun saveSkills() {
        if (checkSelectedSkillCount()) {
            val request: AddSkillsRequest? = AddSkillsRequest()
            var skills: Skills

            for (i in 0 until chgSkills?.getChildCount()!!) {
                val chip: Chip? = chgSkills?.findViewById(
                    chgSkills?.getChildAt(i)?.getId()
                        ?: 0
                )
                if (chip?.isChecked == true) {
                    skills = Skills(chip?.text.toString(), chip?.id, userId)
                    request?.add(skills)
                }
            }

            for (i in 0 until mOtherSkills?.size!!) {
                skills = Skills(
                    mOtherSkills?.get(i)?.skills_name?.toString(),
                    mOtherSkills?.get(i)?.skills_type_id,
                    userId
                )
                request?.add(skills)
            }

            viewModel.saveSkills(userId, request)

        }
    }

    private fun checkSelectedSkillCount(): Boolean {
        val tempList: ArrayList<String>? = ArrayList()

        for (i in 0 until (chgSkills?.getChildCount() ?: 0)) {
            val chip: Chip = chgSkills.findViewById(chgSkills.getChildAt(i).getId())
            if (chip.isChecked) {
                tempList?.add(chip.id.toString() + "")
            }
        }

        for (i in 0 until (mOtherSkills?.size ?: 0)) {
            tempList?.add(mOtherSkills?.get(i)?.skills_type_id?.toString() + "")
        }


        if (Utilities.isOnline(activity) == false) {
            Toast.makeText(activity, "No Internet", Toast.LENGTH_SHORT).show()
        } else
            if (tempList?.size ?: 0 > 3) {
                Toast.makeText(activity, "You can select maximum 3 job skills.", Toast.LENGTH_SHORT)
                    .show()
                return false

            } else if (tempList?.size == 0) {
                Toast.makeText(activity, "Select atlest one job category.", Toast.LENGTH_SHORT)
                    .show()
                return false
            }
        return true
    }
}