package com.okaygo.worker.ui.fragments.job_detail

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import com.okaygo.worker.R
import com.okaygo.worker.adapters.SliderImageAdapter
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.ApplyJobEventDeepLink
import com.okaygo.worker.data.modal.DocType
import com.okaygo.worker.data.modal.reponse.DetailContent
import com.okaygo.worker.data.modal.reponse.JobContent
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.Preferences
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.JobDetailModel
import com.openkey.guest.ui.fragments.verification.JobListModel
import com.openkey.guest.ui.fragments.verification.ProfileModel
import kotlinx.android.synthetic.main.fragment_job_detail.*
import org.greenrobot.eventbus.EventBus
import java.util.*
import kotlin.collections.ArrayList


class JobDetailFragment : BaseFragment(), View.OnClickListener {
    private var currentPage = 0
    var timer: Timer? = null
    private var mJobItems: JobContent? = null
    private var mJobId: String? = null

    private var mDetailResponse: DetailContent? = null
    private var dots: Array<TextView?>? = null

    //    private var mOkayGoFirebaseAnalytics: OkayGoFirebaseAnalytics? = null
    private var mCuisine = ""
    private var mGenderId = 0
    private var mExpId = 0
    private var mEduId = 0
    private var isFromDeepLink = false
    private var deepLinkAssignId = 0
    private var cvFileName: String? = null
    private var isForDeeplink = false
    var mAlertDialog: AlertDialog? = null
    var mSliderListSize: Int? = 0
    private val sliderRunnable: Runnable = Runnable {
        run {
            if (currentPage == mSliderListSize?.minus(1) ?: 0) {
                currentPage = 0
            }
//            viewPager.setCurrentItem(currentPage++, true);
//            if (currentPage < mSliderListSize?.minus( 1)?:0) {
//                currentPage=currentPage+1
//            }else{
//                currentPage=0
//            }
            viewPagerSlider?.currentItem = currentPage++
        }
    }
    private var handler: Handler? = null
    private fun setListeners() {
        imgBack?.setOnClickListener(this)
        txtApply?.setOnClickListener(this)
        txtRefferEarnJob?.setOnClickListener(this)
    }

    //  viewpager change listener
    private val slidingCallback = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            addBottomDots(position)
        }
    }


    private fun setDays(response: String) {
        val jsonParser = JsonParser()
        val jsonObject = jsonParser.parse(response).asJsonObject
        for (key in jsonObject.keySet()) {
            if (key == "0") {
                selectUnselectDay(txtMonday, true)
                selectUnselectDay(txtTuesday, true)
                selectUnselectDay(txtWednesday, true)
                selectUnselectDay(txtThursday, true)
                selectUnselectDay(txtFriday, true)
                selectUnselectDay(txtSaturday, true)
                selectUnselectDay(txtSunday, true)
            } else if (key == "2") {
                selectUnselectDay(txtMonday, jsonObject[key].asBoolean)
            } else if (key == "3") {
                selectUnselectDay(txtTuesday, jsonObject[key].asBoolean)
            } else if (key == "4") {
                selectUnselectDay(txtWednesday, jsonObject[key].asBoolean)
            } else if (key == "5") {
                selectUnselectDay(txtThursday, jsonObject[key].asBoolean)
            } else if (key == "6") {
                selectUnselectDay(txtFriday, jsonObject[key].asBoolean)
            } else if (key == "7") {
                selectUnselectDay(txtSaturday, jsonObject[key].asBoolean)
            } else if (key == "1") {
                selectUnselectDay(txtSunday, jsonObject[key].asBoolean)
            }
        }
    }

    private fun setDaysForOD(sDate: String, workDays: Int) {
        val day: String? = Utilities.getWeekdayFromDate(sDate)
        val mDays: ArrayList<String>? = Utilities.getNextFourDays(day, workDays)
        selectUnselectDay(txtMonday, false)
        selectUnselectDay(txtThursday, false)
        selectUnselectDay(txtTuesday, false)
        selectUnselectDay(txtWednesday, false)
        selectUnselectDay(txtFriday, false)
        selectUnselectDay(txtSaturday, false)
        selectUnselectDay(txtSunday, false)
        for (i in 0..(mDays?.size ?: 0) - 1) {
            Log.e("DAY", mDays?.get(i) + "")
            when (mDays?.get(i)) {
                "Monday" -> selectUnselectDay(txtMonday, true)
                "Tuesday" -> selectUnselectDay(txtTuesday, true)
                "Wednesday" -> selectUnselectDay(txtWednesday, true)
                "Thursday" -> selectUnselectDay(txtThursday, true)
                "Friday" -> selectUnselectDay(txtFriday, true)
                "Saturday" -> selectUnselectDay(txtSaturday, true)
                "Sunday" -> selectUnselectDay(txtSunday, true)
            }
        }
    }

    private fun selectUnselectDay(
        textView: AppCompatTextView?,
        isSelected: Boolean
    ) {
        if (isSelected) {
            textView?.setBackgroundDrawable(resources.getDrawable(R.drawable.sp_orange_circle))
            textView?.setTextColor(resources.getColor(R.color.white))
        } else {
            textView?.setBackgroundDrawable(resources.getDrawable(R.drawable.sp_grey_circle))
            textView?.setTextColor(resources.getColor(R.color.charcol_black))
        }
    }


    private fun setJobType(jobType: String) {
        txtJobType?.text = jobType
        var colorCode = resources.getColor(R.color.kprogresshud_grey_color)
        when (jobType.toUpperCase()) {
            "ON DEMAND" -> {
                colorCode = resources.getColor(R.color.on_demand_color)
                txtEntryLevel?.visibility = View.GONE
                imgEntryLevel?.visibility = View.GONE
            }
            "PART TIME" -> {
                colorCode = resources.getColor(R.color.part_time_color_new)
                txtDateTitle?.visibility = View.GONE
                txtDate?.visibility = View.GONE
            }
            "FULL TIME" -> {
                txtDateTitle?.visibility = View.GONE
                txtDate?.visibility = View.GONE
                linearWeekDays?.visibility = View.GONE
            }
            else -> colorCode = resources.getColor(R.color.full_time_color)
        }
        txtJobType?.setBackgroundColor(colorCode)
    }

    private lateinit var viewModel: JobDetailModel
    private lateinit var viewModelProfile: ProfileModel
    private lateinit var viewModelJobList: JobListModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(JobDetailModel::class.java)
        viewModelProfile = ViewModelProvider(this).get(ProfileModel::class.java)
        viewModelJobList = ViewModelProvider(this).get(JobListModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_job_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        mOkayGoFirebaseAnalytics = OkayGoFirebaseAnalytics(this)
        isForDeeplink = Preferences.prefs?.getBoolean(Constants.IS_FOR_DEEPLINK, false) ?: false
        val bundle: Bundle? = arguments
        bundle?.getSerializable(Constants.JOB_DETAIL_DATA)
//        val intent: Intent = getIntent()
        if (bundle != null && bundle.containsKey(Constants.JOB_DETAIL_DATA)) {
            mJobItems = bundle.getSerializable(Constants.JOB_DETAIL_DATA) as JobContent
        } else if (bundle != null && bundle.containsKey(Constants.JOB_DETAIL_ID)) {
            mJobId = bundle.getString(Constants.JOB_DETAIL_ID)
        } else if (bundle != null && bundle.containsKey(Constants.JOB_ID)) {
            viewModel.getJobDetail(bundle.getString(Constants.JOB_ID)?.toInt())
            Preferences.prefs?.clearValue(Constants.JOB_ID)
            Preferences.prefs?.clearValue(Constants.WORKER_ID)
            Preferences.prefs?.clearValue(Constants.APP_DIRECT)
            viewModelJobList.matchJob(userId, bundle?.getString(Constants.JOB_ID)?.toInt())
            viewModelProfile.getWorkerByUserId(userId)
            isFromDeepLink = true
        } else if (isForDeeplink) {
            txtApply?.text = "Interested"
            viewModelJobList.matchJob(userId, Preferences.prefs?.getInt(Constants.JOB_ID, 0))
            Preferences.prefs?.saveValue(Constants.IS_FOR_DEEPLINK, false)
            viewModel.getJobDetail(Preferences.prefs?.getInt(Constants.JOB_ID, 0))
        }
        if (mJobId != null && mJobId?.isEmpty() == false) {
            viewModel.getJobDetail(mJobId?.toInt())
            txtApply?.visibility = View.GONE
        } else if (mJobItems != null) {
            viewModel.getJobDetail(mJobItems?.jobId)
        }
//        if (mJobItems == null) {
//            txtRefferEarnJob?.visibility = View.GONE
//            imgShare?.visibility = View.GONE
//        }
        setListeners()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.imgBack -> activity?.onBackPressed()
            R.id.txtRefferEarnJob -> viewModelJobList.getRefferJobLink(
                mDetailResponse?.detailsJobs?.get(0)?.jobDetailsId,
                mDetailResponse?.jobId,
                userId
            )
            R.id.txtApply ->
                if (isForDeeplink) {
                    showDialogMessage("Please complete your profile information, before apply.")
                } else {
                    showDailog(
                        getString(R.string.apply_job),
                        getString(R.string.want_to_apply_job),
                        false
                    )
                }
        }
    }


    private fun setData() {
        txtJobTitle?.text = "Job details - " + mDetailResponse?.detailsJobs?.get(0)?.jobTitle
        txtJobName.setText(mDetailResponse?.detailsJobs?.get(0)?.jobTitle)
        if (mDetailResponse?.company?.brandName?.length ?: 0 > 0) {
            txtCompany.setText(mDetailResponse?.company?.brandName)
        } else {
            txtCompany.setText(mDetailResponse?.company?.companyName)
        }
        if ((mDetailResponse?.detailsJobs?.get(0)?.referralClaimAmount ?: 0) > 0) {
            constraintReferAmount?.visibility = View.VISIBLE
            txtReferAmt?.text = "\u20B9 " +
                    mDetailResponse?.detailsJobs?.get(0)?.referralClaimAmount?.toString()
        } else {
            constraintReferAmount?.visibility = View.GONE
        }

        var salaryType = "p.m."
        if (mDetailResponse?.workTypeKey?.equals("ON DEMAND", ignoreCase = true) == true) {
            salaryType = "Daily"
            val comAmount = ((mDetailResponse?.detailsJobs?.get(0)?.amount
                ?: 0.0) / 100.00 * (mDetailResponse?.detailsJobs?.get(0)?.commission ?: 0.0))
            val amt = mDetailResponse?.detailsJobs?.get(0)?.amount?.minus(comAmount)
            txtSalary?.text =
                "\u20B9" + amt + "/" + (mDetailResponse?.detailsJobs?.get(0)?.amountPer
                    ?: salaryType)
//
        }
        val minSalary = Math.round(mDetailResponse?.detailsJobs?.get(0)?.minSalary ?: 0.0)
        val maxSalary = Math.round(mDetailResponse?.detailsJobs?.get(0)?.maxSalary ?: 0.0)
        val minSal = minSalary / 1000
        val maxSal = maxSalary / 1000
        if (mDetailResponse?.workTypeKey?.equals("ON DEMAND", ignoreCase = true) == false) {
            if (maxSal < 1) {
//            if (mDetailResponse?.workTypeKey?.equals("ON DEMAND", ignoreCase = true) == true) {
//                val comAmount = (mDetailResponse?.detailsJobs?.get(0)?.amount
//                    ?: 0.0 / 100.0 * (mDetailResponse?.detailsJobs?.get(0)?.commission ?: 0.0))
//                txtSalary?.text = "\u20B9" + (mDetailResponse?.detailsJobs?.get(0)
//                    ?.amount
//                    ?: 0 - comAmount) + "/" + (mDetailResponse?.detailsJobs?.get(0)?.amountPer?:salaryType)
//            } else {
                txtSalary?.text =
                    "\u20B9" + mDetailResponse?.detailsJobs?.get(0)?.amount + "/" + (mDetailResponse?.detailsJobs?.get(
                        0
                    )?.amountPer ?: salaryType)
//            }
            } else {
                if (minSal > 99) {
                    val mMinSal = minSal / 100.0
                    val mMaxSal = maxSal / 100.0
                    if (mMinSal == mMaxSal) {
                        txtSalary?.text =
                            "\u20B9" + mMaxSal + "L /" + (mDetailResponse?.detailsJobs?.get(0)?.amountPer
                                ?: salaryType)
                    } else {
                        txtSalary?.text =
                            "\u20B9" + mMinSal + "L - \u20B9" + mMaxSal + "L /" + (mDetailResponse?.detailsJobs?.get(
                                0
                            )?.amountPer ?: salaryType)
                    }
                } else {
                    if (minSal == maxSal) {
                        txtSalary?.text =
                            "\u20B9" + minSal + "k /" + (mDetailResponse?.detailsJobs?.get(0)?.amountPer
                                ?: salaryType)
                    } else {
                        txtSalary?.text =
                            "\u20B9" + minSal + "k - \u20B9" + maxSal + "k /" + (mDetailResponse?.detailsJobs?.get(
                                0
                            )?.amountPer ?: salaryType)
                    }
                }
            }
        }
        if (mDetailResponse?.address?.isEmpty() == true) {
            txtAddress.setText(mDetailResponse?.city_name)
        } else {
            txtAddress.setText(mDetailResponse?.address)
        }
        if (mDetailResponse?.detailsJobs?.get(0)?.boostJob == 1) {
            imgHotJobs?.visibility = View.VISIBLE
        } else {
            imgHotJobs?.visibility = View.GONE
        }
        if (mDetailResponse?.workdaysDetails?.isEmpty() == false) {
            linearWeekDays?.visibility = View.VISIBLE
            setDays(mDetailResponse?.workdaysDetails ?: "")
        } else {
            linearWeekDays?.visibility = View.GONE
        }
        val startDate: String? =
            Utilities.convertStringDateToDayMonth(mDetailResponse?.startDate ?: "")
        Log.e("Strst", startDate + "")
        if (startDate?.isEmpty() == false) {
            txtDateTitle?.visibility = View.VISIBLE
            imgCalender?.visibility = View.VISIBLE
            txtDate?.visibility = View.VISIBLE
            txtDate?.text = startDate
            if (mDetailResponse?.workTypeKey?.isEmpty() == false && mDetailResponse?.workTypeKey?.equals(
                    "ON DEMAND",
                    ignoreCase = true
                ) == true
            ) {
                if (mDetailResponse?.workdaysCount ?: 0 > 0) {
                    linearWeekDays?.visibility = View.VISIBLE
                    setDaysForOD(
                        mDetailResponse?.startDate ?: "",
                        mDetailResponse?.workdaysCount ?: 0
                    )
                } else {
                    linearWeekDays?.visibility = View.GONE
                }
            }
        } else {
            txtDateTitle?.visibility = View.GONE
            txtDate?.visibility = View.GONE
            //            holder.imgCalender.setVisibility(View.GONE);
        }
        val loginTime: String? =
            Utilities.convertTimeInAmPm(mDetailResponse?.loginTime)
        val logoutTime: String? =
            Utilities.convertTimeInAmPm(mDetailResponse?.logoutTime)
        Log.e("loginTime", loginTime + "")
        Log.e("logoutTime", logoutTime + "")
        if (mDetailResponse?.workTypeKey?.isEmpty() == false && mDetailResponse?.workTypeKey?.equals(
                "FULL TIME",
                ignoreCase = true
            ) == false
        ) {
            if (loginTime?.isEmpty() == false && logoutTime?.isEmpty() == false) {
                txtTimeTitle?.visibility = View.VISIBLE
                imgCalender?.visibility = View.VISIBLE
                txtTime?.visibility = View.VISIBLE
                txtTime?.text = "$loginTime - $logoutTime"
            } else {
                txtTimeTitle?.visibility = View.GONE
                txtTime?.visibility = View.GONE
            }
        } else {
            if (mDetailResponse?.detailsJobs?.get(0)?.noOfHrs ?: 0 > 0) {
                txtTimeTitle?.visibility = View.VISIBLE
                imgCalender?.visibility = View.VISIBLE
                txtTime?.visibility = View.VISIBLE
                txtTimeTitle?.text = "Working schedule"
                //            holder.imgTime.setVisibility(View.VISIBLE);
                if (mDetailResponse?.workdaysCount ?: 0 > 0) {
                    txtTime.setText(
                        mDetailResponse?.detailsJobs?.get(0)?.noOfHrs
                            .toString() + " hrs x " + mDetailResponse?.workdaysCount + " days"
                    )
                } else {
                    txtTime.setText(
                        mDetailResponse?.detailsJobs?.get(0)?.noOfHrs.toString() + " hrs"
                    )
                }
            } else {
                txtTimeTitle?.visibility = View.GONE
                txtTime?.visibility = View.GONE
                imgCalender?.visibility = View.GONE
            }
        }
        setJobType(mDetailResponse?.workTypeKey ?: "")
        setCompanyData()
        setJobDescription()
    }

    private fun setJobDescription() {
        if (mDetailResponse?.detailsJobs?.get(0)?.additionalRequirement?.length ?: 0 > 0) {
            txtJobDescription?.visibility = View.VISIBLE
            Utilities.readMoreOption(activity)?.addReadMoreTo(
                txtJobDescription,
                mDetailResponse?.detailsJobs?.get(0)?.additionalRequirement
            )
        } else {
            txtJobDescription?.text = "No Job Description"
        }
        if (mDetailResponse?.detailsJobs?.get(0)?.jobInterviewDetails?.joiningSpecialRequirement?.length ?: 0 > 0) {
            txtAdditionJobReq.setText(
                mDetailResponse?.detailsJobs?.get(0)?.jobInterviewDetails?.joiningSpecialRequirement
            )
            txtAdditionJobReq?.visibility = View.VISIBLE
        } else {
            txtAdditionJobReq?.text = "No Additional Requirements"
        }
    }

    private fun setCompanyData() {
        if (mDetailResponse == null) {
            return
        }
        Utilities.setImageByGlideRounded(
            activity,
            mDetailResponse?.company?.companyLogo,
            imgCompanyLogo
        )
        txtCompanyDescTop?.visibility = View.GONE
        if (mDetailResponse?.company?.shortDescription?.isEmpty() == false) {
            txtCompanyDesc.setText(mDetailResponse?.company?.shortDescription)
        } else {
            txtCompanyDesc?.text = "No company description available."
        }
        if (mDetailResponse?.company?.is_2meal == 1) {
            txtMeal?.visibility = View.VISIBLE
        } else {
            txtMeal?.visibility = View.GONE
        }
        if (mDetailResponse?.company?.is_pf_esic == 1) {
            txtPf?.visibility = View.VISIBLE
        } else {
            txtPf?.visibility = View.GONE
        }
        if (mDetailResponse?.company?.is_conveyance == 1) {
            txtConv?.visibility = View.VISIBLE
        } else {
            txtConv?.visibility = View.GONE
        }
        if (mDetailResponse?.company?.is_rooms == 1) {
            txtRoom?.visibility = View.VISIBLE
        } else {
            txtRoom?.visibility = View.GONE
        }
        if (mDetailResponse?.company?.banner_list != null &&
            mDetailResponse?.company?.banner_list?.equals("[]") == false &&
            mDetailResponse?.company?.banner_list?.equals("") == false
        ) {
            val listType = object :
                TypeToken<List<DocType>?>() {}.type
            if (listType != null) {
                val docTypeList: List<DocType> =
                    Gson().fromJson(mDetailResponse?.company?.banner_list, listType)
                val imageList: ArrayList<String>? =
                    ArrayList()
                if (!docTypeList.isEmpty()) {
                    for (x in docTypeList.indices) {
//                    imageList.add(SlideModel(docTypeList[x]?.cloudUrl, ScaleTypes.FIT))
                        imageList?.add(docTypeList[x].cloudUrl ?: "")
                    }
//                sliderComapy?.setImageList(imageList, ScaleTypes.FIT)
                    Log.e("image list", imageList?.toString() + "")
                    val adapter = SliderImageAdapter(context, imageList)
                    viewPagerSlider?.adapter = adapter
                    mSliderListSize = imageList?.size
                    addBottomDots(0)
                    timer = Timer()
                    handler = Handler()
                    timer?.schedule(object : TimerTask() {
                        // task to be scheduled
                        override fun run() {
                            handler?.post(sliderRunnable)
                        }
                    }, 500, 4000)
//                handler?.postDelayed(sliderRunnable, 3500)
                    viewPagerSlider?.registerOnPageChangeCallback(slidingCallback)

//                adapter?.notifyDataSetChanged()
                }
            }
        } else {
            viewPagerSlider?.visibility = View.GONE
//            sliderComapy?.visibility = View.GONE
        }
        when (mDetailResponse?.detailsJobs?.get(0)?.englishFluency) {
            1 -> txtEngValue?.text = resources.getString(R.string.no_eng)
            2 -> txtEngValue?.text = resources.getString(R.string.thoda_eng)
            3 -> txtEngValue?.text = resources.getString(R.string.good_eng)
            else -> txtEngValue?.text = "Not required"
        }
        if (mDetailResponse?.detailsJobs?.get(0)?.minAge ?: 0 == 0) {
            txtAgeValue?.text = "Any"
        } else if (mDetailResponse?.detailsJobs?.get(0)?.minAge ?: 0 > 0 &&
            mDetailResponse?.detailsJobs?.get(0)?.maxAge == 0
        ) {
            txtAgeValue.setText(mDetailResponse?.detailsJobs?.get(0)?.minAge.toString() + " yrs +")
        } else {
            txtAgeValue.setText(
                mDetailResponse?.detailsJobs?.get(0)?.minAge.toString() + " yrs - " + mDetailResponse?.detailsJobs?.get(
                    0
                )?.maxAge + " yrs"
            )
        }
        val skillSize: Int = mDetailResponse?.detailsJobs?.get(0)?.detailSkills?.size ?: 0
        txtEntryLevel?.visibility = View.VISIBLE
        imgEntryLevel?.visibility = View.VISIBLE
        if (skillSize > 0) {
            for (i in 0 until skillSize) {
                when (mDetailResponse?.detailsJobs?.get(0)?.detailSkills?.get(i)?.skillName) {
                    "Qualification" -> mEduId =
                        mDetailResponse?.detailsJobs?.get(0)?.detailSkills?.get(i)?.skillsTypeId
                            ?: 0
                    "Experience" -> mExpId =
                        mDetailResponse?.detailsJobs?.get(0)?.detailSkills?.get(i)?.skillsTypeId
                            ?: 0
                    "Gender" -> mGenderId =
                        mDetailResponse?.detailsJobs?.get(0)?.detailSkills?.get(i)?.skillsTypeId
                            ?: 0
                    "Cuisine" -> mCuisine =
                        mDetailResponse?.detailsJobs?.get(0)?.detailSkills?.get(i)?.skillValue ?: ""
                }
            }
            if (mGenderId == 26) {
                txtGenderValue?.text = "Male"
            } else if (mGenderId == 27) {
                txtGenderValue?.text = "Female"
            } else {
                txtGenderValue?.text = "Any"
            }
            val level: String? =
                mDetailResponse?.detailsJobs?.get(0)?.vacancyType?.substring(0, 1)
                    ?.toUpperCase() + mDetailResponse?.detailsJobs?.get(0)?.vacancyType?.substring(1)
                    .toString() + " level"
            if (level?.contains("null") == false) {
                txtEntryLevel?.text = level
            } else {
                txtEntryLevel?.text = "Entry level"
            }
            txtEduValue.setText(Utilities.getEduFromId(mEduId.toString() + ""))
            if (mCuisine.isEmpty()) {
                txtIndianCuisine?.visibility = View.GONE
                imgCusin?.visibility = View.GONE
            } else {
                txtIndianCuisine?.visibility = View.VISIBLE
                imgCusin?.visibility = View.VISIBLE
                txtIndianCuisine?.text = mCuisine
            }
        } else {
            txtEntryLevel.setText(Utilities.getExpFromId(mExpId))
            txtGenderValue?.text = "Any"
        }
    }

    // add custom dot indigatores
    private fun addBottomDots(currentPage: Int?) {
        dots = arrayOfNulls(mSliderListSize ?: 0)
        val colorsActive = resources.getIntArray(R.array.array_dot_active)
        val colorsInactive = resources.getIntArray(R.array.array_dot_inactive)
        layoutDots?.removeAllViews()
        for (i in 0..(dots?.size ?: 0) - 1) {
            dots!![i] = TextView(activity)
            dots!!.get(i)?.text = (Html.fromHtml("&#8226;"))
            dots!!.get(i)?.setTextSize(35f)
            dots!!.get(i)!!.setTextColor(colorsInactive[currentPage ?: 0])
            layoutDots?.addView(dots?.get(i))
        }
        if (dots?.size ?: 0 > 0) dots?.get(currentPage ?: 0)
            ?.setTextColor(colorsActive[currentPage ?: 0])
    }

    private fun showDialogMessage(body: String) {
        val builder =
            activity?.let { AlertDialog.Builder(it) }
        builder?.setCancelable(false)
        builder?.setMessage(body)?.setNeutralButton("OK") { dialog, which ->
            try {
                mAlertDialog?.dismiss()
                activity?.onBackPressed()
            } catch (e: Exception) {
            }
        }
        mAlertDialog = builder?.create()
        mAlertDialog?.show()
    }

    private fun showDailog(
        title: String,
        msg: String,
        isBack: Boolean
    ) {
        val dialog = activity?.let { Dialog(it) }
        dialog?.setContentView(R.layout.dialog_confirmation)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.setCancelable(false)
        val txtTitle = dialog?.findViewById<AppCompatTextView>(R.id.title)
        val message = dialog?.findViewById<AppCompatTextView>(R.id.txtMsg)
        val cancel = dialog?.findViewById<AppCompatTextView>(R.id.txtCancel)
        val confirm = dialog?.findViewById<AppCompatTextView>(R.id.txtConfirm)
        txtTitle?.text = title
        message?.text = msg
        cancel?.text = "NO"
        confirm?.text = "YES"
        cancel?.setOnClickListener { dialog.dismiss() }
        confirm?.setOnClickListener {
            if (isFromDeepLink) {
                if (mDetailResponse != null) {
                    val eventDeepLink =
                        ApplyJobEventDeepLink(mDetailResponse, cvFileName, deepLinkAssignId)
                    EventBus.getDefault().post(eventDeepLink)
                }
            } else {
                if (mJobItems != null) {
                    EventBus.getDefault().post(mJobItems)
                }
            }
            activity?.onBackPressed()
            dialog.dismiss()
        }
        dialog?.show()
    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.responseJobDetail.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (it.response?.content != null && it.response.content?.isEmpty() == false) {
                        mDetailResponse = it.response.content?.get(0)

                        //USE ONLY IF LAUNCH APP WITH DEEPLINK ONLY
                        ///////////////////////////////////////////////////////////
                        Constants.DEEPLINK_JOB_CAT_ID =
                            mDetailResponse?.detailsJobs?.get(0)?.jobCategoryId ?: 0
                        Constants.DEEPLINK_JOB_TYPE_ID =
                            mDetailResponse?.detailsJobs?.get(0)?.jobTypeId ?: 0
                        Constants.DEEPLINK_JOB_ID = mDetailResponse?.jobId ?: 0
                        Constants.DEEPLINK_JOB_DETAIL_ID =
                            mDetailResponse?.detailsJobs?.get(0)?.jobDetailsId ?: 0
                        Constants.DEEPLINK_JOB_TYPE =
                            mDetailResponse?.detailsJobs?.get(0)?.jobTitle ?: ""
                        Constants.DEEPLINK_WORK_TYPE =
                            mDetailResponse?.workType ?: ""
                        /////////////////////////////////////////////////////////////

                        setData()
                    } else {
                        activity?.onBackPressed()
                        Utilities.showToast(
                            activity,
                            "No detail available for this job at this time. Please try again."
                        )
                    }
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }

        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })

        viewModelProfile.responseWorkerData.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    cvFileName = it.response?.content?.get(0)?.cv_file_name ?: ""
                }
            }
        })

        viewModelProfile.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }

        })

        viewModelProfile.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
        viewModelJobList.responseMatchJob.observe(this, Observer {

            it?.let {
                if (it.code == Constants.SUCCESS) {
                    deepLinkAssignId = it.response?.assignId ?: 0

                    if (it.response?.isAccepted == 1) {
                        txtApply?.text = "Already applied"
                        txtApply?.setBackgroundDrawable(activity?.getDrawable(R.drawable.sp_disabled_button_40dp))
                        activity?.let { it1 -> ContextCompat.getColor(it1, R.color.gray) }
                            ?.let { it2 ->
                                txtApply?.setTextColor(
                                    it2
                                )
                            }
                        txtApply?.isEnabled = false
                    }
                }
            }
        })
        viewModelJobList.responseRefferLink.observe(this, Observer {

            it?.let {
                if (it.code == Constants.SUCCESS) {
                    OkayGoFirebaseAnalytics.job_refferal(
                        userId.toString(),
                        mDetailResponse?.jobId.toString(),
                        "Job Detail Screen"
                    )
                    var isOnDemand = false
                    if (mDetailResponse?.workType?.equals("ON DEMAND", ignoreCase = true) == true) {
                        isOnDemand = true
                    }
                    var title: String? = mDetailResponse?.detailsJobs?.get(0)?.jobTitle
                    if (title == null) {
                        title = mJobItems?.workType
                    }
                    val sal: String? = Utilities.getDisplaySalary(
                        mDetailResponse?.detailsJobs?.get(0)?.minSalary?.toInt() ?: 0,
                        mDetailResponse?.detailsJobs?.get(0)?.maxSalary?.toInt() ?: 0,
                        mDetailResponse?.detailsJobs?.get(0)?.amount ?: 0.0,
                        isOnDemand,
                        mDetailResponse?.detailsJobs?.get(0)?.amountPer
                    )
                    val msg =
                        """
                            I would like to refer you to a new job opportunity:
                            
                            Job Role: $title(${mDetailResponse?.workTypeKey})
                            Company: ${mDetailResponse?.company?.brandName ?: mDetailResponse?.company?.companyName}
                            Location: ${mDetailResponse?.city_name}
                            Experience: 
                            """.trimIndent() + Utilities.getExpFromId(mExpId) + "\nSalary: " + sal + "\nClick on the given link to see complete job requirements and apply.\n" + it?.response?.shortLink
                    activity?.let { it1 -> Utilities.shareApp(it1, msg + "", "Share Jobs") }
                }
            }
        })

        viewModelJobList.apiError.observe(this, Observer
        {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }

        })

        viewModelJobList.isLoading.observe(this, Observer
        {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        handler?.removeCallbacks(sliderRunnable)
    }
}