package com.okaygo.worker.ui.fragments.job_list

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.blankj.utilcode.util.ScreenUtils
import com.github.guilhe.views.SeekBarRangedView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.okaygo.worker.R
import com.okaygo.worker.adapters.FindJobAdapter
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.ApplyJobEventDeepLink
import com.okaygo.worker.data.modal.CheckOdJob
import com.okaygo.worker.data.modal.JobApplySuccess
import com.okaygo.worker.data.modal.LocatonPermissionOD
import com.okaygo.worker.data.modal.reponse.ExpFilter
import com.okaygo.worker.data.modal.reponse.JobCategories
import com.okaygo.worker.data.modal.reponse.JobCity
import com.okaygo.worker.data.modal.reponse.JobContent
import com.okaygo.worker.data.modal.request.FindJobRequest
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.activity.NavigationActivity
import com.okaygo.worker.ui.activity.post_application.PostApplicationActivity
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.Preferences
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.JobListModel
import com.openkey.guest.ui.fragments.verification.OnBoardingModel
import kotlinx.android.synthetic.main.fragment_find_job_list.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class JobListFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModel: JobListModel
    private lateinit var viewModelOnBoard: OnBoardingModel

    private val CV_UPLOAD_REQUEST_CODE = 2011

    var mFindJobList: ArrayList<JobContent>? = null
    var mFindJobAdapter: FindJobAdapter? = null
    private var mBundle: Bundle? = null
    private var mJobCategory: JobCategories? = null

    private var mCvFileName = ""
    var mBtnSubmit: AppCompatButton? = null
    private var mCvPath: String? = null
    private var mChipFileName: Chip? = null
    private var mCVLoader: ProgressBar? = null
    private var mTxtUploadCV: AppCompatTextView? = null

    private var mSlotFirst: String? = null
    private var mSlotSecond: String? = null
    private var mSlotThird: String? = null
    private var mInterViewDate: String? = ""
    private var mSlotFirstDate: String? = null
    private var mSlotSecondDate: String? = null
    private var mSlotThirdDate: String? = null
    private var mUloadedCVName = ""
    private var isCVRequired = 0

    private var txtTime: AppCompatTextView? = null
    private var txtPayTitle: AppCompatTextView? = null
    private var txtPay: AppCompatTextView? = null
    var mPaySeekbar: SeekBarRangedView? = null
    var mTimeSeekBar: SeekBarRangedView? = null
    var newDistance = -1
    var mStartTime = -1
    var mEndTime = -1
    var mMinAmount = -1
    var mMaxAmount = -1
    var selected = ""

    var expSelected = ""
    var citySelected = ""
    var alreadyCitySelected = ""
    var alreadyExpSelected = ""
    private var layoutPay: LinearLayoutCompat? = null
    private var type = "All Jobs"
//    private var mOkayGoFirebaseAnalytics: OkayGoFirebaseAnalytics? = null

    var cpgExperience: ChipGroup? = null
    var cpgCities: ChipGroup? = null
    private var mAssignId: Int? = 0

    //    private var mJobType: String? = null
//    private var mWorkType: String? = null
//    private var mDetailId: Int? = 0
    private var mDeeplink: String? = null
    private var isForNotInterested = false
    private var bottomSheetDialog: BottomSheetDialog? = null
    private var jobData: JobContent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(JobListModel::class.java)
        viewModelOnBoard = ViewModelProvider(this).get(OnBoardingModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_find_job_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
        mBundle = arguments
        mJobCategory = mBundle?.getParcelable(Constants.NAVIGATION_DATA)

        if (Constants.isToggle) {
            swAvailbaleToday?.setChecked(true)
            txtAvailableText?.setText(R.string.time_availablilty)
        } else {
            swAvailbaleToday?.setChecked(false)
            txtAvailableText?.setText(R.string.are_you_free_today)
        }
        CheckActiveFilters()
        initlize()
//        val request: FindJobRequest? = FindJobRequest(
//            userId,
//            mJobCategory?.id
//                ?: 0,
//            Constants.isOnDemand,
//            Constants.isPt,
//            Constants.isFt,
//            Constants.maxPay,
//            Constants.minMay,
//            Constants.jobType,
//            Constants.jobAfter,
//            Constants.jobBefore,
//            Constants.cityFilter,
//            Constants.expFilter,
//            0,
//            500
//        )
//        viewModel.getJobsAccordingToCat(request)
        setJobCatTitle()
        setListeners()
    }

    private fun setListeners() {
        imgBack?.setOnClickListener(this)
        txtCurrentJobType?.setOnClickListener(this)
        imgFilter?.setOnClickListener(this)

        swAvailbaleToday?.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            Log.e("Current Time", Utilities.getCurrentTime() + "")
            if (b) {
//                Constants.isToggle=true
                OkayGoFirebaseAnalytics.availability_today_on(Utilities.getCurrentTime())
                viewModel.saveSuperAvailability(userId, 1, null, workerId)
                txtAvailableText?.text = activity?.resources?.getString(R.string.time_availablilty)
            } else {
//                Constants.isToggle=false
                OkayGoFirebaseAnalytics.availability_today_off(Utilities.getCurrentTime())
                viewModel.saveSuperAvailability(userId, 0, null, workerId)
                txtAvailableText?.text = activity?.resources?.getString(R.string.want_work_today)
            }
        })
    }


    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
//        Constants.isToggle = false
    }

    private fun setJobCatTitle() {
        OkayGoFirebaseAnalytics.find_jobs(mJobCategory?.type_desc)
        txtHeader?.text = mJobCategory?.type_desc
        txtCount?.text = mJobCategory?.count.toString()
        when (mJobCategory?.id) {
            1344 -> imgHeaderIcon?.setImageResource(R.drawable.ic_cook)
            1345 -> imgHeaderIcon?.setImageResource(R.drawable.ic_rr)
            1346 -> imgHeaderIcon?.setImageResource(R.drawable.ic_trolly)
            1347 -> imgHeaderIcon?.setImageResource(R.drawable.ic__office_job)
            1348 -> imgHeaderIcon?.setImageResource(R.drawable.ic_delivery)
            1350 -> imgHeaderIcon?.setImageResource(R.drawable.ic_events)
            1351 -> imgHeaderIcon?.setImageResource(R.drawable.ic_sales)
            1353 -> imgHeaderIcon?.setImageResource(R.drawable.ic_education)
            1354 -> imgHeaderIcon?.setImageResource(R.drawable.ic_other_jobs)
            1722 -> imgHeaderIcon?.setImageResource(R.drawable.ic_health_care)
            1734 -> imgHeaderIcon?.setImageResource(R.drawable.ic_it)
            else -> imgHeaderIcon?.setImageResource(R.drawable.ic_other_jobs)
        }
    }

    private fun setAdapter() {
        val linearLayoutManager = LinearLayoutManager(context)
        mFindJobAdapter = FindJobAdapter(
            context, mFindJobList,
            OnApplyJob,
            OnJobDetailClick,
            onJobRejectClick,
            OnJobRefferAndEarn
        )
        recyclerFindJobHeader?.layoutManager = linearLayoutManager
        recyclerFindJobHeader?.adapter = mFindJobAdapter
    }

    private val OnApplyJob: (JobContent?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        Log.e("apply job id", it?.jobId.toString())

        mDeeplink = "Find Job"
        jobData = it
        isForNotInterested = false
        if (it?.workType?.toUpperCase() == "ON DEMAND" && (activity as NavigationActivity).loactionPermission() == false) {
            Constants.isLocationPermissionJobId = it.jobId ?: 0
            Constants.isApiCalled = false
        } else {
            viewModel.matchJob(userId, it?.jobId)
        }
//        val intent: Intent? = Intent(activity, NavigationActivity::class.java)
//        intent?.putExtra(Constants.NAV_SCREEN, 14)
//        startActivity(intent)

//        viewModel.getRequiredSkills(it?.jobId, workerId)
    }


    private val onJobRejectClick: (JobContent?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        Log.e("onJobRejectClick job id", it?.jobId.toString())
        isForNotInterested = true
        viewModel.matchJob(userId, it?.jobId)
    }

    private val OnJobDetailClick: (JobContent?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        OkayGoFirebaseAnalytics.job_detail_view(
            it?.workType,
            it?.jobId.toString() + "",
            it?.jobType,
            "",
            "findjobs"
        )

//        Constants.JOB_DETAIL_FROM_MIJOB = 0
        val intent: Intent? = Intent(activity, NavigationActivity::class.java)
        intent?.putExtra(Constants.NAV_SCREEN, Constants.JOB_DETAIL)
        intent?.putExtra(Constants.NAVIGATION_DATA, it)
        startActivity(intent)
//        val bundle:Bundle?= Bundle()
//        val fragment:JobDetailFragment?= JobDetailFragment()
//        bundle?.putSerializable(
//            Constants.JOB_DETAIL_DATA,
//            it
//        )
//        fragment?.arguments=bundle
//        fragment?.let { it1 -> attachFragment(it1,true) }
    }

    private val OnJobRefferAndEarn: (JobContent?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        OkayGoFirebaseAnalytics.job_refferal(
            userId?.toString(),
            it?.jobId?.toString(),
            "Find Job List Screen"
        )
        jobData = it
        viewModel.getRefferJobLink(it?.jobDetailId, it?.jobId, userId)
    }


    fun generateSlots(interviewDays: ArrayList<String>) {
        val calendar = Calendar.getInstance()
        val allowed_days: HashMap<String, Boolean>? = getAllowedDaysMap(interviewDays)
        var total_req_days = if (interviewDays.size > 3) 3 else interviewDays.size
        var total_added_days = 0
        var slot_we_are_creating = 0
        while (total_req_days > 0) {
            calendar.add(Calendar.DAY_OF_WEEK, 1)
            val day = calendar[Calendar.DAY_OF_WEEK]
            var slotDay: String? = null
            var slotDate: String? = null
            when (calendar[Calendar.DAY_OF_WEEK]) {
                Calendar.MONDAY -> if (allowed_days?.get("monday") ?: false) {
                    slot_we_are_creating = total_added_days + 1
                    slotDay = Utilities.getDayMonth("monday")
                    slotDate = Utilities.getInterviewSlotTime("monday")
                }
                Calendar.TUESDAY -> if (allowed_days?.get("tuesday") ?: false) {
                    slot_we_are_creating = total_added_days + 1
                    slotDay = Utilities.getDayMonth("tuesday")
                    slotDate = Utilities.getInterviewSlotTime("tuesday")
                }
                Calendar.WEDNESDAY -> if (allowed_days?.get("wednesday") ?: false) {
                    slot_we_are_creating = total_added_days + 1
                    slotDay = Utilities.getDayMonth("wednesday")
                    slotDate = Utilities.getInterviewSlotTime("wednesday")
                }
                Calendar.THURSDAY -> if (allowed_days?.get("thursday") ?: false) {
                    slot_we_are_creating = total_added_days + 1
                    slotDay = Utilities.getDayMonth("thursday")
                    slotDate = Utilities.getInterviewSlotTime("thursday")
                }
                Calendar.FRIDAY -> if (allowed_days?.get("friday") ?: false) {
                    slot_we_are_creating = total_added_days + 1
                    slotDay = Utilities.getDayMonth("friday")
                    slotDate = Utilities.getInterviewSlotTime("friday")
                }
                Calendar.SATURDAY -> if (allowed_days?.get("saturday") ?: false) {
                    slot_we_are_creating = total_added_days + 1
                    slotDay = Utilities.getDayMonth("saturday")
                    slotDate = Utilities.getInterviewSlotTime("saturday")
                }
                Calendar.SUNDAY -> if (allowed_days?.get("sunday") ?: false) {
                    slot_we_are_creating = total_added_days + 1
                    slotDay = Utilities.getDayMonth("sunday")
                    slotDate = Utilities.getInterviewSlotTime("sunday")
                }
            }
            if (slot_we_are_creating == 1) {
                if (slotDay != null && slotDate != null) {
                    mSlotFirst = slotDay
                    mSlotFirstDate = slotDate
                    total_req_days--
                    total_added_days++
                }
            } else if (slot_we_are_creating == 2) {
                if (slotDay != null && slotDate != null) {
                    mSlotSecond = slotDay
                    mSlotSecondDate = slotDate
                    total_req_days--
                    total_added_days++
                }
            } else if (slot_we_are_creating == 3) {
                if (slotDay != null && slotDate != null) {
                    mSlotThird = slotDay
                    mSlotThirdDate = slotDate
                    total_req_days--
                    total_added_days++
                }
            }
        }
    }


    /**
     * commented on reivew hit api to refresh count
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun applyJobEvent(jobItems: JobContent) {
        mCvFileName = jobItems.cv_name ?: ""
        jobItems.interviewDays?.let { generateSlots(it) }
        isCVRequired = jobItems.cv_required ?: 0
        mDeeplink = "JobDetailScreen"
        jobData = jobItems
        isForNotInterested = false
        if (jobItems.workType?.toUpperCase() == "ON DEMAND" && (activity as NavigationActivity).loactionPermission() == false) {
            Constants.isLocationPermissionJobId = jobItems.jobId ?: 0
        } else {
            viewModel.matchJob(userId, jobItems.jobId)
//            viewModel.getRequiredSkills(jobItems.jobId, workerId)
        }
    }

    /**
     * commented on reivew hit api to refresh count
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun ApplyJobEventDeepLink(jobItems: ApplyJobEventDeepLink) {
        Log.e("Apply job", "deeplink")
        mCvFileName = jobItems.cvFileName ?: ""
        isCVRequired = jobItems.content?.detailsJobs?.get(0)?.cvRequired ?: 0
//        viewModel.applyJob(
//            jobItems.assignId,
//            userId,
//            jobItems.content?.detailsJobs?.get(0)?.jobDetailsId
//        )
        viewModel.getRequiredSkills(jobItems.content?.jobId, workerId)

    }

    /**
     *
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun afterLocationPermissionActionOD(data: LocatonPermissionOD) {
        Log.e("permission action event", "true")
        viewModel.matchJob(userId, Constants.isLocationPermissionJobId)
//        viewModel.getRequiredSkills(Constants.isLocationPermissionJobId, workerId)
    }

    /**
     *
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun afterJobApply(data: JobApplySuccess?) {
        Log.e("afterJobApply", "true")
        FindJobsByType()
    }


    fun getAllowedDaysMap(interviewDays: ArrayList<String>): HashMap<String, Boolean>? {
        val temp = HashMap<String, Boolean>()
        temp["monday"] = false
        temp["tuesday"] = false
        temp["wednesday"] = false
        temp["thursday"] = false
        temp["friday"] = false
        temp["saturday"] = false
        temp["sunday"] = false
        for (i in interviewDays.indices) {
            temp[interviewDays[i]] = true
        }
        return temp
    }

    fun FindJobsByType() {
        Constants.isOnDemand = null
        Constants.isPt = null
        Constants.isFt = null
        Constants.maxPay = null
        Constants.minMay = null
        Constants.jobType = null
        Constants.expFilter = null
        Constants.cityFilter = null
        Constants.jobAfter = null
        Constants.jobBefore = null

        val mHomeFilter = Preferences.prefs?.getString(Constants.HOME_JOB_FILTER, null)
        if (mHomeFilter.equals("On demand")) {
            Constants.isOnDemand = "1"
        }
        if (mHomeFilter.equals("Part time")) {
            Constants.isPt = "1"
        }
        if (mHomeFilter.equals("Full time")) {
            Constants.isFt = "1"
        }
        if (Constants.pay_active) {
            if (mHomeFilter.equals("On demand")) {
            } else {
                Constants.maxPay = if (Constants.Maximum_Pay < 11) {
                    "" + Constants.Maximum_Pay * 5000
                } else {
                    "" + 40 * 5000
                }
                Constants.minMay = "" + Constants.Minimum_Pay * 5000
            }
        }
        if (Constants.city_active) {
            Constants.cityFilter = Constants.city_filters
        }

        if (Constants.exp_active) {
            Constants.expFilter = Constants.exp_filters
        }

        if (Constants.time_active) {
            Constants.jobAfter = if (Constants.start_time < 10) {
                Constants.start_time.toString() + "0:00:00"
            } else {
                Constants.start_time.toString() + ":00:00"
            }
            Constants.jobBefore = if (Constants.end_time < 10) {
                Constants.end_time.toString() + "0:00:00"
            } else {
                Constants.end_time.toString() + ":00:00"
            }
        }
        val request: FindJobRequest? = FindJobRequest(
            userId,
            mJobCategory?.id
                ?: 0,
            Constants.isOnDemand,
            Constants.isPt,
            Constants.isFt,
            Constants.maxPay,
            Constants.minMay,
            Constants.jobType,
            Constants.jobAfter,
            Constants.jobBefore,
            Constants.cityFilter,
            Constants.expFilter,
            0,
            500
        )
        viewModel.getJobsAccordingToCat(request)
    }

    private val onOKClick: () -> Unit = {
    }

    private fun showInterviewSlotsBottomSheet() {
        val dialog = activity?.let { BottomSheetDialog(it) }
        dialog?.setContentView(R.layout.dialog_interview_slot_selection)
        dialog?.setCancelable(false)
        val constraintCV = dialog?.findViewById<ConstraintLayout>(R.id.constraintCV)
        val imgClose = dialog?.findViewById<AppCompatImageView>(R.id.imgClose)
        val txtSlotFirst = dialog?.findViewById<AppCompatTextView>(R.id.txtSlotFirst)
        val txtSlotSecond = dialog?.findViewById<AppCompatTextView>(R.id.txtSlotSecond)
        val txtSlotTitle = dialog?.findViewById<AppCompatTextView>(R.id.txtSlotTitle)
        val txtSlotThird = dialog?.findViewById<AppCompatTextView>(R.id.txtSlotThird)
        val txtMsg = dialog?.findViewById<AppCompatTextView>(R.id.txtMsg)
        val chkCV = dialog?.findViewById<AppCompatCheckBox>(R.id.chkCv)
        val txtSupportMsg = dialog?.findViewById<AppCompatTextView>(R.id.txtSupportMsg)
        val txtUploadCVTitle = dialog?.findViewById<AppCompatTextView>(R.id.txtUploadCVTitle)
        val linearSlots = dialog?.findViewById<LinearLayoutCompat>(R.id.linearSlots)
        mTxtUploadCV = dialog?.findViewById(R.id.txtUploadCV)
        mCVLoader = dialog?.findViewById(R.id.loader)
        mChipFileName = dialog?.findViewById(R.id.chipFileName)
        mBtnSubmit = dialog?.findViewById(R.id.btnSubmit)
        chkCV?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mTxtUploadCV?.visibility = View.GONE
                txtSupportMsg?.visibility = View.GONE
                mChipFileName?.visibility = View.GONE
                mUloadedCVName = mCvFileName
            } else {
                mTxtUploadCV?.visibility = View.VISIBLE
                txtSupportMsg?.visibility = View.VISIBLE
                mUloadedCVName = ""
            }
            handleSubmitButton()
        }

        if (mSlotFirst == null || mSlotFirst?.isEmpty() == true) {
            txtSlotTitle?.visibility = View.GONE
            linearSlots?.visibility = View.GONE
        } else {
            txtSlotTitle?.visibility = View.VISIBLE
            linearSlots?.visibility = View.VISIBLE
            txtSlotFirst?.text = mSlotFirst
            if (mSlotSecondDate == null || mSlotSecondDate?.isEmpty() == true) {
                txtSlotSecond?.visibility = View.GONE
            }
            if (mSlotThirdDate == null || mSlotThirdDate?.isEmpty() == true) {
                txtSlotThird?.visibility = View.GONE
            }
            txtSlotSecond?.text = mSlotSecond
            txtSlotThird?.text = mSlotThird
        }
        if (!mCvFileName.isEmpty()) {
            chkCV?.visibility = View.VISIBLE
        } else {
            chkCV?.visibility = View.GONE
        }
        if (isCVRequired > 1) {
            txtMsg?.setText(R.string.congrats_application_submitted_enter_date_time)
            constraintCV?.visibility = View.VISIBLE
        } else {
            txtMsg?.setText(R.string.congrats_application_submitted_enter_date)
            constraintCV?.visibility = View.GONE
        }

        txtSlotFirst?.setOnClickListener {
            mInterViewDate = mSlotFirstDate
            txtSlotFirst.setTextColor(
                activity?.let { it1 -> ContextCompat.getColor(it1, R.color.theme) }
                    ?: R.color.theme
            )
            txtSlotFirst.setBackgroundDrawable(activity?.resources?.getDrawable(R.drawable.sp_border_rounded_blue_selection))
            txtSlotSecond?.setBackgroundDrawable(activity?.resources?.getDrawable(R.drawable.sp_border_rounded_grey))
            txtSlotThird?.setBackgroundDrawable(activity?.resources?.getDrawable(R.drawable.sp_border_rounded_grey))
            handleSubmitButton()
        }

        txtSlotSecond?.setOnClickListener {
            mInterViewDate = mSlotSecondDate
            txtSlotSecond.setTextColor(
                activity?.let { it1 -> ContextCompat.getColor(it1, R.color.theme) }
                    ?: R.color.theme
            )
            txtSlotFirst?.setBackgroundDrawable(activity?.resources?.getDrawable(R.drawable.sp_border_rounded_grey))
            txtSlotSecond?.setBackgroundDrawable(activity?.resources?.getDrawable(R.drawable.sp_border_rounded_blue_selection))
            txtSlotThird?.setBackgroundDrawable(activity?.resources?.getDrawable(R.drawable.sp_border_rounded_grey))
            handleSubmitButton()
        }

        txtSlotThird?.setOnClickListener {
            mInterViewDate = mSlotThirdDate
            txtSlotThird?.setTextColor(
                activity?.let { it1 -> ContextCompat.getColor(it1, R.color.theme) }
                    ?: R.color.theme
            )
            txtSlotFirst?.setBackgroundDrawable(activity?.resources?.getDrawable(R.drawable.sp_border_rounded_grey))
            txtSlotSecond?.setBackgroundDrawable(activity?.resources?.getDrawable(R.drawable.sp_border_rounded_grey))
            txtSlotThird?.setBackgroundDrawable(activity?.resources?.getDrawable(R.drawable.sp_border_rounded_blue_selection))
            handleSubmitButton()
        }

        imgClose?.setOnClickListener {
            Utilities.jobSuccessDialog(
                activity,
                activity?.resources?.getString(R.string.job_applied),
                activity?.resources?.getString(R.string.applied_success_descriotion),
                true, onOKClick
            )

            dialog.dismiss()
        }

        mTxtUploadCV?.setOnClickListener {
            if (checkPermission()) {
                showFileChooser()
            }
        }

        mBtnSubmit?.setOnClickListener {
            if (mCvPath != null && mCvPath?.isNotEmpty() ?: false) {
                bottomSheetDialog = dialog
                val file = File(mCvPath)
                viewModelOnBoard.getDocLink(userId, file)
            } else {
                if (mInterViewDate?.isNotEmpty() ?: false) {
                    viewModel.acceptInterviewSlot(mAssignId, mInterViewDate, userId)
                } else {
                    Utilities.jobSuccessDialog(
                        activity,
                        activity?.resources?.getString(R.string.job_applied),
                        activity?.resources?.getString(R.string.applied_success_descriotion),
                        true, onOKClick
                    )

                }
                dialog?.dismiss()
            }
        }
        if (isAdded) {
            dialog?.show()
        }
    }

    private fun checkPermission(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity?.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || activity?.checkSelfPermission(
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Log.e("STORAGE PERMISSION", "Permission is granted")
                activity?.let {
                    ActivityCompat.requestPermissions(
                        it,
                        arrayOf(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        1
                    )
                }
                return false
            }
            return true
        }
        return true
    }

    private fun showFileChooser() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        val mimeTypes = arrayOf(
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "image/*",
            "application/pdf"
        )
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        try {
            startActivityForResult(
                Intent.createChooser(intent, "Select a File to Upload"),
                CV_UPLOAD_REQUEST_CODE
            )
        } catch (ex: ActivityNotFoundException) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(
                activity, "Please install a File Manager.",
                Toast.LENGTH_SHORT
            ).show()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CV_UPLOAD_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            val uri = data.data
            // Get the path
            mCvPath = activity?.let { Utilities.getFilePathForN(uri, it) }
            if (mCvPath != null) {
                val file = File(mCvPath)
                val file_size = (file.length() / (1024 * 1024)).toString().toInt()
                if (file_size < 1) {
                    val name = mCvPath?.split("/".toRegex())?.toTypedArray()
                    mUloadedCVName = name?.get(name.size - 1) ?: ""
                    mChipFileName?.visibility = View.VISIBLE
                    mChipFileName?.text = mUloadedCVName
                    handleSubmitButton()
                } else {
                    Utilities.showToast(activity, getString(R.string.file_size))
                }
            }
        }
    }

    private fun handleSubmitButton() {
        if (isCVRequired > 1 && !mUloadedCVName.isEmpty() || mInterViewDate?.isEmpty() == false) {
            mBtnSubmit?.setBackgroundDrawable(activity?.resources?.getDrawable(R.drawable.sp_blue_rectangular_radius))
            mBtnSubmit?.isEnabled = true
        } else {
            mBtnSubmit?.setBackgroundDrawable(activity?.resources?.getDrawable(R.drawable.sp_button_border_rounded_dis))
            mBtnSubmit?.isEnabled = false
        }
    }

    private fun setJobType() {
        activity?.let {
            val view = it.layoutInflater.inflate(R.layout.bottomsheet_select_job_type, null)
            val bottomSheetDialog = BottomSheetDialog(it)
            val heightInPixels = (ScreenUtils.getScreenHeight() / 1.5).toInt()
            val radioGroup = view.findViewById<RadioGroup>(R.id.rdJobType)
            val confirm = view.findViewById<AppCompatTextView>(R.id.txtConfirm)
            val close = view.findViewById<AppCompatImageView>(R.id.imgClose)
            val od = view.findViewById<RadioButton>(R.id.on_demand)
            val ft = view.findViewById<RadioButton>(R.id.full_time)
            val pt = view.findViewById<RadioButton>(R.id.part_time)
            val al = view.findViewById<RadioButton>(R.id.all)

            if (txtCurrentJobType?.getText().toString() == "On demand") {
                od.isChecked = true
            } else if (txtCurrentJobType?.getText().toString() == "Full time") {
                ft.isChecked = true
            } else if (txtCurrentJobType?.getText().toString() == "Part time") {
                pt.isChecked = true
            } else if (txtCurrentJobType?.getText().toString() == "All Jobs") {
                al.isChecked = true
            }

            close.setOnClickListener { bottomSheetDialog.dismiss() }
            confirm.setOnClickListener {
                if (radioGroup.checkedRadioButtonId != -1) {
                    val radioButton =
                        radioGroup.findViewById<RadioButton>(radioGroup.checkedRadioButtonId)
                    txtCurrentJobType?.setText(radioButton.text)
                    type = radioButton.text.toString()
                    Preferences.prefs?.saveValue(Constants.HOME_JOB_FILTER, type)
                    filterReset(true)
                    FindJobsByType()
                }
                OkayGoFirebaseAnalytics.find_jobs_change_job_type(type)
                bottomSheetDialog.dismiss()
            }
            val params =
                ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, heightInPixels)
            bottomSheetDialog.setContentView(view, params)
            bottomSheetDialog.show()
        }
    }

    fun filterReset(isForJobType: Boolean? = false) {
        //Time filter reset
        Constants.time_active = false
        Constants.start_time = 0
        Constants.end_time = 0

        //Payment Filter
        Constants.pay_active = false
        Constants.Minimum_Pay = 0
        Constants.Maximum_Pay = 0

        //Job type filter
        Constants.job_type_active = false
        Constants.job_type_ids = null
        Constants.job_type_count = 0

        //Job exp filter
        Constants.exp_active = false
        Constants.exp_filters = null
        Constants.exp_filters_count = 0

        //Job city filter
        Constants.city_active = false
        Constants.city_filters = null
        Constants.city_filters_count = 0
        CheckActiveFilters()
        if (isForJobType == false) {
            Utilities.showSuccessToast(activity, "Filter Reset")
        }
    }

    private fun OnFilterClick() {
        var isFilterUpdate = false
        val view: View? = activity?.layoutInflater?.inflate(R.layout.bottomsheet_filter, null)
        val bottomSheetDialog = activity?.let { BottomSheetDialog(it) }
        bottomSheetDialog?.setCancelable(false)
        val txtClearFilter = view?.findViewById<AppCompatTextView>(R.id.txtClearFilter)
        mPaySeekbar = view?.findViewById(R.id.pay_seekbar)
        txtPay = view?.findViewById(R.id.txtPay)
        mTimeSeekBar = view?.findViewById(R.id.time_seek_bar)
        txtTime = view?.findViewById(R.id.txtTime)
        val txtApply = view?.findViewById<AppCompatButton>(R.id.btnApply)
        cpgExperience = view?.findViewById<ChipGroup>(R.id.cpgExperience)
        cpgCities = view?.findViewById<ChipGroup>(R.id.cpgCities)
        val btnCancel = view?.findViewById<AppCompatTextView>(R.id.txtCancel)
        txtPayTitle = view?.findViewById(R.id.txtPayTitle)
        layoutPay = view?.findViewById<LinearLayoutCompat>(R.id.layoutPay)
        setExpFilter()
        viewModel.getJobCities(userId)

        if (Preferences.prefs?.getString(Constants.HOME_JOB_FILTER, null).equals("On demand")) {
            txtPayTitle?.setVisibility(View.GONE)
            txtPay?.setVisibility(View.GONE)
            layoutPay?.setVisibility(View.GONE)
        } else {
            txtPayTitle?.setVisibility(View.VISIBLE)
            txtPay?.setVisibility(View.VISIBLE)
            layoutPay?.setVisibility(View.VISIBLE)
            txtPay?.setText("\u20B9  - ₹ 50000 +")
            txtPayTitle?.setText("Monthly Pay")
        }

        if (Constants.pay_active) {
            mPaySeekbar?.setSelectedMinValue(Constants.Minimum_Pay.toFloat())
            mPaySeekbar?.setSelectedMaxValue(Constants.Maximum_Pay.toFloat())
            if (Constants.Maximum_Pay > 10) {
                txtPay?.setText("₹ " + Constants.Minimum_Pay * 5000 + " - ₹ 50000+ ")
            } else {
                txtPay?.setText("₹ " + Constants.Minimum_Pay * 5000 + " - ₹ " + Constants.Maximum_Pay * 5000)
            }
        }

        if (Constants.time_active) {
            mTimeSeekBar?.setSelectedMinValue(Constants.start_time.toFloat())
            if (Constants.end_time == 23) {
                mTimeSeekBar?.setSelectedMaxValue(24f)
                txtTime?.setText(Utilities.convertTimeToAmPm(Constants.start_time.toString() + ":00:00") + " - " + "11:59 PM")
            } else {
                mTimeSeekBar?.setSelectedMaxValue(Constants.end_time.toFloat())
                txtTime?.setText(
                    Utilities.convertTimeToAmPm(Constants.start_time.toString() + ":00:00") + " - " + Utilities.convertTimeToAmPm(
                        Constants.end_time.toString() + ":00:00"
                    )
                )
            }
        }

        btnCancel?.setOnClickListener {

            citySelected = ""
            for (i in 0 until cpgCities?.getChildCount()!!) {
                val chip = cpgCities?.getChildAt(i) as Chip
                if (chip.isChecked) {
                    if (citySelected == "") {
                        citySelected = citySelected + chip.id
                    } else {
                        citySelected = citySelected + "," + chip.id
                    }
                }
            }

            expSelected = ""
            for (i in 0 until cpgExperience?.getChildCount()!!) {
                val chip = cpgExperience?.getChildAt(i) as Chip
                if (chip.isChecked) {
                    if (expSelected == "") {
                        expSelected = expSelected + chip.id
                    } else {
                        expSelected = expSelected + "," + chip.id
                    }
                }
            }

            if (isFilterUpdate == true || !alreadyCitySelected.equals(citySelected) || !alreadyExpSelected.equals(
                    expSelected
                )
            ) {
                bottomSheetDialog?.let { it1 -> showAlert(it1) }
            } else {
                bottomSheetDialog?.dismiss()
            }
            isFilterUpdate = false

        }

        txtApply?.setOnClickListener {
            isFilterUpdate = false
            applyFilter(bottomSheetDialog)
        }

        mTimeSeekBar?.setOnSeekBarRangedChangeListener(object :
            SeekBarRangedView.OnSeekBarRangedChangeListener {
            override fun onChanged(view: SeekBarRangedView, minValue: Float, maxValue: Float) {
                isFilterUpdate = true
                val start = Math.round(minValue)
                val end = Math.round(maxValue)
                Log.e("value", "$start $end")
                mStartTime = start
                if (end == 24) {
                    mEndTime = end - 1
                } else {
                    mEndTime = end
                }
            }

            override fun onChanging(view: SeekBarRangedView, minValue: Float, maxValue: Float) {
                val start = Math.round(minValue)
                val end = Math.round(maxValue)
                if (end - start < 4) {
                    Constants.time_active = false
                    Toast.makeText(
                        activity,
                        "Invalid Range. \n Minimum Job Duration is 4 Hours.",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    val start_time: String
                    val end_time: String
                    if (end == 24) {
                        start_time = Utilities.convertTimeToAmPm("$start:00:00").toString()
                        end_time = "11:59 PM"
                        txtTime?.setText("$start_time - $end_time")
                    } else {
                        start_time = Utilities.convertTimeToAmPm("$start:00:00").toString()
                        end_time = Utilities.convertTimeToAmPm("$end:00:00").toString()
                        txtTime?.setText("$start_time - $end_time")
                    }
                }
            }
        })

        mPaySeekbar?.setOnSeekBarRangedChangeListener(object :
            SeekBarRangedView.OnSeekBarRangedChangeListener {
            override fun onChanged(view: SeekBarRangedView, minValue: Float, maxValue: Float) {
                isFilterUpdate = true
            }

            override fun onChanging(view: SeekBarRangedView, minValue: Float, maxValue: Float) {
                if (txtCurrentJobType?.getText() == "On demand") {
                    Constants.pay_active = false
                } else {
                    val min = 5000 * Math.round(minValue)
                    val max = 5000 * Math.round(maxValue)
                    if (max > 50000) {
                        txtPay?.setText("\u20B9 " + min + " - " + "\u20B9 50000 +")
                    } else {
                        txtPay?.setText("\u20B9 " + min + " - " + "\u20B9 " + max)
                    }
                    mMinAmount = Math.round(minValue)
                    mMaxAmount = Math.round(maxValue)
                }
            }
        })

        txtClearFilter?.setOnClickListener { //                //Time filter reset
            Constants.time_active = false
            Constants.start_time = 0
            Constants.end_time = 0

            //Payment Filter
            Constants.pay_active = false
            Constants.Minimum_Pay = 0
            Constants.Maximum_Pay = 0

            //Job type filter
            Constants.job_type_active = false
            Constants.job_type_ids = null
            Constants.job_type_count = 0

            //Job exp filter
            Constants.exp_active = false
            Constants.exp_filters = null
            Constants.exp_filters_count = 0
            //Job cit filter
            Constants.city_active = false
            Constants.city_filters = null
            Constants.city_filters_count = 0
            bottomSheetDialog?.dismiss()
            CheckActiveFilters()
            Utilities.showSuccessToast(activity, "Filter Reset")
            FindJobsByType()
        }

        val params = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            (ScreenUtils.getScreenHeight() / 1)
        )
        view?.let { bottomSheetDialog?.setContentView(it, params) }
        bottomSheetDialog?.show()
    }


    private fun setExpFilter() {
        val expFilterList: ArrayList<ExpFilter>? = ArrayList()
        var expFilter: ExpFilter
        expFilter = ExpFilter("Fresher", 40)
        expFilterList?.add(expFilter)
        expFilter = ExpFilter("1-2 yrs", 41)
        expFilterList?.add(expFilter)
        expFilter = ExpFilter("3-5 yrs", 42)
        expFilterList?.add(expFilter)
        expFilter = ExpFilter("5+ yrs", 43)
        expFilterList?.add(expFilter)

        for (i in (expFilterList?.indices)!!) {

            val expName = expFilterList.get(i)
            val chip = Chip(activity)
            chip.isCheckable = true
            chip.isCheckedIconVisible = false
            if (Constants.exp_filters != null) {
                if (Constants.exp_filters?.contains(expName.expId?.toString() + "") == true) {
                    chip.isChecked = true
                }
            }

            chip.text = expName.exp
            chip.id = expName.expId ?: 0
            chip.setChipBackgroundColorResource(R.color.bg_chips_drawable_light_white)
            chip.setChipStrokeColorResource(R.color.theme)
            chip.chipStrokeWidth = 1f
            chip.isClickable = true
            chip.isCheckedIconVisible = false

            cpgExperience?.addView(chip)
        }

        //for check filter makes any chnages or not
        alreadyExpSelected = ""
        var count = 0
        for (i in 0 until cpgExperience?.getChildCount()!!) {
            val chip = cpgExperience?.getChildAt(i) as Chip
            if (chip.isChecked) {
                count++
                if (alreadyExpSelected == "") {
                    alreadyExpSelected = alreadyExpSelected + chip.id
                } else {
                    alreadyExpSelected = alreadyExpSelected + "," + chip.id
                }
            }
        }
    }

    private fun applyFilter(bottomSheetDialog: BottomSheetDialog?) {

        val time: String = txtTime?.getText().toString().trim({ it <= ' ' })
        val pay: String = txtPay?.getText().toString().trim({ it <= ' ' })
        val timeArray: Array<String>
        val payArray: Array<String>
        var sTime = ""
        var eTime = ""
        var payMax = ""
        var payMin = ""

        if (mMaxAmount > -1) {
            Constants.Maximum_Pay = mMaxAmount
            Constants.pay_active = true
            mMaxAmount = -1
        }
        if (mMinAmount > -1) {
            Constants.Minimum_Pay = mMinAmount
            Constants.pay_active = true
            mMinAmount = -1
        }

        if (mStartTime > -1) {
            Constants.time_active = true
            Constants.start_time = mStartTime
            mStartTime = -1
        }
        if (mEndTime > -1) {
            Constants.time_active = true
            Constants.end_time = mEndTime
            mEndTime = -1
        }
        if (time != null && time.contains("-")) {
            timeArray = time.split("-".toRegex()).toTypedArray()
            sTime = timeArray[0]
            eTime = timeArray[1]
        }
        if (pay != null && pay.contains("-")) {
            payArray = pay.split("-".toRegex()).toTypedArray()
            payMax = payArray[1]
            payMin = payArray[0]
        }

        Constants.Active = true
        expSelected = ""
        var count = 0
        for (i in 0 until cpgExperience?.getChildCount()!!) {
            val chip = cpgExperience?.getChildAt(i) as Chip
            if (chip.isChecked) {
                count++
                if (expSelected == "") {
                    expSelected = expSelected + chip.id
                } else {
                    expSelected = expSelected + "," + chip.id
                }
            }
        }
        Constants.exp_filters_count = count
        if (expSelected.equals("")) {
            Constants.exp_filters = null
        } else {
            Constants.exp_filters = expSelected
        }

        if (count > 0) {
            Constants.exp_active = true
        }

        citySelected = ""
        var cityCount = 0
        for (i in 0 until cpgCities?.getChildCount()!!) {
            val chip = cpgCities?.getChildAt(i) as Chip
            if (chip.isChecked) {
                cityCount++
                if (citySelected == "") {
                    citySelected = citySelected + chip.id
                } else {
                    citySelected = citySelected + "," + chip.id
                }
            }
        }
        Constants.city_filters_count = cityCount
        if (citySelected.equals("")) {
            Constants.city_filters = null
        } else {
            Constants.city_filters = citySelected
        }
        if (cityCount > 0) {
            Constants.city_active = true
        }

        if (Constants.city_active || Constants.exp_active || Constants.pay_active || Constants.time_active) {
            imgFilter?.setImageResource(R.drawable.ic_group_470)
        } else {
            imgFilter?.setImageResource(R.drawable.ic_filter_svg)
        }
        OkayGoFirebaseAnalytics.find_jobs_filters_applied(
            Constants.exp_filters,
            Constants.city_filters,
            eTime,
            payMin,
            payMax
        )
        bottomSheetDialog?.dismiss()
        FindJobsByType()
    }

    fun CheckActiveFilters() {
        if (Constants.exp_active || Constants.city_active || Constants.pay_active || Constants.time_active) {
            imgFilter?.setImageResource(R.drawable.ic_group_470)
        } else {
            imgFilter?.setImageResource(R.drawable.ic_filter_svg)
        }
    }

    private fun showAlert(bottomSheetDialog: BottomSheetDialog) {
        // Create custom dialog object
        val dialog = activity?.let { Dialog(it) }
        dialog?.setContentView(R.layout.dialog_box_custom)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val message = dialog?.findViewById<View>(R.id.txtMsg) as AppCompatTextView
        val cancel = dialog.findViewById<View>(R.id.txtCancel) as AppCompatTextView
        val confirm = dialog.findViewById<View>(R.id.txtConfirm) as AppCompatTextView
        message.setText(R.string.data_not_saved)
        cancel.text = "No"
        confirm.text = "Save"
        dialog.setCancelable(false)
        dialog.show()

        // if decline button is clicked, close the custom dialog
        confirm.setOnClickListener { // Close dialog
            dialog.dismiss()
            applyFilter(bottomSheetDialog)
        }

        // if decline button is clicked, close the custom dialog
        cancel.setOnClickListener { // Close dialog
            dialog.dismiss()
            bottomSheetDialog.dismiss()
        }
    }

    private fun initlize() {
        val homeJobFilter = Preferences.prefs?.getString(Constants.HOME_JOB_FILTER, null)
        if (homeJobFilter == null) {
            Preferences.prefs?.saveValue(Constants.HOME_JOB_FILTER, "All Jobs")
            txtCurrentJobType?.setText("All Jobs")
        } else {
            txtCurrentJobType?.setText(homeJobFilter)
        }
        FindJobsByType()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> activity?.onBackPressed()
            R.id.txtCurrentJobType -> {
                setJobType()
            }
            R.id.imgFilter -> {
                OnFilterClick()
            }
        }
    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.responseFindJob.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (mFindJobList == null) {
                        mFindJobList = ArrayList()
                    } else {
                        mFindJobList?.clear()
                    }
                    it.response?.odJobs?.let { mFindJobList?.addAll(it) }
                    it.response?.hotJobs?.let { mFindJobList?.addAll(it) }
                    it.response?.nonHotJobs?.let { mFindJobList?.addAll(it) }
                    txtCount?.text = mFindJobList?.size.toString()
                    setAdapter()
                }
            }
        })

        viewModel.responseMatchJob.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    mAssignId = it.response?.assignId
                    if (isForNotInterested == false) {
                        viewModel.getRequiredSkills(it.response?.jobId, workerId)
//                        viewModel.applyJob(mAssignId, userId, null)
                    } else {
                        viewModel.rejectJob(mAssignId, userId)
                    }
                }
            }
        })

        viewModel.responseRejectJob.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    Utilities.showToast(context, "Job Rejected.")
                    val request: FindJobRequest? = FindJobRequest(
                        userId,
                        mJobCategory?.id
                            ?: 0,
                        Constants.isOnDemand,
                        Constants.isPt,
                        Constants.isFt,
                        Constants.maxPay,
                        Constants.minMay,
                        Constants.jobType,
                        Constants.jobAfter,
                        Constants.jobBefore,
                        Constants.cityFilter,
                        Constants.expFilter,
                        0,
                        500
                    )
                    viewModel.getJobsAccordingToCat(request)
                }
            }
        })

        viewModel.responseApplyJob.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    EventBus.getDefault()?.post(CheckOdJob(true))
                    OkayGoFirebaseAnalytics.on_job_apply(
                        jobData?.workType,
                        jobData?.jobType,
                        jobData?.jobDetailId?.toString(),
                        mDeeplink
                    )
                    if ((isCVRequired > 1 || mSlotFirst != null && mSlotFirst?.isNotEmpty() ?: false) && jobData?.jobType?.toUpperCase() != "ON DEMAND") {
                        showInterviewSlotsBottomSheet()
                    } else {
                        Utilities.jobSuccessDialog(
                            activity,
                            activity?.resources?.getString(R.string.job_applied),
                            activity?.resources?.getString(R.string.applied_success_descriotion),
                            true, onOKClick
                        )

                    }
                    FindJobsByType()
                }
            }
        })

        viewModel.responseSupperToggle.observe(this, Observer {
            it?.let {

            }
        })

        viewModel.responseRefferLink.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    val sal = Utilities.getDisplaySalary(
                        jobData?.min_salary
                            ?: 0,
                        jobData?.max_salary ?: 0,
                        jobData?.amount
                            ?: 0.0,
                        jobData?.workType?.toUpperCase().equals("ON DEMAND"),
                        jobData?.amountPer
                    )
                    val msg =
                        "I would like to refer you to a new job opportunity:\n\nJob Role: ${jobData?.jobType}(${jobData?.workType})\nCompany: ${(jobData?.brandName) ?: jobData?.company}\nLocation: ${jobData?.city ?: jobData?.city_name}\nExperience: ${jobData?.experience}\nSalary: $sal\nClick on the given link to see complete job requirements and apply.\n${it.response?.shortLink}"
                    activity?.let { it1 -> Utilities.shareApp(it1, msg + "", "Share Jobs") }
                }
            }
        })

        viewModelOnBoard.responseGetDocLink.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    viewModelOnBoard.uplaodCV(it.response, workerId, mUloadedCVName)
                }
            }
        })

        viewModel.responseRequiredSkills.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    val intent: Intent? = Intent(activity, PostApplicationActivity::class.java)
                    var completeDataCount = 0
                    var requiredFalseCount = 0
                    for (i in 0..(it.response?.size ?: 0) - 1) {
                        if (it.response?.get(i)?.required == true) {
                            if (it.response.get(i).required != it.response.get(i).available) {
                                intent?.putExtra(
                                    Constants.POST_NAV_SCREEN,
                                    Constants.UNCOMPLETE_INFO
                                )
                                break
                            } else {
                                completeDataCount = completeDataCount + 1
                            }
                        } else {
                            requiredFalseCount = requiredFalseCount + 1
                            completeDataCount = completeDataCount + 1
                        }
                    }

                    if (it.response?.size == requiredFalseCount) {
                        intent?.putExtra(Constants.POST_NAV_SCREEN, Constants.NO_REQUIREMENT)
                    } else
                        if (it.response?.size == completeDataCount) {
                            intent?.putExtra(Constants.POST_NAV_SCREEN, Constants.COMPLETE_INFO)
                        }
                    intent?.putExtra(Constants.POST_APPLY_JOB_DATA, jobData)
                    intent?.putExtra(Constants.POST_APPLY_REQUIRED_DATA, it)
                    intent?.putExtra(Constants.POST_Assign_id, mAssignId)
                    activity?.startActivity(intent)
                }
            }
        })

        viewModelOnBoard.responseUploadCV.observe(this, Observer {
            it?.let {

                mTxtUploadCV?.text = "Update CV"
                mChipFileName?.visibility = View.VISIBLE
                mChipFileName?.text = mUloadedCVName
                Utilities.showSuccessToast(activity, "File Uploaded Sucessfully!")
                if (mInterViewDate?.isEmpty() == false) {
                    viewModel.acceptInterviewSlot(mAssignId, mInterViewDate, userId)
                } else {
                    Utilities.jobSuccessDialog(
                        activity,
                        activity?.resources?.getString(R.string.job_applied),
                        activity?.resources?.getString(R.string.applied_success_descriotion),
                        true, onOKClick
                    )
                }
                bottomSheetDialog?.dismiss()
            }
        })

        viewModel.responseAcceptInterview.observe(this, Observer {
            it?.let {
                Utilities.jobSuccessDialog(
                    activity,
                    activity?.resources?.getString(R.string.job_applied),
                    activity?.resources?.getString(R.string.applied_success_descriotion),
                    true, onOKClick
                )
            }
        })

        viewModel.responseJobCities.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    val tempList: ArrayList<JobCity>? = ArrayList()
                    val set: HashSet<JobCity> = HashSet()
                    var jobCity: JobCity
                    for (i in (it.response?.indices)!!) {
                        jobCity = JobCity(
                            it.response.get(i).city_name, (it.response.get(i).new_city_id
                                ?: "0").toInt()
                        )
                        set.add(jobCity)
                    }
                    tempList?.clear()
                    tempList?.addAll(set)
                    for (i in (tempList?.indices)!!) {
                        val expName = tempList?.get(i)
                        val chip = Chip(activity)
                        chip.isCheckable = true
                        chip.isCheckedIconVisible = false
                        if (Constants.city_filters != null) {
                            if (Constants.city_filters?.contains(expName?.cityId?.toString() + "") == true) {
                                chip.isChecked = true
                            }
                        }

                        chip.text = expName.cityName
                        chip.id = expName.cityId ?: 0
                        chip.setChipBackgroundColorResource(R.color.bg_chips_drawable_light_white)
                        chip.setChipStrokeColorResource(R.color.theme)
                        chip.chipStrokeWidth = 1f
                        chip.isClickable = true
                        chip.isCheckedIconVisible = false
                        cpgCities?.addView(chip)
                    }

                    //for check filter makes any chnages or not
                    alreadyCitySelected = ""
                    var count = 0
                    for (i in 0 until cpgCities?.getChildCount()!!) {
                        val chip = cpgCities?.getChildAt(i) as Chip
                        if (chip.isChecked) {
                            count++
                            if (alreadyCitySelected == "") {
                                alreadyCitySelected = alreadyCitySelected + chip.id
                            } else {
                                alreadyCitySelected = alreadyCitySelected + "," + chip.id
                            }
                        }
                    }
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })

        viewModelOnBoard.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }
        })

        viewModelOnBoard.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }
}