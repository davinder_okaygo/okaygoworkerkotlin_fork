package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.*
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * @author Davinder Goel OkayGo.
 */
object SchedualRepository {
    private val mService = ApiHelper.getService()

    /**
     * get in app data for particular user to our server
     */
    fun getTravalMode(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        assignId: Int?
    ) {
        mService.getTravalMode(assignId)?.enqueue(object : Callback<SuccessResponse> {
            override fun onResponse(
                call: Call<SuccessResponse>?,
                response: Response<SuccessResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("in app data  failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    fun setTravalMode(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        assignId: Int?,
        userId: Int?,
        workerId: Int?,
        travalMode: Int?
    ) {
        mService.setTravalMode(assignId, userId, workerId, travalMode)
            ?.enqueue(object : Callback<SuccessResponse> {
                override fun onResponse(
                    call: Call<SuccessResponse>?,
                    response: Response<SuccessResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("in app data  failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun getCheckInData(
        successHandler: (ChekinResonse) -> Unit,
        failureHandler: (String) -> Unit,
        assignId: Int?,
        userId: Int?
    ) {
        mService.getCheckInData(assignId, userId)?.enqueue(object : Callback<ChekinResonse> {
            override fun onResponse(
                call: Call<ChekinResonse>?,
                response: Response<ChekinResonse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<ChekinResonse>?, t: Throwable?) {
                t?.let {
                    Log.e("in app data  failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    fun setCheckInData(
        successHandler: (CheckOutResponse) -> Unit,
        failureHandler: (String) -> Unit,
        check_in_id: Int?,
        updated_by: Int?,
        otp: String?
    ) {
        mService.setCheckInData(check_in_id, updated_by, otp)
            ?.enqueue(object : Callback<CheckOutResponse> {
                override fun onResponse(
                    call: Call<CheckOutResponse>?,
                    response: Response<CheckOutResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<CheckOutResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("in app data  failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun jobCheckOut(
        successHandler: (CheckOutResponse) -> Unit,
        failureHandler: (String) -> Unit,
        check_in_id: Int?,
        updated_by: Int?
    ) {
        mService.jobCheckOut(check_in_id, updated_by)?.enqueue(object : Callback<CheckOutResponse> {
            override fun onResponse(
                call: Call<CheckOutResponse>?,
                response: Response<CheckOutResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<CheckOutResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("in app data  failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    fun saveReview(
        successHandler: (SaveReviewResponse) -> Unit,
        failureHandler: (String) -> Unit,
        employer_id: Int?,
        jobId: Int?,
        ratings: Int?,
        requested_by: Int?,
        review: String?
    ) {
        mService.saveReview(employer_id, jobId, ratings, requested_by, review)
            ?.enqueue(object : Callback<SaveReviewResponse> {
                override fun onResponse(
                    call: Call<SaveReviewResponse>?,
                    response: Response<SaveReviewResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SaveReviewResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("in app data  failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun getEmployerReviewOpt(
        successHandler: (EmployerReviewResponse) -> Unit,
        failureHandler: (String) -> Unit,
        type_key: String?
    ) {
        mService.getEmployerReviewOpt(type_key)
            ?.enqueue(object : Callback<EmployerReviewResponse> {
                override fun onResponse(
                    call: Call<EmployerReviewResponse>?,
                    response: Response<EmployerReviewResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<EmployerReviewResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("in app data  failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun saveEmployerReviewPt(
        successHandler: (EmployerPointsResponse) -> Unit,
        failureHandler: (String) -> Unit,
        point_type: String?,
        status: String?,
        review_id: String?
    ) {
        val params: HashMap<String, String> = HashMap()
        params.put("point_type", point_type ?: "")
        params.put("status", status ?: "")
        params.put("review_id", review_id ?: "")

        mService.saveEmployerReviewPt(params)
            ?.enqueue(object : Callback<EmployerPointsResponse> {
                override fun onResponse(
                    call: Call<EmployerPointsResponse>?,
                    response: Response<EmployerPointsResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<EmployerPointsResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("in app data  failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun verifyInterviewOtp(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        interview_id: Int?,
        requestedBy: Int?,
        otp: String?
    ) {
        mService.verifyInterviewOtp(interview_id, requestedBy, otp)
            ?.enqueue(object : Callback<SuccessResponse> {
                override fun onResponse(
                    call: Call<SuccessResponse>?,
                    response: Response<SuccessResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("in app data  failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }
}