package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.CliamResponse
import com.okaygo.worker.data.modal.reponse.RaiseEnquiryResponse
import com.okaygo.worker.data.modal.reponse.ReferralDataResponse
import com.okaygo.worker.data.modal.reponse.ReferralWorkerDataResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class ReferralModel(app: Application) : MyViewModel(app) {
    var responseReferralData = MutableLiveData<ReferralDataResponse>()
    var responseReferralWorkerData = MutableLiveData<ReferralWorkerDataResponse>()
    var responseReferraClaim = MutableLiveData<CliamResponse>()
    var responseRaiseEnquiry = MutableLiveData<RaiseEnquiryResponse>()


    /**
     *get referral header data from server
     */
    fun getReferralData(userId: Int?, isLoader: Boolean? = true) {
        isLoading.value = isLoader
        ReferralRepository.getReferralData({
            isLoading.value = false
            responseReferralData.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

    /**
     *get referral child data fro server
     */
    fun getReferralWorkerData(userId: Int?, jobId: Int, isLoader: Boolean? = true) {
        isLoading.value = isLoader
        ReferralRepository.getReferralWorkerData({
            isLoading.value = false
            responseReferralWorkerData.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId, jobId)
    }

    /**
     *claim areferral amount
     */
    fun claimReferral(userId: Int?, referrBy: Int?, jobId: Int, claimAmount: Int?) {
        isLoading.value = true
        ReferralRepository.claimReferral({
            isLoading.value = false
            responseReferraClaim.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId, referrBy, claimAmount, jobId)
    }

    /**
     *Raise enquiry for particular referral
     */
    fun raiseEnquiry(userId: Int?, comment: String?, request: String?, referralId: String?) {
        isLoading.value = true
        ReferralRepository.raiseEnquiry({
            isLoading.value = false
            responseRaiseEnquiry.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId, comment, request, referralId)
    }
}