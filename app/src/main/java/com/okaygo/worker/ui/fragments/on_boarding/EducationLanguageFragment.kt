package com.okaygo.worker.ui.fragments.on_boarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.chip.Chip
import com.okaygo.worker.R
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.reponse.SaveEducationRequest
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.OnBoardingModel
import com.openkey.guest.ui.fragments.verification.ProfileModel
import kotlinx.android.synthetic.main.fragment_edu_lang.*

class EducationLanguageFragment : BaseFragment(), View.OnClickListener {

    private lateinit var viewModel: OnBoardingModel
    private lateinit var viewModelProfile: ProfileModel
    private var mCurentEduId: Int? = null
    private var mCurentLangId: Int? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(OnBoardingModel::class.java)
        viewModelProfile = ViewModelProvider(this).get(ProfileModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edu_lang, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        handleForProfile()
    }

    private fun setListeners() {
        imgBackIcon?.setOnClickListener(this)
        imgBack?.setOnClickListener(this)
        txtEdit?.setOnClickListener(this)
        txtSave?.setOnClickListener(this)
        txtDiscard?.setOnClickListener(this)
        btnSaveContinue?.setOnClickListener(this)
    }

    private fun handleForProfile() {
        if (Constants.IS_FROM_PROFILE == true) {
            constraintBottom?.visibility = View.GONE
            imgBackIcon?.visibility = View.VISIBLE
            linearBottomBtns?.visibility = View.VISIBLE
            txtEdit?.visibility = View.VISIBLE
            lnrBottomLayout?.visibility = View.GONE

//            for (i in 0 until chgEdu?.getChildCount()!!) {
//                chgEdu?.getChildAt(i)?.setEnabled(false)
//            }
//            for (i in 0 until chgLang?.getChildCount()!!) {
//                chgLang?.getChildAt(i)?.setEnabled(false)
//            }
            viewModelProfile.getCurrentEdu(workerId)
        } else {
            OkayGoFirebaseAnalytics.on_boarding_education_details()
            addLanguage()
            viewModel.getEducation()                    //get education api call
        }
    }

    private fun addLanguage() {
        chgLang?.removeAllViews()
        var chip: Chip? = null
        var tagName: String
        for (index in 1..3) {
            when (index) {
                1 -> {
                    chip = Chip(activity)
                    tagName = getString(R.string.no_eng)
                }
                2 -> {
                    chip = Chip(activity)
                    tagName = getString(R.string.thoda_eng)
                }
                3 -> {
                    chip = Chip(activity)
                    tagName = getString(R.string.good_eng)
                }
                else -> {
                    chip = Chip(activity)
                    tagName = ""
                }
            }
            chip.isCheckable = true
            chip.isCheckedIconVisible = false
            chip.text = tagName
            chip.id = index
            chip.setChipBackgroundColorResource(R.color.bg_chips_drawable)
            chip.setChipStrokeColorResource(R.color.theme)
            chip.chipStrokeWidth = 2f
            if (mCurentLangId != null && index == mCurentLangId) {
                chip.isChecked = true
            }
            chgLang?.addView(chip)
        }
        if (Constants.IS_FROM_PROFILE) {
            for (i in 0 until chgLang?.getChildCount()!!) {
                chgLang?.getChildAt(i)?.setEnabled(false)
            }
        }
    }


    private fun attachObservers() {
        viewModel.responseEducation.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    chgEdu?.removeAllViews()

                    for (index in 0 until (it.response?.content?.size ?: 0)) {
                        val tagName: String = it.response?.content?.get(index)?.typeValue ?: ""
                        val chip = Chip(activity)
                        chip.isCheckable = true
                        chip.isCheckedIconVisible = false
                        chip.text = tagName
                        chip.id = it.response?.content?.get(index)?.id ?: 0
                        chip.setChipBackgroundColorResource(R.color.bg_chips_drawable)
                        chip.setChipStrokeColorResource(R.color.theme)
                        chip.chipStrokeWidth = 2f
                        if (mCurentEduId != null) {
                            if (it.response?.content?.get(index)?.id == mCurentEduId) {
                                chip.isChecked = true
                            }
                        }
                        chgEdu?.addView(chip)
                    }

                    if (Constants.IS_FROM_PROFILE) {
                        for (i in 0 until chgEdu?.getChildCount()!!) {
                            chgEdu?.getChildAt(i)?.setEnabled(false)
                        }
                    }
                }
            }
        })

        viewModel.responseSaveEdu.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (Constants.IS_FROM_PROFILE == true) {
                        handleForProfile()
                    } else {
                        attachFragment(ExperienceFragment(), false)
                    }
                }
            }
        })

        viewModelProfile.responseCurrentEdu.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {

                    mCurentEduId = it.response?.content?.get(0)?.qualificationId
                    mCurentLangId = it.response?.content?.get(0)?.english_known_level
                    addLanguage()
                    viewModel.getEducation(false)                    //get education api call
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })

        viewModelProfile.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelProfile.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    private fun handleEdit() {
        constraintBottom?.visibility = View.VISIBLE
        txtEdit?.visibility = View.GONE


        for (i in 0 until chgEdu?.getChildCount()!!) {
            chgEdu?.getChildAt(i)?.setEnabled(true)
        }
        for (i in 0 until chgLang?.getChildCount()!!) {
            chgLang?.getChildAt(i)?.setEnabled(true)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBackIcon,
            R.id.imgBack -> activity?.onBackPressed()
            R.id.txtEdit -> {
                handleEdit()
            }
            R.id.txtDiscard -> handleForProfile()
            R.id.txtSave,
            R.id.btnSaveContinue -> {
                if (chgEdu?.getCheckedChipId() == -1) {
                    Utilities.showToast(activity, "Select atleast one education.")
                } else {
                    if (chgEdu?.getCheckedChipId() == -1) {
                        Utilities.showToast(activity, "Select Your English Level.")
                    } else {
                        val request: SaveEducationRequest? = SaveEducationRequest(
                            userId,
                            chgEdu.checkedChipId,
                            chgLang.checkedChipId,
                            workerId
                        )
                        viewModel.saveEducation(request)                        //save education api call
                    }
                }
            }
        }
    }
}