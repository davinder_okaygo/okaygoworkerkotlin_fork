package com.okaygo.worker.ui.fragments.choose_password

import android.content.Context
import android.content.Intent
import android.net.wifi.WifiManager
import android.os.Bundle
import android.text.format.Formatter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.iid.FirebaseInstanceId
import com.okaygo.worker.R
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.cognito.AuthHelper
import com.okaygo.worker.data.modal.AppResponse
import com.okaygo.worker.data.modal.request.AddUserRoleRequest
import com.okaygo.worker.data.modal.request.CreateSessionRequest
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.activity.dashboard.DashBoardActivity
import com.okaygo.worker.ui.activity.login_onboarding.OnBoardingActivity
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.application.OkayGo
import com.openkey.guest.help.Preferences
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.LoginModel
import com.openkey.guest.ui.fragments.verification.PasswordModel
import kotlinx.android.synthetic.main.fragment_chose_password.*

class ChosePasswordFragment : BaseFragment() {
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    private lateinit var viewModelLogin: LoginModel
    private lateinit var viewModelPassword: PasswordModel
    var current_latitude: Double? = 0.0
    var current_longitude: Double? = 0.0
    var set = false
    var oldPassword: String? = null
    var userName: String? = null

    var callback: GenericHandler = object : GenericHandler {
        override fun onSuccess() {
            Utilities.hideLoader()
//            mPrefManager?.setIsUserLoggedIn(true)
            userName?.let {
//                Preferences.prefs?.clearValue(Constants.IS_CHOSE_PASS)
//                Preferences.prefs?.clearValue(Constants.SAVE_USERNAME)
                val request = AddUserRoleRequest(phone_number = it, referred_by_code = "");
                viewModelPassword.addUserRolePhoneNumber(request)
            }

        }

        override fun onFailure(exception: Exception) {
            Utilities.hideLoader()
            imgNext?.setVisibility(View.VISIBLE)
            Utilities.showToast(activity, exception.message ?: "")
            showDialogMessage(
                "Password Setting failed",
                AuthHelper.formatException(exception)
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        viewModelLogin = ViewModelProvider(this).get(LoginModel::class.java)
        viewModelPassword = ViewModelProvider(this).get(PasswordModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_chose_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(OkayGo.appContext)
        if (Utilities.locationPermisiion(activity)) {
            mFusedLocationClient?.lastLocation?.addOnSuccessListener {
                current_latitude = it?.latitude
                current_longitude = it?.longitude
                Log.e("Location", current_latitude?.toString() + " \n " + current_longitude)
            }
        }
        val bundle: Bundle? = arguments
        userName = bundle?.getString("name")
        oldPassword = bundle?.getString("password")

//        if (userName == null || userName?.isEmpty() == true) {
//            userName = Preferences.prefs?.getString(Constants.SAVE_USERNAME, "")
//        }
        Preferences.prefs?.saveValue(Constants.DONE_ONBOARDING, false)


        imgNext?.setOnClickListener {
            if (edtPassword?.getText()?.length ?: 0 >= 6 && edtPassword?.getText()?.length ?: 0 <= 12 && edtConfPassword?.getText()?.length ?: 0 >= 6 && edtConfPassword?.getText()?.length ?: 0 <= 12 && edtConfPassword?.getText()
                    ?.toString()?.length == edtPassword?.getText()?.toString()?.length
            ) {
                if (edtPassword?.getText().toString() == edtConfPassword?.getText().toString()) {
                    Utilities.showLoader(activity)
                    OkayGoFirebaseAnalytics.onboarding_password()
                    changePassword()

                } else {
                    edtConfPassword?.setError("Password did not match")
                }
            } else if (edtPassword?.getText()?.length ?: 0 < 6) {
                edtPassword?.setError("Password Length 6 to 12 is required")
            } else if (edtConfPassword?.getText()?.length ?: 0 < 6) {
                edtConfPassword?.setError("Password did not match")
            } else if (edtConfPassword?.getText().toString().length != edtPassword?.getText()
                    .toString().length
            ) {
                edtConfPassword?.setError("Password did not match")
            }
        }
    }


    private fun createSession() {
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
            val manager =
                activity?.applicationContext?.getSystemService(Context.WIFI_SERVICE) as WifiManager
            val info = manager.connectionInfo
            val mac_address = info.macAddress
            val ip_address = Formatter.formatIpAddress(info.ipAddress)
            if (userId == 0) {
                userId = Preferences.prefs?.getInt(Constants.ID, 0)
            }
            val request = CreateSessionRequest(
                instanceIdResult.token,
                ip_address,
                mac_address,
                userId,
                current_longitude,
                current_latitude,
                Utilities.appVersion(activity!!),
                Constants.osVersion,
                Constants.brand.replace(" ", "")
            )
            viewModelPassword.createWorkerSession(request)
        }
    }

    private fun setUserDataOnFireBase(response: AppResponse?) {
        activity?.let {
            if (response?.user_id ?: 0 > 0) {
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("user_id", response?.user_id.toString() + "")
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_age", response?.worker_age.toString() + "")
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_gender", response?.worker_gender)
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_city", response?.worker_city)
                FirebaseAnalytics.getInstance(it).setUserProperty(
                    "worker_last_salary",
                    response?.worker_last_salary.toString() + ""
                )
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_english_level", response?.worker_english_level)
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_education", response?.worker_education)
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_job_type", response?.worker_job_type)
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_category", response?.worker_category)
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_exp", response?.worker_exp.toString() + "")
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty(
                        "worker_cv_status",
                        response?.worker_cv_status.toString() + ""
                    )
                FirebaseAnalytics.getInstance(it).setUserProperty(
                    "worker_onboard_status",
                    response?.worker_onboard_status.toString() + ""
                )
                FirebaseAnalytics.getInstance(it).setUserProperty(
                    "worker_job_apply_count",
                    response?.worker_job_apply_count.toString() + ""
                )
                FirebaseAnalytics.getInstance(it).setUserProperty(
                    "worker_interview_count",
                    response?.worker_interview_count.toString() + ""
                )
                FirebaseAnalytics.getInstance(it).setUserProperty(
                    "worker_selected_count",
                    response?.worker_selected_count.toString() + ""
                )
                FirebaseAnalytics.getInstance(it).setUserProperty(
                    "worker_joined_count",
                    response?.worker_joined_count.toString() + ""
                )
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_last_activity", response?.worker_last_activity)
            }
        }
    }

    private fun changePassword() {
        userName?.let {
            AuthHelper.getPool().getUser(it).changePasswordInBackground(
                oldPassword,
                edtPassword?.getText().toString(),
                callback
            )
        }
    }


    private fun showDialogMessage(title: String, body: String) {
        var userDialog: AlertDialog? = null

        val builder = activity?.let { AlertDialog.Builder(it) }
        builder?.setTitle(title)?.setMessage(body)?.setNeutralButton("OK") { dialog, which ->
            try {
                userDialog?.dismiss()

            } catch (e: java.lang.Exception) {
                activity?.onBackPressed()
            }
        }
        userDialog = builder?.create()
        userDialog?.show()
    }


    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModelPassword.response.observe(this, Observer {
            it?.let {
                Log.e("chose pass", " add user role response" + it?.toString())
                if (it.code == Constants.SUCCESS) {
                    Preferences.prefs?.saveValue(
                        Constants.userName,
                        it.response?.firstName + " " + it.response?.lastName
                    )
                    imgNext?.setVisibility(View.VISIBLE)
                    if (it.response != null) {
                        Log.e(
                            "chose pass",
                            " add user role response not null" + it.response?.toString()
                        )

                        Preferences.prefs?.saveValue(Constants.USER_ID, userName)
                        if (it.response.userId != 0) {
                            Preferences.prefs?.saveValue(Constants.ID, it?.response?.userId)
                            viewModelLogin.getInAppData(it.response.userId)
                            createSession()
                            if (Preferences.prefs?.getBoolean(
                                    Constants.DONE_ONBOARDING,
                                    false
                                ) == true
                            ) {
                                val intent = Intent(activity, DashBoardActivity::class.java)
                                startActivity(intent)
                                activity?.finish()
                            } else {
//                                val intent = Intent(activity, SummaryActivity::class.java)
                                val intent = Intent(activity, OnBoardingActivity::class.java)
                                intent.putExtra("name", userName)
                                intent.putExtra("very_first", true)
                                startActivity(intent)
                                activity?.finish()
                            }
                        } else {
                            Utilities.showToast(
                                activity,
                                "Invalid user id, Please re-login."
                            )
                            Utilities.logoutUser(userId, activity)
                        }
                    } else {
                        Utilities.showToast(activity, "Invalid Response!")
                    }
                }
            }
        })

        viewModelPassword.responseCreateSession.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    Preferences.prefs?.saveValue(Constants.OG_SESSION_ID, it.response?.sessionId)
                }
            }
        })

        viewModelLogin.response.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    setUserDataOnFireBase(it.response)
                }
            }
        })


        viewModelPassword.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
//                    Utilities.showToast(activity, it)
                }
            }

        })

        viewModelPassword.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
        viewModelLogin.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }

        })

        viewModelLogin.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }
}