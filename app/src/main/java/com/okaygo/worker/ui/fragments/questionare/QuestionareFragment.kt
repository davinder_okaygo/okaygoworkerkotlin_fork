package com.okaygo.worker.ui.fragments.questionare

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.SnapHelper
import com.google.android.material.textfield.TextInputEditText
import com.okaygo.worker.R
import com.okaygo.worker.adapters.QuestionareAdapter
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.JobApplySuccess
import com.okaygo.worker.data.modal.reponse.JobContent
import com.okaygo.worker.data.modal.reponse.QuestionareResponseItem
import com.okaygo.worker.data.modal.reponse.ReqiredSkillsResponse
import com.okaygo.worker.data.modal.request.QuestionareRequestItem
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.Dialogs
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.JobListModel
import com.openkey.guest.ui.fragments.verification.PaymentDetailModel
import com.openkey.guest.ui.fragments.verification.QuestionareModel
import kotlinx.android.synthetic.main.fragment_questionare.*
import org.greenrobot.eventbus.EventBus

class QuestionareFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModel: QuestionareModel
    private lateinit var viewModelJobList: JobListModel
    private lateinit var viewModelPaymentDetail: PaymentDetailModel

    private var mList: ArrayList<QuestionareResponseItem>? = null
    private var mAdapter: QuestionareAdapter? = null
    private var firstVisiblePosition = 0
    var linearLayoutManager: LinearLayoutManager? = null
    private var count = 0
    var mQuestionPos = 0
    var mNextCount = 0
    var height: Int = 0
    var width: Int = 0
    var numberOfSections: Int = 0
    private var mJobData: JobContent? = null
    private var mMap: HashMap<Int?, QuestionareRequestItem?>? = null
    private var mAssignId: Int? = null
    var mRequiredData: ReqiredSkillsResponse? = null
    private var isDialogForUpi = false
    private var upi: String? = ""

    //    var layoutViews: LinearLayout? = null
    var viewsList: ArrayList<View> = ArrayList()

    //    var mJobId: Int? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(QuestionareModel::class.java)
        viewModelJobList = ViewModelProvider(this).get(JobListModel::class.java)
        viewModelPaymentDetail = ViewModelProvider(this).get(PaymentDetailModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_questionare, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recylerQuestionare?.isNestedScrollingEnabled = false
        mMap = HashMap()
        val bundle: Bundle? = arguments
//        mJobId = bundle?.getInt(Constants.QUEST_JOB_ID) ?: 0
        if (bundle?.containsKey(Constants.POST_APPLY_JOB_DATA) == true) {
            mJobData = bundle.getSerializable(Constants.POST_APPLY_JOB_DATA) as JobContent
        }
        if (bundle?.containsKey(Constants.POST_APPLY_REQUIRED_DATA) == true) {
            mRequiredData =
                bundle.getSerializable(Constants.POST_APPLY_REQUIRED_DATA) as ReqiredSkillsResponse
//            mAssignId = bundle.getInt(Constants.POST_Assign_id)

        }
        if (bundle?.containsKey(Constants.POST_Assign_id) == true) {
            mAssignId = bundle.getInt(Constants.POST_Assign_id)
        }

        if (Utilities.isOnline(activity)) {
            viewModel.getJobQuestions(mJobData?.jobId ?: Constants.DEEPLINK_JOB_ID)
            viewModelPaymentDetail.getUPI(workerId)
//            viewModel.getJobQuestions(406)
        } else {
            Utilities.showToast(
                activity,
                activity?.resources?.getString(R.string.no_internet) ?: ""
            )
            activity?.onBackPressed()
        }
        setAdapter()
        setListeners()
        OkayGoFirebaseAnalytics.postApplicationOnQuestionnaire(
            mJobData?.jobId ?: Constants.DEEPLINK_JOB_ID
        )

//        setProgress()
    }

    private fun setProgress() {

        //for getting dimensions of screen
        val displayMetrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        height = displayMetrics.heightPixels
        width = displayMetrics.widthPixels

        numberOfSections = mList?.size ?: 0
        width -= (64 + 4 * numberOfSections)    //reducing length of layout by 16dp from left and right and in between 16dp {in between*number of sections)
//        width -= (30)    //reducing length of layout by 16dp from left and right and in between 16dp {in between*number of sections)
        width /= numberOfSections
        for (i in 0 until numberOfSections) {
            val v = View(activity)
            val params = LinearLayoutCompat.LayoutParams(width, 5)
            params.setMargins(2, 0, 2, 0) //giving 16dp internal margin between two views
            v.layoutParams = params
//            v.setText(Html.fromHtml("&#8259;"))
//            context?.let { ContextCompat.getColor(it,R.color.dark_grey) }?.let { v.setTextColor(it) }
//            v.setPadding(10, 0, 2, 0)
            viewsList.add(v) //adding views in array list for changing color on click of button
            v.setBackgroundColor(resources.getColor(R.color.divider_color))
            stories?.addView(v)
        }
        viewsList.get(0).setBackgroundColor(getResources().getColor(R.color.theme));

    }

    private fun setListeners() {
        txtBack?.setOnClickListener(this)
        txtNext?.setOnClickListener(this)
    }

    private fun setAdapter() {
        val snapHelper: SnapHelper? = PagerSnapHelper()
        snapHelper?.attachToRecyclerView(recylerQuestionare)
        if (mList == null) {
            mList = ArrayList()
        }

        mAdapter = QuestionareAdapter(
            activity,
            mList,
            onYesNoClick
        )
        linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        recylerQuestionare?.setAdapter(mAdapter)
        recylerQuestionare?.setLayoutManager(linearLayoutManager)

    }

    private val onYesNoClick: (QuestionareResponseItem?, Int?, Int?) -> Unit =
        { it, pos, answer ->
            Log.e("question", it?.question.toString())
            Log.e("Postion", pos.toString())
            Log.e("answer", answer.toString())
            mQuestionPos = pos ?: 0
            val items = QuestionareRequestItem(answer, userId, it?.job_question_id, userId, userId)
            mMap?.set(pos, items)
//            recylerQuestionare?.smoothScrollToPosition((pos ?: 0) + 1)
//            if (pos ?: 0 < (mList?.size ?: 0) - 1) {
//                viewsList.get((pos ?: 0) + 1)
//                    .setBackgroundColor(getResources().getColor(R.color.theme));
//            }
        }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.responseJobQuestions.observe(this, Observer {
            it?.let {
                if (it.isEmpty() == false) {
                    if (mList == null) {
                        mList = ArrayList()
                    } else {
                        mList?.clear()
                    }
                    mList?.addAll(it)
                    if (mList?.size == 1) {
                        txtNext?.text = "APPLY"
                    }
                    mAdapter?.notifyDataSetChanged()
                    setProgress()
                } else {
//                    txtNext?.text = "APPLY"
                    txtTitle?.visibility = View.GONE
                    linearBottomBtns?.visibility = View.GONE
//                    txtNoFound?.visibility = View.VISIBLE
                    viewModelJobList.applyJob(mAssignId, userId, Constants.DEEPLINK_REFFER_BY)
                }
            }
        })

        viewModel.responseSaveJobQuestions.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    OkayGoFirebaseAnalytics.postApplicationSubmitQuestionnaire(
                        mJobData?.jobId ?: Constants.DEEPLINK_JOB_ID
                    )
                    viewModelJobList.applyJob(mAssignId, userId, Constants.DEEPLINK_REFFER_BY)
                }
            }
        })


//        viewModelJobList.responseMatchJob.observe(this, Observer {
//            if (isAdded) {
//                it?.let {
//                    if (it.code == Constants.SUCCESS) {
//                        viewModelJobList.applyJob(it.response?.assignId, userId, null)
//                    }
//                }
//            }
//        })

        viewModelJobList.responseApplyJob.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    if (it.code == Constants.SUCCESS) {
                        var workType: String? = ""
                        if (mJobData == null) {
                            OkayGoFirebaseAnalytics.on_job_apply(
                                Constants.DEEPLINK_WORK_TYPE,
                                Constants.DEEPLINK_JOB_TYPE,
                                Constants.DEEPLINK_JOB_DETAIL_ID?.toString(),
                                "apply"
                            )
                            workType = Constants.DEEPLINK_WORK_TYPE
                        } else {
                            OkayGoFirebaseAnalytics.on_job_apply(
                                mJobData?.workType,
                                mJobData?.jobType,
                                mJobData?.jobDetailId?.toString(),
                                "apply"
                            )
                            workType = mJobData?.workType
                        }
                        if (workType.equals("ON DEMAND", true) && upi?.isEmpty() == true) {
                            isDialogForUpi = false
                            Dialogs.showAlertDialog(
                                activity,
                                "Job applied",
                                activity?.resources?.getString(R.string.applied_msg_od),
                                "UPDATE",
                                "LATER",
                                onAlertPosBtnClick,
                                onAlertNagClick
                            )
                        } else {
                            Utilities.jobSuccessDialog(
                                activity,
                                activity?.resources?.getString(R.string.job_applied),
                                activity?.resources?.getString(R.string.applied_success_descriotion),
                                true, onOKClick
                            )
                        }
                    }
                }
            }
        })
        viewModelPaymentDetail.responseSaveUPI.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    upi = it.response
                    Utilities.showToast(activity, "UPI Id is updated.")
                    EventBus.getDefault().post(JobApplySuccess(true))
                    activity?.finish()
                }
            }
        })
        viewModelPaymentDetail.responseGetUPI.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    upi = it.response ?: ""
                }
            }
        })

        viewModelPaymentDetail.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelPaymentDetail.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }
        })

        viewModelJobList.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })

        viewModelJobList.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }


    private fun showUpiDialog(savedUpi: String?) {
        activity?.let {
            val dialog = Dialog(it)
            dialog.setContentView(R.layout.dialog_salary)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            val title = dialog.findViewById<TextView>(R.id.title)
            val edtUpi = dialog.findViewById<TextInputEditText>(R.id.salary)
            val cancel = dialog.findViewById<TextView>(R.id.cancel)
            val confirm = dialog.findViewById<TextView>(R.id.confirm)
            title.text = "Enter your valid UPI id"
            confirm.text = "Save"
            edtUpi?.setHint("")
            edtUpi?.setFilters(arrayOf<InputFilter>(InputFilter.LengthFilter(50)))
            edtUpi?.gravity = Gravity.START
            edtUpi?.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            edtUpi?.setText(savedUpi ?: "")

            cancel.setOnClickListener {
                dialog.dismiss()
                EventBus.getDefault().post(JobApplySuccess(true))
                activity?.finish()
            }
            confirm.setOnClickListener {
                upi = edtUpi?.text?.trim()?.toString()
                val count = upi?.split("@")
                if (upi?.isEmpty() == true) {
                    Utilities.showToast(activity, "Enter your UPI id.")
                } else if ((upi?.length) ?: 0 < 3 || upi?.contains("@") == false || (count?.size
                        ?: 0) != 2
                ) {
                    edtUpi?.setError("Invalid UPI id.")
                } else {
                    dialog.dismiss()
                    isDialogForUpi = true
                    Dialogs.showAlertDialog(
                        activity,
                        "Check your UPI ID",
                        "Entered UPI ID is $upi. Do you want to save?",
                        "Yes",
                        "Edit",
                        onAlertPosBtnClick,
                        onAlertNagClick
                    )
                }
            }
            dialog.show()
        }
    }


    private val onAlertNagClick: () -> Unit = {
        if (isDialogForUpi) {
            showUpiDialog(upi)
        } else {
            EventBus.getDefault().post(JobApplySuccess(true))
            activity?.finish()
        }
    }

    private val onAlertPosBtnClick: () -> Unit = {
        if (isDialogForUpi == false) {
            showUpiDialog(upi)
        } else {
            viewModelPaymentDetail.updateUPI(workerId, upi)
        }
    }

    private val onOKClick: () -> Unit = {
        EventBus.getDefault().post(JobApplySuccess(true))
        activity?.finish()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtBack -> {
                if (mNextCount > 0) {
                    mNextCount = mNextCount - 1
//                    mQuestionPos = mQuestionPos - 1
                    recylerQuestionare?.smoothScrollToPosition(mNextCount)
                    if (mNextCount <= (mList?.size ?: 0) - 1) {
                        viewsList.get(mNextCount + 1)
                            .setBackgroundColor(getResources().getColor(R.color.divider_color))
                    }

                    if (mNextCount < (mList?.size ?: 0) - 1) {
                        txtNext?.text = "NEXT"
                    }
                } else {
                    activity?.onBackPressed()
                }
            }

            R.id.txtNext -> {
                if (txtNext?.text?.equals("APPLY") == true) {
                    OkayGoFirebaseAnalytics.postApplicationSubmitQuestionnaire(
                        mJobData?.jobId ?: Constants.DEEPLINK_JOB_ID
                    )
                    if (mList?.isEmpty() == true) {

//                        viewModelJobList.matchJob(userId, mJobData?.jobId)
                        viewModelJobList.applyJob(mAssignId, userId, Constants.DEEPLINK_REFFER_BY)
                    } else {
                        Log.e("value", mMap?.toString())
                        val tempList: ArrayList<QuestionareRequestItem?> = ArrayList()
                        for ((key, value) in mMap?.entries!!) {
                            println("Key: $key & Value: $value")
                            tempList.add(value)
                        }
                        Log.e("tempList value", tempList.toString())
//                        val request:QuestionareRequest?=tempList

                        if (mJobData == null) {
                            viewModel.saveJobQuestions(
                                Constants.DEEPLINK_JOB_ID,
                                Constants.DEEPLINK_JOB_DETAIL_ID,
                                userId,
                                tempList
                            )
                        } else {
                            viewModel.saveJobQuestions(
                                mJobData?.jobId,
                                mJobData?.jobDetailId,
                                userId,
                                tempList
                            )
                        }
                    }
                } else
                    if (mMap?.containsKey(mNextCount) == true) {
                        if (mNextCount >= 0) {
                            mNextCount = mNextCount + 1
                            recylerQuestionare?.smoothScrollToPosition(mNextCount)
                            if (mNextCount < mList?.size ?: 0) {
                                viewsList.get(mNextCount)
                                    .setBackgroundColor(getResources().getColor(R.color.theme));
                            }

                            if (mNextCount == (mList?.size ?: 0) - 1) {
                                txtNext?.text = "APPLY"
                            }
                        }
                    } else {
                        Utilities.showToast(activity, "Please select your answer first.")
                    }
            }
        }
    }
}