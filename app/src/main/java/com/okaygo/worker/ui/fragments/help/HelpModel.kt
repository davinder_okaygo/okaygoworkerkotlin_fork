package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.FaqResponse
import com.okaygo.worker.data.modal.reponse.UserDetailResponse
import com.okaygo.worker.data.modal.reponse.YoueTubeResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class HelpModel(app: Application) : MyViewModel(app) {
    var responseFaq = MutableLiveData<FaqResponse>()
    var responseYouetube = MutableLiveData<YoueTubeResponse>()
    var responseUserDetail = MutableLiveData<UserDetailResponse>()

    /**
     *
     */
    fun getFaq() {
        isLoading.value = false
        HelpRepository.getFaq({
            isLoading.value = false
            responseFaq.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        })
    }

    /**
     *
     */
    fun getYoutubeVideos() {
        isLoading.value = false
        HelpRepository.getYoutubeVideos({
            isLoading.value = false
            responseYouetube.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        })
    }

    /**
     *
     */
    fun getUserDetail(userId: Int?) {
        isLoading.value = true
        HelpRepository.getUserDetail({
            isLoading.value = false
            responseUserDetail.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

}