package com.okaygo.worker.ui.fragments.login

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler
import com.amazonaws.services.cognitoidentityprovider.model.CodeDeliveryDetailsType
import com.amazonaws.services.cognitoidentityprovider.model.SignUpResult
import com.amazonaws.services.cognitoidentityprovider.model.UsernameExistsException
import com.okaygo.worker.R
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.cognito.AuthHelper
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.activity.dashboard.DashBoardActivity
import com.okaygo.worker.ui.activity.login_onboarding.OnBoardingActivity
import com.okaygo.worker.ui.fragments.BaseFragment
import com.okaygo.worker.ui.fragments.choose_password.ChosePasswordFragment
import com.okaygo.worker.ui.fragments.login.model.AuthDetails
import com.okaygo.worker.ui.fragments.login.model.AuthenticationSuccess
import com.okaygo.worker.ui.fragments.otp.OtpFragment
import com.okaygo.worker.ui.fragments.password.PasswordFragment
import com.openkey.guest.help.Preferences
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.LoginModel
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : BaseFragment() {
    private var MY_PERMISSIONS_REQUEST_READ_LOCATION = 1011
    private lateinit var viewModel: LoginModel
    private var userDialog: AlertDialog? = null
    private var username: String? = null
    private var password: String? = null
    private var usernameInput: String? = null
    private var passwordInput: String? = null


    /**
     * registered user on cognito if not registerd and handle signup response
     */
    var signUpHandler: SignUpHandler = object : SignUpHandler {
        override fun onSuccess(user: CognitoUser, signUpResult: SignUpResult) {
            // Check signUpConfirmationState to see if the user is already confirmed
            val regState = signUpResult.userConfirmed
            if (regState) {
                Utilities.hideLoader()

                // User is already confirmed
                showDialogMessage(
                    "Sign up successful!",
                    usernameInput + " has been Confirmed",
                    true
                )
            } else {
                // User is not confirmed
                confirmSignUp(signUpResult.codeDeliveryDetails)
            }
        }

        override fun onFailure(exception: java.lang.Exception) {
            Utilities.hideLoader()
            if (exception is UsernameExistsException) {
                signInUser()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        viewModel = ViewModelProvider(this).get(LoginModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        mOkayGoFirebaseAnalytics = OkayGoFirebaseAnalytics(this)
        username = "9829789257"
        password = "hello"
        locationPermisiion()
        edtMobile?.setText("")
        Preferences.prefs?.clearValue(Constants.IS_WHATSAPP_SUB_DIALOG)

        setListeners()
        findCurrent()
    }

    /**
     * set click listner for required views
     */
    private fun setListeners() {
        imgNext?.setOnClickListener {
            if (edtMobile?.text?.length == 10) {
                Utilities.showLoader(activity)
                SignUpUser()
            } else {
                Toast.makeText(activity, "Enter 10 Digit.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    /**
     * find current logged in user for cognito in particular device
     */
    private fun findCurrent() {
        val user: CognitoUser? = AuthHelper.getPool().currentUser
        username = user?.userId
        username?.let {
            Log.e("username", "not null $username")
            AuthHelper.setUser(it)
            Utilities.showLoader(activity)
            user?.getSessionInBackground(
                Utilities.getAuthHandler(
                    onAuthSuccess,
                    onAuthDetails,
                    onAuthFailure
                )
            )
        }
    }

    /**
     * handle authentication success reposne for cognito
     */
    private val onAuthSuccess: (AuthenticationSuccess?) -> Unit = { it ->
        Utilities.showLoader(activity)
        Log.e("authenticationHandler", " -- Auth Success")
        AuthHelper.setCurrSession(it?.cognitoUserSession)
        AuthHelper.newDevice(it?.device)
        try {
            Preferences.prefs?.saveValue(
                Constants.ACCESS_TOKEN,
                AuthHelper.getCurrSession().accessToken.jwtToken
            )

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        launchUser()
    }

    private val onAuthDetails: (AuthDetails?) -> Unit = { it ->
        getUserAuthentication(it?.authenticationContinuation, it?.username)
    }

    /**
     * auth error for cognito
     */
    private val onAuthFailure: (Exception?) -> Unit = { it ->
        Utilities.hideLoader()
    }

    override fun onResume() {
        super.onResume()
        Preferences.prefs?.clearValue(Constants.IS_LOGGED_IN)
        Preferences.prefs?.clearValue(Constants.ID)
        Preferences.prefs?.clearValue(Constants.EMPLOYER_ID)
    }

    /**
     * registered user on cognito
     */
    private fun SignUpUser() {
        val userAttributes = CognitoUserAttributes()
        usernameInput = edtMobile?.text.toString()
        AuthHelper.setUser(usernameInput)
        passwordInput = edtMobile?.text.toString()
        val userGivenName: String = edtMobile?.text.toString()
        userAttributes.addAttribute(AuthHelper.getSignUpFieldsC2O()["Given name"], userGivenName)
        val userEmail: String = edtMobile?.text.toString() + "@okaygo.in"
        userAttributes.addAttribute(AuthHelper.getSignUpFieldsC2O()["Email"].toString(), userEmail)
        Preferences.prefs?.saveValue(Constants.MOBILE_NO, edtMobile?.text.toString().trim())
        val userPhoneNumber = "+91" + edtMobile?.text.toString()
        userAttributes.addAttribute(
            AuthHelper.getSignUpFieldsC2O()["Phone number"].toString(),
            userPhoneNumber
        )
        AuthHelper.getPool().signUpInBackground(
            usernameInput,
            passwordInput + "",
            userAttributes,
            null,
            signUpHandler
        )
    }

    /**
     * launch screen after success authentication
     */
    private fun launchUser() {
        Utilities.hideLoader()
        val isOnBoardingDone = Preferences.prefs?.getBoolean(Constants.DONE_ONBOARDING, false)
        val isLoggedIn = Preferences.prefs?.getBoolean(Constants.IS_LOGGED_IN, false)
        if (isOnBoardingDone == true) {
            val userActivity = Intent(activity, DashBoardActivity::class.java)
            userActivity.putExtra("name", username ?: "")
            startActivityForResult(userActivity, 4)
            activity?.finish()
        } else if (isLoggedIn == true && isOnBoardingDone == false) {
//            val intent = Intent(activity, SummaryActivity::class.java)
            val intent = Intent(activity, OnBoardingActivity::class.java)
            intent.putExtra("name", usernameInput)
            startActivity(intent)
            activity?.finish()
        }
    }

    private fun getUserAuthentication(
        continuation: AuthenticationContinuation?,
        username: String?
    ) {
        val authenticationDetails =
            AuthenticationDetails(edtMobile?.text.toString(), edtMobile?.text.toString(), null)
        continuation?.setAuthenticationDetails(authenticationDetails)
        continuation?.continueTask()
    }

    /**
     * alert dialog
     */
    private fun showDialogMessage(title: String?, body: String?, exit: Boolean) {
        val builder = activity?.let { AlertDialog.Builder(it) }
        builder?.setTitle(title)?.setMessage(body)?.setNeutralButton("OK") { dialog, which ->
            try {
                userDialog?.dismiss()
                if (exit) {
                    exit(usernameInput)
                }
            } catch (e: java.lang.Exception) {
                if (exit) {
                    exit(usernameInput)
                }
            }
        }
        userDialog = builder?.create()
        userDialog?.show()
    }

    private fun exit(uname: String?) {
        exit(uname, null)
    }

    private fun exit(u_name: String?, pass: String?) {
        val uname: String? = u_name
        val password: String? = pass
        val fragment: ChosePasswordFragment? = ChosePasswordFragment()
        val bundle: Bundle? = Bundle()
        bundle?.putString("name", uname)
        bundle?.putString("password", password)
        fragment?.arguments = bundle
        fragment?.let {
            attachFragment(it, true)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Constants.IS_UPDATE_DIALOG_DISPLAYED = false
    }

    private fun confirmSignUp(cognitoUserCodeDeliveryDetails: CodeDeliveryDetailsType) {
        OkayGoFirebaseAnalytics.onboarding_mobile_no(
            edtMobile?.text?.toString()?.trim({ it <= ' ' })
        )
        Utilities.hideLoader()
        val bundle: Bundle? = Bundle()
        val fragment: BaseFragment? = OtpFragment()
        bundle?.putString("source", "signup")
        bundle?.putString("name", usernameInput)
        bundle?.putBoolean("fault", false)
        bundle?.putString("destination", cognitoUserCodeDeliveryDetails.destination)
        bundle?.putString("deliveryMed", cognitoUserCodeDeliveryDetails.deliveryMedium)
        bundle?.putString("attribute", cognitoUserCodeDeliveryDetails.attributeName)
        fragment?.arguments = bundle
        fragment?.let { attachFragment(it, true) }
    }

    /**
     * sign in user if already registred on cognito
     */
    private fun signInUser() {
        OkayGoFirebaseAnalytics.login_mobile_no(
            edtMobile?.text?.toString()?.trim({ it <= ' ' })
        )
        val bundle: Bundle? = Bundle()
        val fragment: PasswordFragment? =
            PasswordFragment()
        bundle?.putBoolean(Constants.EXIST, true)
        bundle?.putString(Constants.USERNAME, usernameInput)
        fragment?.arguments = bundle
        fragment?.let { attachFragment(it, true) }
    }

    /**
     * check for loc permisson
     */
    private fun locationPermisiion() {
        activity?.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(
                        it,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(
                        it,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        ActivityCompat.requestPermissions(
                            it, arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
                            MY_PERMISSIONS_REQUEST_READ_LOCATION
                        )
                    } else {
                        ActivityCompat.requestPermissions(
                            it,
                            arrayOf(
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION
                            ),
                            MY_PERMISSIONS_REQUEST_READ_LOCATION
                        )
                    }
                }
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 10) {
            if (resultCode == Activity.RESULT_OK) {
                var name: String? = null
                if (data?.hasExtra("name") == true) {
                    name = data.getStringExtra("name")
                    AuthHelper.setUser(name)
                }
                exit(name, password)
            }
        }
    }
}