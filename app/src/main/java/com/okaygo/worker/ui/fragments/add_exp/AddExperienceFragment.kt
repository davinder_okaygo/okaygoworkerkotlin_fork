package com.okaygo.worker.ui.fragments.add_exp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.AddExpEvent
import com.okaygo.worker.data.modal.reponse.CategoryResponse
import com.okaygo.worker.data.modal.reponse.Experiences
import com.okaygo.worker.data.modal.reponse.JobType
import com.okaygo.worker.data.modal.request.AddExpRequest
import com.okaygo.worker.date_picker.MonthYearPickerDialog
import com.okaygo.worker.date_picker.MonthYearPickerDialogFragment
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.Preferences
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.AddExpModel
import com.openkey.guest.ui.fragments.verification.OnBoardingModel
import kotlinx.android.synthetic.main.fragment_add_exp.*
import org.greenrobot.eventbus.EventBus
import java.util.*
import kotlin.collections.HashMap

class AddExperienceFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModel: OnBoardingModel
    private lateinit var viewModelAdd: AddExpModel
    private var expData: Experiences? = null
    private var mMinYear = 0
    private var mMinMonth = 0
    private var mMinDay = 0
    private var mMaxYear = 0
    private var mMaxMonth = 0
    private var mMaxDay = 0
    private var mExpId: Int? = null                         //if their is any exp, like: for edit
    private var myCalendar: Calendar? = null
    private var isStartDate: Boolean? = false               //is start date entred
    private var selectedJobType: String? = null
    private var selectedJobTypeID: String? = null
    private var isJobTypeOther = false
    private var isForEdit: Boolean? = false   // true if screen open for edit
    private var industry_str = ""
    private var job_type_str = ""
    private var mapJobTypes: HashMap<String, String>? = null
    private var mJobTypesList: ArrayList<String>? = null
    private var jobTypeList: ArrayList<JobType>? = null
    private var mJobTypeListOther: ArrayList<JobType>? = null
    private var ind = ""
    private var job = ""
    private var mEndDate: String? = null                    //for selected end date
    private var mStartDate: String? = null                  //for selected start date
    private var spnAdapter: ArrayAdapter<String>? = null
    private var isOtherInd = false                      //for other interested cat
    private var isOtherJobType = false                  //for other job type

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(OnBoardingModel::class.java)
        viewModelAdd = ViewModelProvider(this).get(AddExpModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_exp, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        val bundle: Bundle? = arguments
        expData = bundle?.getParcelable(Constants.EXP_DATA)
        if (expData != null) {
            isForEdit = true
            setExpData()
        }
        mapJobTypes = HashMap()
        myCalendar = Calendar.getInstance()
        mMinYear = 1980
        mMinMonth = 1
        mMinDay = 1
        mMaxYear = myCalendar?.get(Calendar.YEAR) ?: 0
        mMaxMonth = myCalendar?.get(Calendar.MONTH)?.minus(1) ?: 0
        mMaxDay = myCalendar?.get(Calendar.DAY_OF_MONTH)?.minus(7) ?: 0
        if (isForEdit == false) {
            viewModel.fetchInterestedJobTypes()
        }
        setSpinnerForJobType()
    }

    /**
     * set data to view if it for edit page
     */
    private fun setExpData() {
        mExpId = expData?.experienceId
        isOtherInd = !(expData?.industryType ?: 0 in 105..115)
        ind = expData?.industryType?.toString() ?: ""
        isStartDate = true
        job = expData?.jobType?.toString() ?: ""
        edtLocation?.setText(expData?.workLocation)
        edtCompany?.setText(expData?.companyName)
        mStartDate = expData?.fromDate
        txtStartDate.setText(Utilities.getDateInMonthYear(mStartDate))
        if (expData?.toDate?.isNotEmpty() == true) {
            mEndDate = expData?.toDate
            txtEndDate?.setText(Utilities.getDateInMonthYear(mEndDate))
        } else {
            chkStillWorking?.setChecked(true);
            txtEndDate?.setVisibility(GONE);
            txtEDateTitle.setVisibility(GONE);
            mEndDate = "";
        }

        if (!isOtherInd) {
            viewModel.fetchInterestedJobTypes()

        } else {
            industry_str = expData?.industryName ?: ""
            job_type_str = expData?.jobTypeName ?: ""
            chgJobCat?.setVisibility(GONE);
            spnJobType?.setVisibility(GONE);
            edtOtherCat?.setText(industry_str);
            edtOtherCat?.setEnabled(false);
            edtOtherCat?.setVisibility(View.VISIBLE);
            edtOtherJobType?.setText(job_type_str);
            edtOtherJobType?.setEnabled(false);
            edtOtherJobType?.setVisibility(View.VISIBLE);
            txtJobTypeTitle?.setVisibility(View.VISIBLE)
        }
    }

    /**
     * set listeners for required views
     */
    private fun setListeners() {
        txtStartDate?.setOnClickListener(this)
        txtEndDate?.setOnClickListener(this)
        txtCancel?.setOnClickListener(this)
        txtSave?.setOnClickListener(this)

        chkStillWorking?.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, isChecked ->
            if (isChecked) {
                txtEDateTitle.setVisibility(View.GONE)
                txtEndDate.setVisibility(View.GONE)
                mEndDate = ""
            } else {
                txtEDateTitle.setVisibility(View.VISIBLE)
                txtEndDate.setVisibility(View.VISIBLE)
                txtEndDate.setText("")
                mEndDate = ""
            }
        })

        chgJobCat?.setOnCheckedChangeListener(ChipGroup.OnCheckedChangeListener { chipGroup, i ->
            if (i == -1) {
                txtJobTypeTitle?.setVisibility(View.GONE)
                spnJobType?.visibility = View.GONE
                edtOtherCat?.setVisibility(View.GONE)
                edtOtherJobType?.setVisibility(View.GONE)
                isOtherInd = false
            } else {
                if (i != 1354) {
                    isOtherInd = false
                }
                if (mJobTypesList != null) {
                    mJobTypesList?.clear()
                }

                val chip: Chip? = chipGroup?.findViewById(chipGroup.getCheckedChipId())
                if (isOtherInd || chip?.text == "Other") {
                    viewModelAdd.getInterestedJobType()                     // get other job types api call
                    txtJobTypeTitle?.setVisibility(View.VISIBLE)
                    edtOtherCat?.setVisibility(View.VISIBLE)
                    isOtherInd = true
                } else {
                    edtOtherCat.setText("")
                    edtOtherJobType.setText("")
                    edtOtherCat?.setVisibility(View.GONE)
                    edtOtherJobType?.setVisibility(View.GONE)
                    txtJobTypeTitle.setVisibility(View.VISIBLE)
                    spnJobType.visibility = View.VISIBLE
                    viewModelAdd.getJobTypeForInd(                              //get job type for all api call
                        mapJobTypes?.get(
                            chip?.text ?: ""
                        )
                    )
//                    isOtherInd = false
                }
            }
        })
    }

    /**
     * set adapter for sppiner for job type
     */
    private fun setSpinnerForJobType() {
        mJobTypesList = ArrayList<String>()

        spnAdapter = activity?.let {
            ArrayAdapter<String>(
                it,
                android.R.layout.simple_spinner_dropdown_item,
                mJobTypesList!!
            )
        }
        spnAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnJobType?.adapter = spnAdapter

        spnJobType?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                selectedJobType = parent.getItemAtPosition(position).toString()
                if (isJobTypeOther) {
                    for (i in mJobTypeListOther?.indices!!) {
                        if (selectedJobType == mJobTypeListOther?.get(i)?.categorySubType) {
                            selectedJobTypeID = mJobTypeListOther?.get(i)?.id.toString() + ""
                            break
                        }
                    }
                } else {
                    for (i in jobTypeList?.indices!!) {
                        if (selectedJobType == jobTypeList?.get(i)?.categorySubType) {
                            selectedJobTypeID = jobTypeList?.get(i)?.id.toString() + ""
                            break
                        }
                    }
                }
                if (selectedJobType == "Other") {
                    edtOtherJobType?.setVisibility(View.VISIBLE)
                    isOtherJobType = true
                } else {
                    isOtherJobType = false
                    edtOtherJobType?.setVisibility(View.GONE)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun setMaxTimeMili(): Long {
        val max = Calendar.getInstance()
        max[mMaxYear, mMaxMonth] = mMaxDay
        return max.timeInMillis
    }

    private fun setMinTimeMili(): Long {
        val min = Calendar.getInstance()
        min[mMinYear, mMinMonth] = mMinDay
        return min.timeInMillis
    }

    /**
     * when click on start date view
     */
    private fun handleStartDateClick() {
        val min = Calendar.getInstance()
        min[1980, 0] = 1
        val min_time = min.timeInMillis

        val dialogFragment: MonthYearPickerDialogFragment = MonthYearPickerDialogFragment
            .getInstance(
                myCalendar?.get(Calendar.MONTH) ?: 0,
                myCalendar?.get(Calendar.YEAR)?.minus(1) ?: 0,
                min_time,
                setMaxTimeMili(),
                txtStartDate?.text.toString()
            )
        activity?.supportFragmentManager?.let { dialogFragment.show(it, null) }
        dialogFragment.setOnDateSetListener(object : MonthYearPickerDialog.OnDateSetListener {
            override fun onDateSet(year: Int, monthOfYear: Int) {
                txtStartDate?.setText(Utilities.getDateInMonthYear(year.toString() + "-" + (monthOfYear + 1) + "-" + 1))
                mStartDate = year.toString() + "-" + (monthOfYear + 1) + "-" + "01"
                mMinYear = year
                mMinMonth = monthOfYear + 1
                isStartDate = true
            }
        })
    }

    /**
     * when click on end date view
     */
    private fun handleEndDateClick() {
        if (isStartDate == true) {
            val max_end = Calendar.getInstance()
            max_end.set(
                myCalendar?.get(Calendar.YEAR) ?: 0, myCalendar?.get(Calendar.MONTH) ?: 0,
                myCalendar?.get(Calendar.DAY_OF_MONTH) ?: 0
            )
            val max_time_end = max_end.timeInMillis

            val dialogFragment_end = MonthYearPickerDialogFragment
                .getInstance(
                    myCalendar?.get(Calendar.MONTH) ?: 0,
                    myCalendar?.get(Calendar.YEAR)?.minus(1) ?: 0,
                    setMinTimeMili(),
                    max_time_end,
                    txtEndDate?.text.toString()
                )
            activity?.supportFragmentManager?.let { dialogFragment_end.show(it, null) }
            dialogFragment_end.setOnDateSetListener(object :
                MonthYearPickerDialog.OnDateSetListener {
                override fun onDateSet(year: Int, monthOfYear: Int) {
                    txtEndDate?.text =
                        Utilities.getDateInMonthYear(year.toString() + "-" + (monthOfYear + 1) + "-" + "01")
                    mEndDate = year.toString() + "-" + (monthOfYear + 1) + "-" + "01"
                    mMaxYear = year
                    mMaxMonth = monthOfYear - 1
                }
            })
        } else {
            Utilities.showToast(activity, "Select a start date")
        }
    }

    /**
     * add exp button click handling
     */
    private fun addExp() {
        if (!isOtherInd) {
            if (chgJobCat?.getCheckedChipId() == -1) {
                Utilities.showToast(activity, "Select a industry.")
                return
            }

            if (selectedJobType == null || selectedJobType!!.isEmpty()) {
                Utilities.showToast(activity, "Select a Job Type.")
                return
            }
        }

        if (edtCompany?.text.toString().isEmpty()) {
            edtCompany?.setError("Company name required")
            return
        } else if (edtLocation?.text.toString().isEmpty()) {
            edtLocation?.setError("Location required")
            return
        } else if (txtStartDate?.text.toString().isEmpty()) {
            Utilities.showToast(activity, "Choose Start date.")
            return
        } else if (chkStillWorking?.isChecked() == false) {
            if (txtEndDate?.text.toString().isEmpty()) {
                Utilities.showToast(activity, "Choose End date.")
                return
            }
        }
        if (edtOtherCat?.getVisibility() == View.VISIBLE) {
            if (edtOtherCat?.text.toString().length == 0) {
                Utilities.showToast(activity, "Add Category")
                return
            }
        }
        if (edtOtherJobType?.getVisibility() == View.VISIBLE) {
            if (edtOtherJobType?.text.toString().length == 0) {
                Utilities.showToast(activity, "Select Job role")
                return
            }
        }
        if (isOtherInd && isForEdit == false) {
            if (isOtherJobType) {
                viewModelAdd.addTypeToConfigMaster(
                    edtOtherJobType?.text?.toString()?.trim(),
                    Preferences.prefs?.getInt(Constants.ID, 0)
                )
                addExpApi(mExpId?.toString())
            } else {
                if (selectedJobType == null || selectedJobType!!.isEmpty()) {
                    Utilities.showToast(activity, "Select a job type")
                    return
                } else {
                    viewModelAdd.addIndToConfigMaster(
                        edtOtherJobType?.text?.toString()?.trim(),
                        selectedJobTypeID,
                        Preferences.prefs?.getInt(Constants.ID, 0)
                    )
                    addExpApi(mExpId?.toString())
                }
            }
        } else {
            addExpApi(mExpId?.toString())
        }
    }

    /**
     * add exp api call
     */
    private fun addExpApi(expId: String?) {
        if (chkStillWorking?.isChecked == true) {
            mEndDate = null
        }
        var request: AddExpRequest? = null
        if (expId?.isNotEmpty() == true) {
            request = AddExpRequest(
                Preferences.prefs?.getInt(Constants.ID, 0),
                edtCompany?.text.toString().trim(),
                expData?.industryType?.toString(),
                expData?.jobType?.toString(), "1",
                edtLocation?.text?.toString(),
                Preferences.prefs?.getInt(Constants.EMPLOYER_ID, 0).toString(),
                mStartDate,
                mEndDate, expId
            )
        } else {
            request = AddExpRequest(
                Preferences.prefs?.getInt(Constants.ID, 0),
                edtCompany?.text.toString().trim(),
                chgJobCat?.getCheckedChipId()?.toString(),
                selectedJobTypeID, "1",
                edtLocation?.text?.toString(),
                Preferences.prefs?.getInt(Constants.EMPLOYER_ID, 0).toString(),
                mStartDate,
                mEndDate,
                null
            )
        }
        viewModelAdd.addWorkExp(request)                            //add worker exp api call
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtStartDate -> handleStartDateClick()
            R.id.txtEndDate -> handleEndDateClick()
            R.id.txtCancel -> activity?.onBackPressed()
            R.id.txtSave -> addExp()
        }
    }

    /**
     * handle interested job cat
     */
    private fun handleJobCat(response: CategoryResponse?) {
        mapJobTypes?.clear()
        var check = 0
        for (index in 0 until response?.content?.size!!) {
            val tagName: String = response?.content?.get(index).typeDesc ?: ""
            mapJobTypes?.put(
                tagName,
                response.content.get(index).typeValue ?: ""
            )
            val chip = Chip(activity)
            chip.isCheckable = true
            chip.isCheckedIconVisible = false
            chip.text = tagName
            if (response?.content?.get(index).id.toString() + "" == ind) {
                check = response?.content.get(index).id ?: 0
            }
            chip.id = response?.content.get(index).id ?: 0
            chip.setChipBackgroundColorResource(R.color.bg_chips_drawable)
            chip.setChipStrokeColorResource(R.color.theme)
            chip.chipStrokeWidth = 1f
            activity?.let {
                if (tagName?.contains("Chef") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_cook)
                } else if (tagName?.contains("Restaurant") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_resturants)
                } else if (tagName?.contains("Hotel") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_hotel_services)
                } else if (tagName?.contains("Office Job") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic__office_job)
                } else if (tagName?.contains("Delivery") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_delivery)
                } else if (tagName?.contains("Warehouse") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_warehouse)
                } else if (tagName?.contains("Event") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_events)
                } else if (tagName?.contains("Sales") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_sales)
                } else if (tagName?.contains("Customer Support") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_customer_support)
                } else if (tagName?.contains("Education") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_education)
                } else {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_other_jobs)
                }
            }
            chip.isCloseIconVisible = true
            chip.isCheckedIconVisible = false
            chip.setCloseIconTintResource(R.color.charcol_black)
            chgJobCat.addView(chip)
        }

        if (check != 0) {
            chgJobCat.check(check)
        }
    }

    /**
     * handle all api response
     */
    private fun attachObservers() {
        viewModelAdd.responseAddExp.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (isForEdit == true) {
                        Utilities.showSuccessToast(activity, "Experience edited successfuly.")
                    } else {
                        Utilities.showSuccessToast(activity, "Experience added successfuly.")
                    }
                    EventBus.getDefault().post(AddExpEvent(true))
                    activity?.onBackPressed()
                }
            }
        })

        viewModelAdd.responseIntrestedJobType.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (mJobTypesList == null) {
                        mJobTypesList = ArrayList<String>()
                    } else {
                        mJobTypesList?.clear()
                    }

                    if (mJobTypeListOther == null) {
                        mJobTypeListOther = ArrayList<JobType>()
                    } else {
                        mJobTypeListOther?.clear()
                    }
                    it.response?.content?.let { it1 ->
                        mJobTypeListOther?.addAll(
                            it1
                        )
                    }

                    for (i in 0 until it.response?.content?.size!!) {
                        it.response?.content.get(i).categorySubType?.let { it1 ->
                            mJobTypesList?.add(
                                it1
                            )
                        }
                    }
                    spnJobType?.visibility = View.VISIBLE

                    isJobTypeOther = true

                    spnAdapter?.notifyDataSetChanged()
                    if (mJobTypeListOther?.isEmpty() == false) {
                        selectedJobTypeID = mJobTypeListOther?.get(0)?.id.toString() + ""
                    }
                }
            }
        })

        viewModelAdd.responseJobTypeInd.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (mJobTypesList == null) {
                        mJobTypesList = ArrayList<String>()
                    } else {
                        mJobTypesList?.clear()
                    }

                    if (jobTypeList == null) {
                        jobTypeList = ArrayList<JobType>()
                    } else {
                        jobTypeList?.clear()
                    }
                    it.response?.content?.let { it1 -> jobTypeList?.addAll(it1) }
                    for (index in 0 until it.response?.content?.size!!) {
                        it.response?.content?.get(index).categorySubType?.let { it1 ->
                            mJobTypesList?.add(
                                it1
                            )
                        }
                    }

                    spnJobType.visibility = View.VISIBLE
                    isJobTypeOther = false

                    spnAdapter!!.notifyDataSetChanged()
                    if (jobTypeList?.isEmpty() == false) {
                        selectedJobTypeID = jobTypeList?.get(0)?.id.toString() + ""
                    }
                }
            }
        })

        viewModel.responseIntrestedJob.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    handleJobCat(it.response)
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })

        viewModelAdd.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelAdd.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }
}