package com.okaygo.worker.ui.fragments.referral

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.blankj.utilcode.util.ScreenUtils
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.textfield.TextInputEditText
import com.okaygo.worker.R
import com.okaygo.worker.adapters.ReferralHeaderAdtapter
import com.okaygo.worker.data.modal.reponse.ReferralData
import com.okaygo.worker.data.modal.reponse.ReferralWorkerData
import com.okaygo.worker.data.modal.request.UpdateWorkerRequest
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.Dialogs
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.ExperinceModel
import com.openkey.guest.ui.fragments.verification.ReferralModel
import kotlinx.android.synthetic.main.fragment_refferal.*

class ReferralFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModel: ReferralModel
    private lateinit var viewModelExperince: ExperinceModel

    private var mRequestList: ArrayList<String>? = null
    private var mSelectedRequest: String? = "Select request"

    private var mHeaderList: ArrayList<ReferralData>? = null
    private var mHeaderAdapter: ReferralHeaderAdtapter? = null
    private var upi: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ReferralModel::class.java)
        viewModelExperince = ViewModelProvider(this).get(ExperinceModel::class.java)
        attachObservers()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_refferal, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getReferralData(userId)
        if (Constants.USER_UPI_ID?.isEmpty() == false) {
//            txtUpi?.visibility = View.VISIBLE
            txtUpi?.text = "Registered UPI ID is ${Constants.USER_UPI_ID}"
        } else {
            txtUpi?.text = "Click to add UPI ID"
        }
        setListeners()
    }

    /**
     * set click listeners for required views
     */
    private fun setListeners() {
        imgBack?.setOnClickListener(this)
        txtTerms?.setOnClickListener(this)
        txtUpi?.setOnClickListener(this)
    }

    /**
     * set adapter for referral list outer part
     */
    private fun setAdapter() {
        val linearLayoutManager = LinearLayoutManager(activity)
        mHeaderAdapter =
            ReferralHeaderAdtapter(
                activity,
                mHeaderList,
                onExpandClick,
                onRaiseEnqueryClick,
                onClaimClick
            )
        recyleReferral?.layoutManager = linearLayoutManager
        recyleReferral?.adapter = mHeaderAdapter
    }

    /**
     * click handling for child list when click on any header item
     */
    private val onExpandClick: (ReferralData?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        Log.e("onExpandClick job id", it?.jobId.toString())
        viewModel.getReferralWorkerData(userId, it?.jobId ?: 0)

    }

    /**
     * click handling for iase Enquiry
     */
    private val onRaiseEnqueryClick: (ReferralWorkerData?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        Log.e("onMenuClick job id", it?.jobId.toString())
        showBottomSheetRaiseEnquiry(it)
    }

    /**
     * click handling for claim button
     */
    private val onClaimClick: (ReferralWorkerData?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        Log.e("onMenuClick job id", it?.jobId.toString())
        if (Constants.USER_UPI_ID?.isEmpty() == false) {
            viewModel.claimReferral(
                it?.userId,
                userId,
                it?.jobId ?: 0,
                it?.referralClaimAmount ?: 0
            )
        } else {
            showUpiDialog(Constants.USER_UPI_ID)
        }
    }

    /**
     * bottom sheet for raise enquiry
     */
    private fun showBottomSheetRaiseEnquiry(data: ReferralWorkerData?) {
        activity?.let {
            val view = it.layoutInflater.inflate(R.layout.bottomsheet_raise_enquiry, null)
            val bottomSheetDialog = BottomSheetDialog(it)
            val heightInPixels = (ScreenUtils.getScreenHeight() / 1.5).toInt()
            val submit = view.findViewById<AppCompatTextView>(R.id.txtSubmit)
            val close = view.findViewById<AppCompatImageView>(R.id.imgClose)
            val edtComment = view.findViewById<AppCompatEditText>(R.id.edtComment)
            val spnRequest = view.findViewById<Spinner>(R.id.spnRequests)

            mRequestList = ArrayList()
            mRequestList?.add("Select request")
            mRequestList?.add("Status of referred candidate")
            mRequestList?.add("Referral amount")
            val spnAdapter: ArrayAdapter<String>? = activity?.let {
                ArrayAdapter<String>(
                    it,
                    android.R.layout.simple_spinner_dropdown_item,
                    mRequestList!!
                )
            }
            spnAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spnRequest?.setAdapter(spnAdapter)

            spnRequest?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
                ) {
                    mSelectedRequest = parent.getItemAtPosition(position).toString()
                } // to close the onItemSelected

                override fun onNothingSelected(parent: AdapterView<*>) {

                }
            }

            close.setOnClickListener { bottomSheetDialog.dismiss() }
            submit?.setOnClickListener {
                if (mSelectedRequest.equals("Select request")) {
                    Utilities.showToast(activity, "Please select a rquest first.")
                } else {
                    viewModel.raiseEnquiry(
                        userId,
                        edtComment.text?.toString()?.trim(),
                        mSelectedRequest,
                        data?.referral_id
                    )
                    bottomSheetDialog.dismiss()
                }
            }
            val params = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, heightInPixels)
            bottomSheetDialog.setContentView(view, params)
            bottomSheetDialog.show()
        }
    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.responseReferralData.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (it.response?.isEmpty() == false) {
                        linearNoResult?.visibility = View.GONE
                        if (mHeaderList == null) {
                            mHeaderList = ArrayList()
                        } else {
                            mHeaderList?.clear()
                        }
                        mHeaderList?.addAll(it.response)
                        setAdapter()
                    } else {
                        linearNoResult?.visibility = View.VISIBLE
                    }
                }
            }
        })

        viewModel.responseReferralWorkerData.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
//                    if (it.response?.isEmpty() == false) {
//                        mHeaderList?.addAll(it.response)
//                        setAdapter()
//                    }
                    mHeaderAdapter?.setChildAdapter(it.response)
                }
            }
        })
        viewModel.responseRaiseEnquiry.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    Log.e("Raise Enquiry Success", it.response?.enquiryStatus + "")
                    Utilities.showSuccessToast(activity, "Enquiry submitted successfully!")
                }
            }
        })

        viewModel.responseReferraClaim.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    viewModel.getReferralWorkerData(userId, it.response?.jobId ?: 0)
                }
            }
        })

        viewModelExperince.responseSaveData.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    Constants.USER_UPI_ID = upi
                    txtUpi?.text = "Registered UPI ID is ${Constants.USER_UPI_ID}"
                    Utilities.showToast(activity, "UPI Id is updated.")
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })

        viewModelExperince.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelExperince.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> activity?.onBackPressed()
            R.id.txtUpi -> showUpiDialog(Constants.USER_UPI_ID)
            R.id.txtTerms -> {
                Utilities.showAlertTermsPolicy(
                    activity,
                    resources.getString(R.string.tc),
                    resources.getString(R.string.referral_terms)
                )
            }
        }
    }


    private fun showUpiDialog(savedUpi: String?) {
        activity?.let {
            val dialog = Dialog(it)
            dialog.setContentView(R.layout.dialog_salary)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            val title = dialog.findViewById<TextView>(R.id.title)
            val edtUpi = dialog.findViewById<TextInputEditText>(R.id.salary)
            val cancel = dialog.findViewById<TextView>(R.id.cancel)
            val confirm = dialog.findViewById<TextView>(R.id.confirm)
            title.text = "Enter your valid UPI id"
            confirm.text = "Save"
            edtUpi?.setHint("")
            edtUpi?.setFilters(arrayOf<InputFilter>(InputFilter.LengthFilter(50)))
            edtUpi?.gravity = Gravity.START
            edtUpi?.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            edtUpi?.setText(savedUpi ?: "")

            cancel.setOnClickListener {
                dialog.dismiss()
            }
            confirm.setOnClickListener {
                upi = edtUpi?.text?.trim()?.toString()
                val count = upi?.split("@")
                if (upi?.isEmpty() == true) {
                    Utilities.showToast(activity, "Enter your UPI id.")
                } else if ((upi?.length) ?: 0 < 3 || upi?.contains("@") == false || (count?.size
                        ?: 0) != 2
                ) {
                    edtUpi?.setError("Invalid UPI id.")
                } else {
                    dialog.dismiss()
                    Dialogs.showAlertDialog(
                        activity,
                        "Check your UPI ID",
                        "Entered UPI ID is $upi. Do you want to save?",
                        "Yes",
                        "Edit",
                        onAlertPosBtnClick,
                        onAlertNagClick
                    )
                }
            }
            dialog.show()
        }
    }

    private val onAlertNagClick: () -> Unit = {
        showUpiDialog(upi)
    }

    private val onAlertPosBtnClick: () -> Unit = {
        val request = UpdateWorkerRequest(requestedBy = userId, upi_id = upi)
        viewModelExperince.saveWorkerData(workerId, request)
    }
}