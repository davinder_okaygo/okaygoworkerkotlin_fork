package com.okaygo.worker.ui.fragments.on_boarding

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.chip.Chip
import com.okaygo.worker.R
import com.okaygo.worker.adapters.ExperienceAdapter
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.AddExpEvent
import com.okaygo.worker.data.modal.reponse.CategoryResponse
import com.okaygo.worker.data.modal.reponse.Experiences
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.okaygo.worker.ui.fragments.add_exp.AddExperienceFragment
import com.openkey.guest.help.Dialogs
import com.openkey.guest.help.Preferences
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.OnBoardingModel
import kotlinx.android.synthetic.main.fragment_experience.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.File

class ExperienceFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModel: OnBoardingModel
    private val FILE_CHOOSER: Int = 501
    private var cvPath: String? = null
    private var fileName: String? = null
    private var mExpList: ArrayList<Experiences>? = null
    private var mJobsCategories: String? = null
    private var mCurrentSalary: String? = null
    private var mUpdatedSalary: Int? = 0
    private var mExpAdapter: ExperienceAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(OnBoardingModel::class.java)
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_experience, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        mCurrentSalary = Preferences.prefs?.getInt(Constants.LAST_SALARY, 0)?.toString()
        txtSalary?.text = "₹ " + mCurrentSalary
        viewModel.fetchInterestedJobTypes()
        if (Constants.IS_FROM_PROFILE) {
            lnrBottomLayout?.visibility = View.GONE
            imgBackIcon?.visibility = View.VISIBLE
            linearUplaodCv?.visibility = View.GONE
//            txtOptional?.visibility = View.GONE
            txtFileFormat?.visibility = View.GONE
            chFileName?.visibility = View.GONE
//            chkNoExp?.visibility = View.GONE
        } else {
            txtFileFormat?.text =
                "[Optional] " + activity?.resources?.getString(R.string.file_formats)
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getWorkerExp(workerId)
    }


    private val onAlertPosBtnClick: () -> Unit = {
        txtAddExp?.visibility = View.GONE
        txtLastSalary?.visibility = View.GONE
        constraintSalary?.visibility = View.GONE
        recylerExp?.visibility = View.GONE
        if (Constants.IS_FROM_PROFILE == false) {
            chgJobs?.visibility = View.VISIBLE
            txtSelectMaxJob?.visibility = View.VISIBLE
        } else {
//            chkNoExp?.isChecked = true
            viewModel.setNoExp(workerId)
        }
    }
    private val onAlertNagBtnClick: () -> Unit = {
        chkNoExp?.isChecked = false

    }

    /**
     * set click listeners for required views
     */
    private fun setListeners() {
        imgBack?.setOnClickListener(this)
        imgBackIcon?.setOnClickListener(this)
        txtAddExp?.setOnClickListener(this)
        imgEditSalary?.setOnClickListener(this)
        txtLastSalary?.setOnClickListener(this)
        txtUploadCV?.setOnClickListener(this)
        btnSaveContinue?.setOnClickListener(this)

        chFileName?.setOnCloseIconClickListener(View.OnClickListener {
            cvPath = null
            chFileName?.visibility = View.GONE
        })

        chkNoExp?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                if (Constants.IS_FROM_PROFILE == true && mExpList?.isEmpty() == false) {
//                    chkNoExp?.isChecked = false
                    Dialogs.showAlertDialog(
                        activity,
                        "Warning!",
                        getString(R.string.no_exp_alert_msg),
                        "Yes",
                        "No",
                        onAlertPosBtnClick,
                        onAlertNagBtnClick
                    )

                } else {
                    txtAddExp?.visibility = View.GONE
                    txtLastSalary?.visibility = View.GONE
                    constraintSalary?.visibility = View.GONE
                    recylerExp?.visibility = View.GONE
                    if (Constants.IS_FROM_PROFILE == false) {
                        chgJobs?.visibility = View.VISIBLE
                        txtSelectMaxJob?.visibility = View.VISIBLE
                    } else {
                        viewModel.setNoExp(workerId)
                    }
                }
            } else {
                txtAddExp?.visibility = View.VISIBLE
//                constraintSalary?.visibility = View.GONE
//                txtLastSalary?.visibility = View.GONE
                recylerExp.visibility = View.VISIBLE
                if (Constants.IS_FROM_PROFILE == true) {
                    if (mCurrentSalary != null && mCurrentSalary?.isEmpty() == false && mCurrentSalary.equals(
                            "0"
                        ) == false
                    ) {
                        constraintSalary?.setVisibility(View.VISIBLE)
                        txtLastSalary?.setVisibility(View.GONE)
                    } else {
                        txtLastSalary?.setVisibility(View.VISIBLE)
                    }
                }
//                    linearUplaodCv?.visibility = View.VISIBLE
//                    txtOptional?.visibility = View.VISIBLE
//                    txtFileFormat?.visibility = View.VISIBLE
//                }
                chgJobs?.visibility = View.GONE
                txtSelectMaxJob?.visibility = View.GONE
            }
        }
    }

    /**
     * uplaod cv on server and save interested job cat
     */
    private fun uploadCvAndSaveJobs() {
        val sb = StringBuilder()
        for (i in 0 until chgJobs?.childCount!!) {
            val chip: Chip? = chgJobs?.findViewById(chgJobs?.getChildAt(i)?.getId() ?: 0)
            if (chip?.isChecked == true) {
                sb.append(chip?.id.toString() + "")
                sb.append(",")
            }
        }
        Log.e("selected chips", sb.toString() + "")
        mJobsCategories = sb.toString().replace(",$".toRegex(), "")
        if (mJobsCategories?.isEmpty() == true) {
            Utilities.showToast(activity, "Select atlest one job category.")
        } else if (mJobsCategories?.contains(",") == true) {
            val selectedJob: Array<String>? = mJobsCategories?.split(",".toRegex())?.toTypedArray()
            if (selectedJob?.size ?: 0 > 3) {
                Utilities.showToast(activity, "You can select maximum 3 job categories.")
            } else if (cvPath != null && cvPath?.isEmpty() == false) {
                val file = File(cvPath)
                viewModel.getDocLink(userId, file)
            } else {
                viewModel.saveInterestedCat(
                    Preferences.prefs?.getInt(Constants.EMPLOYER_ID, 0),
                    mJobsCategories
                )
            }
        } else {
            if (cvPath != null && cvPath?.isEmpty() == false) {
                val file = File(cvPath)
                viewModel.getDocLink(                                           // get link api call
                    Preferences.prefs?.getInt(Constants.ID, 0),
                    file
                )

            } else {
                viewModel.saveInterestedCat(
                    Preferences.prefs?.getInt(Constants.EMPLOYER_ID, 0),
                    mJobsCategories
                )
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBackIcon,
            R.id.imgBack -> activity?.onBackPressed()
            R.id.txtUploadCV -> {
                if (Utilities.checkStoragePermission(activity)) {
                    showFileChooserCV()
                }
            }

            R.id.txtLastSalary, R.id.imgEditSalary -> Utilities.showSalaryPopUp(
                activity,
                onConfirmSalryClick
            )

            R.id.txtAddExp -> {
                if (Constants.IS_FROM_PROFILE) {
                    attachFragment(AddExperienceFragment(), true)
                } else {
                    addFragment(AddExperienceFragment(), true)
                }
            }

            R.id.btnSaveContinue -> {
                if (chkNoExp?.isChecked() == false) {
                    if (mExpList != null && mExpList?.size ?: 0 > 0) {
                        OkayGoFirebaseAnalytics.on_boarding_experience_details(true)
                        if (cvPath != null) {
                            val file = File(cvPath)
                            viewModel.getDocLink(Preferences.prefs?.getInt(Constants.ID, 0), file)
                        } else {
                            Utilities.showSalaryPopUp(activity, onConfirmSalryClick)
                        }
                    } else {
                        Utilities.showToast(activity, "Add atleast one experience.")
                    }
                } else {
                    OkayGoFirebaseAnalytics.on_boarding_experience_details(false)
                    uploadCvAndSaveJobs()
                }
            }
        }
    }

    /**
     * handle all interested job cat from local server
     */
    private fun handleIntrestedJobCat(response: CategoryResponse?) {
        for (i in 0 until response?.content?.size!!) {
            val tagName: String? = response?.content?.get(i)?.typeDesc ?: ""
            val chip = Chip(activity)

            if (i == 0) {
                chip.isChecked = true
            }
            chip.isCheckable = true
            chip.isCheckedIconVisible = false
            chip.text = tagName
            chip.id = response?.content?.get(i).id ?: 0
            chip.setChipBackgroundColorResource(R.color.bg_chips_drawable)
            chip.setChipStrokeColorResource(R.color.theme)
            chip.chipStrokeWidth = 1f
            chip.isClickable = true
            chip.isChipIconVisible = true

            activity?.let {
                if (tagName?.contains("Chef") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_cook)
                } else if (tagName?.contains("Restaurant") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_resturants)
                } else if (tagName?.contains("Hotel") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_hotel_services)
                } else if (tagName?.contains("Office Job") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic__office_job)
                } else if (tagName?.contains("Delivery") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_delivery)
                } else if (tagName?.contains("Warehouse") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_warehouse)
                } else if (tagName?.contains("Event") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_events)
                } else if (tagName?.contains("Sales") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_sales)
                } else if (tagName?.contains("Customer Support") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_customer_support)
                } else if (tagName?.contains("Education") == true) {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_education)
                } else {
                    chip.closeIcon = ContextCompat.getDrawable(it, R.drawable.ic_other_jobs)
                }
            }
            chip.isCloseIconVisible = true
            chip.isCheckedIconVisible = false
            if (chip.text.toString() != "Other") {
                chgJobs?.addView(chip)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun addExp(addExp: AddExpEvent?) {
        Log.e("isAdded", addExp?.isAdded.toString() + "")
        if (Constants.IS_FROM_PROFILE == false) {
            viewModel.getWorkerExp(workerId)
        }
    }

    /**
     * handle all api's response
     */
    private fun attachObservers() {
        viewModel.responseIntrestedJob.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    handleIntrestedJobCat(it.response)
                }
            }
        })

        viewModel.responseDeleteExp.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    Utilities.showSuccessToast(
                        activity,
                        it.response ?: "Experince Deleted Sucessfully."
                    )
                    viewModel?.getWorkerExp(workerId)
                }
            }
        })

        viewModel.responseGetDocLink.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    viewModel.uplaodCV(
                        it.response,
                        Preferences.prefs?.getInt(Constants.EMPLOYER_ID, 0),
                        fileName
                    )
                }
            }
        })

        viewModel.responseUploadCV.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (chkNoExp?.isChecked == false) {
                        Utilities.showSalaryPopUp(activity, onConfirmSalryClick)
                    } else {
                        viewModel.saveInterestedCat(
                            Preferences.prefs?.getInt(
                                Constants.EMPLOYER_ID,
                                0
                            ), mJobsCategories, false
                        )
                    }
                    Utilities.showSuccessToast(activity, "CV Uploaded Successfully.")

                }
            }
        })

        viewModel.responseSaveCat.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    viewModel.setNoExp(workerId, false)
                }
            }
        })

        viewModel.responseNoExp.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (Constants.IS_FROM_PROFILE == false) {
                        attachFragment(SkillsFragment(), false)
                    } else {
//                        viewModel.getWorkerExp(workerId,false)
                        mExpList?.clear()
                        mExpAdapter?.notifyDataSetChanged()
                    }
                }
            }
        })

        viewModel.responseUpdateSalry.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (Constants.IS_FROM_PROFILE == true) {
                        mCurrentSalary = mUpdatedSalary.toString()
                        Preferences.prefs?.saveValue(Constants.LAST_SALARY, mUpdatedSalary)
                        txtLastSalary?.setVisibility(View.GONE)
                        constraintSalary?.setVisibility(View.VISIBLE)
                        txtSalary?.text = "\u20B9 $mCurrentSalary"

                    } else {
                        Constants.IS_FROM_PROFILE_SKILLS = false
                        attachFragment(SkillsFragment(), false)
                    }
                }
            }
        })

        viewModel.responseGetExp.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (mExpList == null) {
                        mExpList = ArrayList()
                    } else {
                        mExpList?.clear()
                    }
                    if (it.response?.content?.isEmpty() == true) {
                        recylerExp?.visibility = View.GONE
                        if (Constants.IS_FROM_PROFILE == true) {
                            chkNoExp?.visibility = View.VISIBLE
                            chkNoExp?.isChecked = true
                            txtAddExp?.visibility = View.GONE
                            chgJobs?.visibility = View.GONE
                            txtSelectMaxJob?.visibility = View.GONE
                            mCurrentSalary = null
                        }

                    } else {
                        if (Constants.IS_FROM_PROFILE == true) {
                            txtAddExp?.visibility = View.VISIBLE
                            if (mCurrentSalary != null && mCurrentSalary?.isEmpty() == false && mCurrentSalary.equals(
                                    "0"
                                ) == false
                            ) {
                                constraintSalary?.setVisibility(View.VISIBLE)
                                txtLastSalary?.setVisibility(View.GONE)
                            } else {
                                constraintSalary?.setVisibility(View.GONE)
                                txtLastSalary?.setVisibility(View.VISIBLE)
                            }
                        }
                        recylerExp?.visibility = View.VISIBLE
                        it.response?.content?.let { it1 -> mExpList?.add(it1.get(it1.size - 1)) }
                        val linearLayoutManager = LinearLayoutManager(context)
                        mExpAdapter = ExperienceAdapter(
                            context, mExpList,
                            onEditExp,
                            onDeleteExp
                        )
                        recylerExp?.layoutManager = linearLayoutManager
                        recylerExp?.adapter = mExpAdapter
                    }
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    /**
     * click listener when edit any exp
     */
    private val onEditExp: (Experiences?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        Log.e("OnEditExp", it?.companyName.toString())
        val bundle: Bundle? = Bundle()
        val fragment: AddExperienceFragment? = AddExperienceFragment()
        bundle?.putParcelable(Constants.EXP_DATA, it)
        fragment?.arguments = bundle
        fragment?.let { it1 -> attachFragment(it1, true) }
    }

    /**
     * click listener for postive button for salary popup
     */
    private val onConfirmSalryClick: (Int?) -> Unit = { it ->
        mUpdatedSalary = it
        viewModel.updateLastSalary(it, Preferences.prefs?.getInt(Constants.EMPLOYER_ID, 0))
    }

    /**
     * click listener for delete exp
     */
    private val onDeleteExp: (Experiences?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        Log.e("onDeleteExp", it?.companyName.toString())
        val builder = context?.let { it1 -> AlertDialog.Builder(it1) }
        builder?.setMessage(getString(R.string.want_delete))
        builder?.setCancelable(true)
        builder?.setPositiveButton("Yes") { dialog, id ->
            viewModel.deleteExp(it?.experienceId)
        }

        builder?.setNegativeButton("No") { dialog, id -> dialog.cancel() }
        val alert = builder?.create()
        alert?.show()
    }

    /**
     * file choser for cv
     */
    fun showFileChooserCV() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        val mimeTypes = arrayOf(
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "image/*",
            "application/pdf"
        )
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        //        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(
                Intent.createChooser(intent, "Select a File to Upload"),
                FILE_CHOOSER
            )
        } catch (ex: ActivityNotFoundException) {
            // Potentially direct the user to the Market with a Dialog
            Utilities.showToast(activity, "Please install a File Manager.")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == FILE_CHOOSER) {
            // Get the Uri of the selected file
            val uri = data?.data
            cvPath = activity?.let { Utilities.getFilePathForN(uri, it) }
            if (cvPath != null) {
                val file = File(cvPath)
                val file_size: Int = (file.length() / (1024 * 1024)).toString().toInt()
                if (file_size >= 1) {
                    Utilities.showToast(activity, getString(R.string.file_size))
                    cvPath = null
                } else {
                    val name: Array<String>? =
                        cvPath?.split("/".toRegex())?.toTypedArray()
                    fileName = name?.get(name.size - 1)
                    chFileName?.visibility = View.VISIBLE
                    chFileName?.text = fileName
                }
                Log.e("Work Experience", "File Path: $cvPath")
                Log.e("Work Experience", "File file_size: $file_size")
            }
        }
    }
}