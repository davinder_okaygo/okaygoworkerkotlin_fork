package com.okaygo.worker.ui.activity

import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.firebase.installations.FirebaseInstallations
import com.okaygo.worker.R
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.help.LanguageHelper
import com.okaygo.worker.ui.fragments.LanguageSelectionFragment
import com.okaygo.worker.ui.fragments.TutorialFragment
import com.okaygo.worker.ui.fragments.splash.SplashFragment
import com.openkey.guest.application.OkayGo
import com.openkey.guest.help.Preferences
import com.openkey.guest.help.utils.Constants

class LauncherActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_launcher)
        proceed()
        FirebaseInstallations.getInstance().getId()
            .addOnSuccessListener(object : OnSuccessListener<String?> {
                override fun onSuccess(s: String?) {
                    Log.e("Install Id", s + "")
                }
            })
//        try {
//            val info = getPackageManager().getPackageInfo(
//                "com.okaygo.worker",
//                PackageManager.GET_SIGNATURES
//            );
//            for (i in 0..info.signatures?.size!!-1) {
//                val md = MessageDigest.getInstance("SHA");
//                md.update(info.signatures?.get(i)?.toByteArray());
//                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (e: PackageManager.NameNotFoundException) {
//
//        } catch (e: NoSuchAlgorithmException) {
//
//        }
    }

    /**
     * Making notification bar transparent
     */
    private fun changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.TRANSPARENT
        }
    }

    /**
     * go to dashboard screen
     */
    private fun proceed() {
        val isFirstTime = Preferences.prefs?.getBoolean(Constants.IS_FIRST_TIME_LAUNCH, false)
        val isUtmAccessStatus = Preferences.prefs?.getBoolean(Constants.UTM_ACCESS_STATUS, false)
        // Checking for first time launch - before calling setContentView()
        LanguageHelper.getDefaultLang(this)
        if (isFirstTime == true) {
            addFragment(SplashFragment(), false)
        } else {
            if (Preferences.prefs?.getBoolean(Constants.LANG_SELECTION, false) == true) {
                addFragment(TutorialFragment(), false)
            } else {
                addFragment(LanguageSelectionFragment(), false)
            }
        }
        Preferences.prefs?.clearValue(Constants.REFERED_BY)
        checkForDynamicLink()
        // making notification bar transparent
        changeStatusBarColor()
        OkayGo.firebaseAnalytics?.setUserId(
            Preferences.prefs?.getInt(Constants.ID, 0).toString() + ""
        )

        OkayGoFirebaseAnalytics.commonData()
    }


    fun checkForDynamicLink() {
        FirebaseDynamicLinks.getInstance()
            .getDynamicLink(intent)
            .addOnSuccessListener { pendingDynamicLinkData ->
                var deepLink: Uri? = null
                if (pendingDynamicLinkData != null) {
                    deepLink = pendingDynamicLinkData.link
                    Log.e("firbaseDynamic link", deepLink.toString() + "")
                }
                if (deepLink != null) {
                    val jobId = deepLink.getQueryParameter("job_id")
                    val worker_id = deepLink.getQueryParameter("worker_id")
                    val app_direct = deepLink.getQueryParameter("app_direct")
                    val jobDetailId =
                        deepLink.getQueryParameter("job_detail_id")
                    val utm_source = deepLink.getQueryParameter("utm_source")
                    val utm_medium = deepLink.getQueryParameter("utm_medium")
                    val utm_content = deepLink.getQueryParameter("utm_content")
                    val utm_campaign =
                        deepLink.getQueryParameter("utm_campaign")
                    val refer_by = deepLink.getQueryParameter("refer_by")
                    val utm_term = deepLink.getQueryParameter("utm_term")
                    Log.e(
                        "Deeplink",
                        """
                            refer_by= ${refer_by}utm_source= $utm_source
                            utm_medium= $utm_medium
                            utm_content= $utm_content
                            utm_campaign= $utm_campaign
                            utm_term= $utm_term
                            """.trimIndent()
                    )
                    Log.e(
                        "Deeplink utm",
                        "Job_id $jobId,w= $worker_id,ap $app_direct"
                    )
                    if (jobId != null && !jobId.isEmpty()) {
                        Preferences.prefs?.saveValue(Constants.IS_FOR_DEEPLINK, true)
                        if (refer_by != null && !refer_by.isEmpty()) {
                            OkayGoFirebaseAnalytics.deeplinkClickReferJob(
                                jobId,
                                jobDetailId,
                                refer_by,
                                utm_source,
                                utm_medium,
                                utm_content,
                                utm_campaign,
                                utm_term
                            )
                        }
                    }
                    if (refer_by == null || refer_by.isEmpty()) {
                        OkayGoFirebaseAnalytics.deeplinkClick(
                            utm_source,
                            utm_medium,
                            utm_content,
                            utm_campaign,
                            utm_term
                        )
                    }
                    Preferences.prefs?.saveValue(
                        Constants.JOB_DETAIL_ID_DEEPLINK,
                        jobDetailId
                    )
                    Preferences.prefs?.saveValue(Constants.JOB_ID, jobId)
                    Preferences.prefs?.saveValue(Constants.REFERED_BY, refer_by)
                    Preferences.prefs?.saveValue(Constants.WORKER_ID, worker_id)
                    Preferences.prefs?.saveValue(Constants.APP_DIRECT, app_direct)
                }
                if (deepLink != null && deepLink.getBooleanQueryParameter("invitedby", false)) {
                    val referrerUid = deepLink.getQueryParameter("invitedby")
                    Preferences.prefs?.saveValue(Constants.REFERED_BY, referrerUid)
                } else {
                    //Toast.makeText(MainActivity.this, "No Dynamic Link Found", Toast.LENGTH_SHORT).show();
                }
            }
    }
}