package com.okaygo.worker.ui.fragments.show_post_application_info

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.textfield.TextInputEditText
import com.okaygo.worker.R
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.JobApplySuccess
import com.okaygo.worker.data.modal.reponse.JobContent
import com.okaygo.worker.data.modal.reponse.QuestionareResponseItem
import com.okaygo.worker.data.modal.reponse.ReqiredSkillsResponse
import com.okaygo.worker.data.modal.reponse.WorkerDetails
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.okaygo.worker.ui.fragments.complete_post_apply.CompleteInfoFragment
import com.okaygo.worker.ui.fragments.experience.ExperienceAddFragment
import com.okaygo.worker.ui.fragments.questionare.QuestionareFragment
import com.openkey.guest.help.Dialogs
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.JobListModel
import com.openkey.guest.ui.fragments.verification.PaymentDetailModel
import com.openkey.guest.ui.fragments.verification.ProfileModel
import com.openkey.guest.ui.fragments.verification.QuestionareModel
import kotlinx.android.synthetic.main.fragment_show_application_info.*
import org.greenrobot.eventbus.EventBus

class ShowCompleteInfoFragment : BaseFragment() {

    private lateinit var viewModelProfile: ProfileModel
    private lateinit var viewModelJobList: JobListModel
    private lateinit var viewModelQues: QuestionareModel
    private lateinit var viewModelPaymentDetail: PaymentDetailModel

    private var mJobData: JobContent? = null
    private var mAssignId: Int? = null
    private var mQuestList: ArrayList<QuestionareResponseItem>? = null
    var mRequiredData: ReqiredSkillsResponse? = null
    var isNotOnlyForExp = false
    private var mCvLink: String? = null
    private var mCvFileName: String? = null
    private var upi: String? = ""
    private var isDialogForUpi = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelProfile = ViewModelProvider(this).get(ProfileModel::class.java)
        viewModelPaymentDetail = ViewModelProvider(this).get(PaymentDetailModel::class.java)
        viewModelJobList = ViewModelProvider(this).get(JobListModel::class.java)
        viewModelQues = ViewModelProvider(this).get(QuestionareModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_show_application_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundle: Bundle? = arguments
        if (bundle?.containsKey(Constants.POST_APPLY_JOB_DATA) == true) {
            mJobData = bundle.getSerializable(Constants.POST_APPLY_JOB_DATA) as JobContent
        }
        if (bundle?.containsKey(Constants.POST_APPLY_REQUIRED_DATA) == true) {
            mRequiredData =
                bundle.getSerializable(Constants.POST_APPLY_REQUIRED_DATA) as ReqiredSkillsResponse
            mAssignId = bundle.getInt(Constants.POST_Assign_id)
        }
        showHideFields()
        setListeners()
        OkayGoFirebaseAnalytics.postApplicationAllInfoComplete(
            mJobData?.jobId ?: Constants.DEEPLINK_JOB_ID
        )
        viewModelProfile.getWorkerByUserId(userId)
        viewModelQues.getJobQuestions(mJobData?.jobId ?: Constants.DEEPLINK_JOB_ID)
    }

    private fun showHideFields() {
        if (mRequiredData?.response?.isEmpty() == false) {

            for (i in 0..(mRequiredData?.response?.size ?: 0) - 1) {
                when (mRequiredData?.response?.get(i)?.skillId) {
                    Constants.USER_EDUCATION -> {
                        isNotOnlyForExp = true
                        txtEduTitle?.visibility = View.VISIBLE
                        txtEducation?.visibility = View.VISIBLE
                    }

                    Constants.USER_TOTAL_EXP -> {
                        txtExpTitle?.visibility = View.VISIBLE
                        txtExp?.visibility = View.VISIBLE
                    }

                    Constants.USER_LANG -> {
                        isNotOnlyForExp = true
                        txtEngTitle?.visibility = View.VISIBLE
                        txtEngLevel?.visibility = View.VISIBLE
                    }
                    Constants.USER_CV -> {
                        txtCVTitle?.visibility = View.VISIBLE
                        chFileName?.visibility = View.VISIBLE
                        imgDownload?.visibility = View.VISIBLE
                    }

                    Constants.USER_NOTICE -> {
                        txtNoticeTitle?.visibility = View.VISIBLE
                        txtNotice?.visibility = View.VISIBLE
                    }

                    Constants.USER_LAST_SALARY -> {
                        txtLastSalTitle?.visibility = View.VISIBLE
                        txtLastSalary?.visibility = View.VISIBLE
                    }

                    Constants.USER_LAPTOP,
                    Constants.USER_SMARTPHONE,
                    Constants.USER_WIFI,
                    Constants.USER_BIKE,
                    Constants.USER_DL,
                    Constants.USER_RC,
                    Constants.USER_ADHAAR,
                    Constants.USER_PAN -> {
                        isNotOnlyForExp = true
                        txtDocTitle?.visibility = View.VISIBLE
                        txtDocuments?.visibility = View.VISIBLE
                    }
                    Constants.USER_EXP -> {
                        txtLastExpTitle?.visibility = View.VISIBLE
                        txtLastExp?.visibility = View.VISIBLE
                    }

                    Constants.USER_SKILLS -> {
                        isNotOnlyForExp = true
                        txtSkillTitle?.visibility = View.VISIBLE
                        txtSkills?.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun setListeners() {
        txtEditDetail?.setOnClickListener {
            val bundle: Bundle? = Bundle()
            var fragment: BaseFragment? = null
            if (isNotOnlyForExp) {
                fragment = CompleteInfoFragment()
            } else {
                fragment = ExperienceAddFragment()
            }
            if (mJobData != null) {
                bundle?.putSerializable(Constants.POST_APPLY_JOB_DATA, mJobData)
            }
            bundle?.putSerializable(Constants.POST_APPLY_REQUIRED_DATA, mRequiredData)
            bundle?.putInt(Constants.POST_Assign_id, mAssignId ?: 0)
            fragment.arguments = bundle
            fragment.let { it1 -> attachFragment(it1, true) }
        }

        txtNext?.setOnClickListener {
            if (mQuestList?.isEmpty() == false) {
                val bundle: Bundle? = Bundle()
                val fragment = QuestionareFragment()
                if (mJobData != null) {
                    bundle?.putSerializable(Constants.POST_APPLY_JOB_DATA, mJobData)
                }
                bundle?.putInt(Constants.POST_Assign_id, mAssignId ?: 0)
                fragment.arguments = bundle
                attachFragment(fragment, true)
            } else {
                viewModelJobList.applyJob(mAssignId, userId, Constants.DEEPLINK_REFFER_BY)
            }
        }

        imgDownload?.setOnClickListener {
            isDialogForUpi = 2
            Dialogs.showAlertDialog(
                activity,
                resources.getString(R.string.cv_download),
                resources.getString(R.string.cv_download_msg),
                "Yes",
                "No",
                onAlertPosBtnClick,
                onAlertNagClick
            )
        }

//        chFileName?.setOnCloseIconClickListener(View.OnClickListener {
//            mCvFileName=null
//            chFileName?.text=""
//            imgDownload?.visibility = View.GONE
//            imgDownload?.visibility = View.GONE
//        })

    }


    private fun setDataToInfo(response: WorkerDetails?) {
        val skills: StringBuilder? = StringBuilder()
        for (i in 0..(response?.userSkills?.size ?: 0) - 1) {
            skills?.append(response?.userSkills?.get(i)?.skillsName)
            skills?.append(", ")
        }
        txtExp?.text = response?.total_experience?.toString()
        txtEducation?.text = response?.qualification ?: "Not available"
        txtEngLevel?.text = Utilities.getEnglishLevel(response?.english_known_level ?: 0, activity)
        txtSkills?.text = skills?.toString()?.trim()?.replace(",$", "")

        if (response?.workerExperiences?.isEmpty() == false) {
            val lastExpIndex = response.workerExperiences.size - 1
            val lastExp = response.workerExperiences.get(lastExpIndex)
            val expValue =
                "${lastExp.industryName}\n ${lastExp.companyName}"
            txtLastExp?.text = expValue
        }

        txtLastSalary?.text = "₹ ${response?.last_salary ?: 0}/month"
        txtNotice?.text = "Notice period: ${response?.notice_period ?: "Immidiate"}"
        if (response?.cv_link?.isEmpty() == false && response.cv_file_name?.isEmpty() == false) {
            mCvFileName = response.cv_file_name
            chFileName?.text = mCvFileName
            mCvLink = response.cv_link
        } else {
            txtCVTitle?.visibility = View.GONE
            chFileName?.visibility = View.GONE
            imgDownload?.visibility = View.GONE
        }

        val ownerShip: StringBuilder? = StringBuilder()
        if (response?.own_bike == 1) {
            ownerShip?.append("Bike, ")
        }

        if (response?.bike_license == 1) {
            ownerShip?.append("Bike License, ")
        }
//
//        if (response?.car_license == 1) {
//            ownerShip?.append("Car License, ")
//        }

        if (response?.own_laptop == 1) {
            ownerShip?.append("Laptop, ")
        }

        if (response?.own_aadhar_card == 1) {
            ownerShip?.append("Aadhar, ")
        }

        if (response?.own_pan_card == 1) {
            ownerShip?.append("PAN Card, ")
        }

        if (response?.own_smartphone == 1) {
            ownerShip?.append("Smartphone, ")
        }

        if (response?.own_vehicle_rc == 1) {
            ownerShip?.append("RC, ")
        }

        if (response?.own_wifi == 1) {
            ownerShip?.append("Wifi, ")
        }

        txtDocuments?.text = ownerShip?.toString()?.trim()?.replace(",$", "")
    }

    /**
     * handle api reposne
     */
    private fun attachObservers() {
        viewModelProfile.responseWorkerData.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS && it.response?.content?.isEmpty() == false) {
                    setDataToInfo(it.response.content.get(0))
                }
            }
        })

        viewModelQues.responseJobQuestions.observe(this, Observer {
            it?.let {
                if (!it.isEmpty()) {
                    if (mQuestList == null) {
                        mQuestList = ArrayList()
                    } else {
                        mQuestList?.clear()
                    }
                    mQuestList?.addAll(it)
                } else {
                    viewModelPaymentDetail.getUPI(workerId)
                    txtNext?.text = "APPLY"
                }
            }
        })
        viewModelJobList.responseApplyJob.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    if (it.code == Constants.SUCCESS) {
                        var workType: String? = ""
                        if (mJobData == null) {
                            OkayGoFirebaseAnalytics.on_job_apply(
                                Constants.DEEPLINK_WORK_TYPE,
                                Constants.DEEPLINK_JOB_TYPE,
                                Constants.DEEPLINK_JOB_DETAIL_ID?.toString(),
                                "apply"
                            )
                            workType = Constants.DEEPLINK_WORK_TYPE
                        } else {
                            OkayGoFirebaseAnalytics.on_job_apply(
                                mJobData?.workType,
                                mJobData?.jobType,
                                mJobData?.jobDetailId?.toString(),
                                "apply"
                            )
                            workType = mJobData?.workType
                        }
                        if (workType.equals("ON DEMAND", true) && upi?.isEmpty() == true) {
                            isDialogForUpi = 0
                            Dialogs.showAlertDialog(
                                activity,
                                "Job applied",
                                activity?.resources?.getString(R.string.applied_msg_od),
                                "UPDATE",
                                "LATER",
                                onAlertPosBtnClick,
                                onAlertNagClick
                            )
                        } else {
                            Utilities.jobSuccessDialog(
                                activity,
                                activity?.resources?.getString(R.string.job_applied),
                                activity?.resources?.getString(R.string.applied_success_descriotion),
                                true, onOKClick
                            )
                        }
                    }
                }
            }
        })

        viewModelPaymentDetail.responseSaveUPI.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    upi = it.response
                    Utilities.showToast(activity, "UPI Id is updated.")
                    EventBus.getDefault().post(JobApplySuccess(true))
                    activity?.finish()
                }
            }
        })
        viewModelPaymentDetail.responseGetUPI.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    upi = it.response ?: ""
                }
            }
        })

        viewModelProfile.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelProfile.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })

        viewModelJobList.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelJobList.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })

        viewModelPaymentDetail.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelPaymentDetail.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    private val onAlertNagClick: () -> Unit = {
        if (isDialogForUpi == 1) {
            showUpiDialog(upi)
        } else if (isDialogForUpi == 0) {
            EventBus.getDefault().post(JobApplySuccess(true))
            activity?.finish()
        } else {

        }
    }

    private val onAlertPosBtnClick: () -> Unit = {
        if (isDialogForUpi == 0) {
            showUpiDialog(upi)
        } else if (isDialogForUpi == 1) {
            viewModelPaymentDetail.updateUPI(workerId, upi)
        } else {
            if (Utilities.checkStoragePermission(activity)) {
                Utilities.downloadAnyFile(activity, mCvLink, mCvFileName)
            }
        }
    }

    private val onOKClick: () -> Unit = {
        EventBus.getDefault().post(JobApplySuccess(true))
        activity?.finish()
    }


    private fun showUpiDialog(savedUpi: String?) {
        activity?.let {
            val dialog = Dialog(it)
            dialog.setContentView(R.layout.dialog_salary)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            val title = dialog.findViewById<TextView>(R.id.title)
            val edtUpi = dialog.findViewById<TextInputEditText>(R.id.salary)
            val cancel = dialog.findViewById<TextView>(R.id.cancel)
            val confirm = dialog.findViewById<TextView>(R.id.confirm)
            title.text = "Enter your valid UPI id"
            confirm.text = "Save"
            edtUpi?.setHint("")
            edtUpi?.setFilters(arrayOf<InputFilter>(InputFilter.LengthFilter(50)))
            edtUpi?.gravity = Gravity.START
            edtUpi?.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
            edtUpi?.setText(savedUpi ?: "")

            cancel.setOnClickListener {
                dialog.dismiss()
                EventBus.getDefault().post(JobApplySuccess(true))
                activity?.finish()
            }
            confirm.setOnClickListener {
                upi = edtUpi?.text?.trim()?.toString()
                val count = upi?.split("@")
                if (upi?.isEmpty() == true) {
                    Utilities.showToast(activity, "Enter your UPI id.")
                } else if ((upi?.length) ?: 0 < 3 || upi?.contains("@") == false || (count?.size
                        ?: 0) != 2
                ) {
                    edtUpi?.setError("Invalid UPI id.")
                } else {
                    dialog.dismiss()
                    isDialogForUpi = 1
                    Dialogs.showAlertDialog(
                        activity,
                        "Check your UPI ID",
                        "Entered UPI ID is $upi. Do you want to save?",
                        "Yes",
                        "Edit",
                        onAlertPosBtnClick,
                        onAlertNagClick
                    )
                }
            }
            dialog.show()
        }
    }

}