package com.okaygo.worker.ui.activity.login_onboarding

import android.os.Bundle
import com.okaygo.worker.R
import com.okaygo.worker.ui.activity.BaseActivity
import com.okaygo.worker.ui.fragments.on_boarding.PersonalDetailFragment
import com.openkey.guest.help.utils.Constants

class OnBoardingActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activiy_on_boarding)
        Constants.IS_FROM_PROFILE = false
        addFragment(PersonalDetailFragment(), false)
    }
}