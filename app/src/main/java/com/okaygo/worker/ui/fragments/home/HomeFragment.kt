package com.okaygo.worker.ui.fragments.home

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.blankj.utilcode.util.ScreenUtils
import com.github.guilhe.views.SeekBarRangedView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.okaygo.worker.R
import com.okaygo.worker.adapters.HomeAdtapter
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.ApplyJobEventDeepLink
import com.okaygo.worker.data.modal.reponse.ExpFilter
import com.okaygo.worker.data.modal.reponse.JobCategories
import com.okaygo.worker.data.modal.reponse.JobCity
import com.okaygo.worker.data.modal.request.JobCategoryRequest
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.activity.post_application.PostApplicationActivity
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.Preferences
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.HomeModel
import com.openkey.guest.ui.fragments.verification.JobListModel
import com.openkey.guest.ui.fragments.verification.PasswordModel
import kotlinx.android.synthetic.main.fragment_home.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModel: HomeModel
    private lateinit var viewModelPasswordModel: PasswordModel
    private lateinit var viewModelJobList: JobListModel
    private var mHeaderList: ArrayList<JobCategories>? = null
    private var mHeaderAdapter: HomeAdtapter? = null
    private var type = "All Jobs"
    private var txtTime: AppCompatTextView? = null
    private var txtPayTitle: AppCompatTextView? = null
    private var txtPay: AppCompatTextView? = null
    var mPaySeekbar: SeekBarRangedView? = null
    var mTimeSeekBar: SeekBarRangedView? = null
    var cpgExperience: ChipGroup? = null
    var cpgCities: ChipGroup? = null
    private var layoutPay: LinearLayoutCompat? = null
    var mStartTime = -1
    var mEndTime = -1
    var mMinAmount = -1
    var mMaxAmount = -1
    var selected = ""

    var expSelected = ""
    var citySelected = ""
    var alreadyCitySelected = ""
    var alreadyExpSelected = ""
    private var mDeeplinkJobData: ApplyJobEventDeepLink? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(HomeModel::class.java)
        viewModelJobList = ViewModelProvider(this).get(JobListModel::class.java)
        viewModelPasswordModel = ViewModelProvider(this).get(PasswordModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
        Constants.BOTTOM_MENU_ITEM_SELECTED = 1
        OkayGoFirebaseAnalytics?.find_job_categories()
        setAdapterForheader()
        val request: JobCategoryRequest? = JobCategoryRequest(
            userId,
            Constants.isOnDemand,
            Constants.isPt,
            Constants.isFt,
            Constants.maxPay,
            Constants.minMay,
            Constants.jobType,
            Constants.jobAfter,
            Constants.cityFilter,
            Constants.expFilter,
            Constants.jobBefore
        )
        viewModel.getJobCategories(request)
        setListetenrs()
    }

    private fun setAdapterForheader() {
        mHeaderList = ArrayList()
        val linearLayoutManager = LinearLayoutManager(activity)
        mHeaderAdapter = HomeAdtapter(activity, mHeaderList)
        recyclerFindJobHeader?.layoutManager = linearLayoutManager
        recyclerFindJobHeader?.adapter = mHeaderAdapter
    }

    override fun onResume() {
        super.onResume()
        viewModelPasswordModel.getWorker(userId, false)
        CheckActiveFilters()
        initlize()
    }

    private fun setListetenrs() {
        txtCurrentJobType?.setOnClickListener(this)
        imgFilter?.setOnClickListener(this)
        swAvailbaleToday?.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            Log.e("Current Time", Utilities.getCurrentTime() + "")
            if (b) {
                Constants.isToggle = true
//                mOkayGoFirebaseAnalytics?.availability_today_on(General.getCurrentTime())
                viewModelJobList.saveSuperAvailability(userId, 1, null, workerId)
                txtAvailableText?.setText(activity?.resources?.getString(R.string.want_work_today))
            } else {
                Constants.isToggle = false
//                mOkayGoFirebaseAnalytics?.availability_today_off(General.getCurrentTime())
                viewModelJobList.saveSuperAvailability(userId, 0, null, workerId)
                txtAvailableText?.setText(R.string.want_work_today)
            }
        })

    }

    //
    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }


    private fun setJobType() {
        activity?.let {
            val view = it.layoutInflater.inflate(R.layout.bottomsheet_select_job_type, null)
            val bottomSheetDialog = BottomSheetDialog(it)
            val heightInPixels = (ScreenUtils.getScreenHeight() / 1.5).toInt()
            val radioGroup = view.findViewById<RadioGroup>(R.id.rdJobType)
            val confirm = view.findViewById<AppCompatTextView>(R.id.txtConfirm)
            val close = view.findViewById<AppCompatImageView>(R.id.imgClose)
            val od = view.findViewById<RadioButton>(R.id.on_demand)
            val ft = view.findViewById<RadioButton>(R.id.full_time)
            val pt = view.findViewById<RadioButton>(R.id.part_time)
            val al = view.findViewById<RadioButton>(R.id.all)

            if (txtCurrentJobType?.getText().toString() == "On demand") {
                od.isChecked = true
            } else if (txtCurrentJobType?.getText().toString() == "Full time") {
                ft.isChecked = true
            } else if (txtCurrentJobType?.getText().toString() == "Part time") {
                pt.isChecked = true
            } else if (txtCurrentJobType?.getText().toString() == "All Jobs") {
                al.isChecked = true
            }

            close.setOnClickListener { bottomSheetDialog.dismiss() }
            confirm.setOnClickListener {
                if (radioGroup.checkedRadioButtonId != -1) {
                    val radioButton =
                        radioGroup.findViewById<RadioButton>(radioGroup.checkedRadioButtonId)
                    txtCurrentJobType?.setText(radioButton.text)
                    type = radioButton.text.toString()
                    Preferences.prefs?.saveValue(Constants.HOME_JOB_FILTER, type)
                    filterReset(true)
                    FindJobsByType()
                }
                OkayGoFirebaseAnalytics.find_jobs_change_job_type(type)
                bottomSheetDialog.dismiss()
            }
            val params = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, heightInPixels)
            bottomSheetDialog.setContentView(view, params)
            bottomSheetDialog.show()
        }
    }

    fun filterReset(isForJobType: Boolean? = false) {
        //Time filter reset
        Constants.time_active = false
        Constants.start_time = 0
        Constants.end_time = 0

        //Payment Filter
        Constants.pay_active = false
        Constants.Minimum_Pay = 0
        Constants.Maximum_Pay = 0

        //Job type filter
        Constants.job_type_active = false
        Constants.job_type_ids = null
        Constants.job_type_count = 0

        //Job exp filter
        Constants.exp_active = false
        Constants.exp_filters = null
        Constants.exp_filters_count = 0

        //Job city filter
        Constants.city_active = false
        Constants.city_filters = null
        Constants.city_filters_count = 0
        CheckActiveFilters()
        if (isForJobType == false) {
            Utilities.showSuccessToast(activity, "Filter Reset")
        }
    }

    private fun OnFilterClick() {
        var isFilterUpdate = false
        val view: View? = activity?.layoutInflater?.inflate(R.layout.bottomsheet_filter, null)
        val bottomSheetDialog = activity?.let { BottomSheetDialog(it) }
        bottomSheetDialog?.setCancelable(false)
        val txtClearFilter = view?.findViewById<AppCompatTextView>(R.id.txtClearFilter)
        mPaySeekbar = view?.findViewById(R.id.pay_seekbar)
        txtPay = view?.findViewById(R.id.txtPay)
        mTimeSeekBar = view?.findViewById(R.id.time_seek_bar)
        txtTime = view?.findViewById(R.id.txtTime)
        val txtApply = view?.findViewById<AppCompatButton>(R.id.btnApply)
        cpgExperience = view?.findViewById<ChipGroup>(R.id.cpgExperience)
        cpgCities = view?.findViewById<ChipGroup>(R.id.cpgCities)
        val btnCancel = view?.findViewById<AppCompatTextView>(R.id.txtCancel)
        txtPayTitle = view?.findViewById(R.id.txtPayTitle)
        layoutPay = view?.findViewById<LinearLayoutCompat>(R.id.layoutPay)
        setExpFilter()
        viewModelJobList.getJobCities(userId)

        if (Preferences.prefs?.getString(Constants.HOME_JOB_FILTER, null).equals("On demand")) {
            txtPayTitle?.setVisibility(View.GONE)
            txtPay?.setVisibility(View.GONE)
            layoutPay?.setVisibility(View.GONE)
        } else {
            txtPayTitle?.setVisibility(View.VISIBLE)
            txtPay?.setVisibility(View.VISIBLE)
            layoutPay?.setVisibility(View.VISIBLE)
            txtPay?.setText("\u20B9  - ₹ 50000 +")
            txtPayTitle?.setText("Monthly Pay")
        }

        if (Constants.pay_active) {
            mPaySeekbar?.setSelectedMinValue(Constants.Minimum_Pay.toFloat())
            mPaySeekbar?.setSelectedMaxValue(Constants.Maximum_Pay.toFloat())
            if (Constants.Maximum_Pay > 10) {
                txtPay?.setText("₹ " + Constants.Minimum_Pay * 5000 + " - ₹ 50000+ ")
            } else {
                txtPay?.setText("₹ " + Constants.Minimum_Pay * 5000 + " - ₹ " + Constants.Maximum_Pay * 5000)
            }
        }

        if (Constants.time_active) {
            mTimeSeekBar?.setSelectedMinValue(Constants.start_time.toFloat())
            if (Constants.end_time == 23) {
                mTimeSeekBar?.setSelectedMaxValue(24f)
                txtTime?.setText(Utilities.convertTimeToAmPm(Constants.start_time.toString() + ":00:00") + " - " + "11:59 PM")
            } else {
                mTimeSeekBar?.setSelectedMaxValue(Constants.end_time.toFloat())
                txtTime?.setText(
                    Utilities.convertTimeToAmPm(Constants.start_time.toString() + ":00:00") + " - " + Utilities.convertTimeToAmPm(
                        Constants.end_time.toString() + ":00:00"
                    )
                )
            }
        }

        btnCancel?.setOnClickListener {

            citySelected = ""
            for (i in 0 until cpgCities?.getChildCount()!!) {
                val chip = cpgCities?.getChildAt(i) as Chip
                if (chip.isChecked) {
                    if (citySelected == "") {
                        citySelected = citySelected + chip.id
                    } else {
                        citySelected = citySelected + "," + chip.id
                    }
                }
            }

            expSelected = ""
            for (i in 0 until cpgExperience?.getChildCount()!!) {
                val chip = cpgExperience?.getChildAt(i) as Chip
                if (chip.isChecked) {
                    if (expSelected == "") {
                        expSelected = expSelected + chip.id
                    } else {
                        expSelected = expSelected + "," + chip.id
                    }
                }
            }

            if (isFilterUpdate == true || !alreadyCitySelected.equals(citySelected) || !alreadyExpSelected.equals(
                    expSelected
                )
            ) {
                bottomSheetDialog?.let { it1 -> showAlert(it1) }
            } else {
                bottomSheetDialog?.dismiss()
            }
            isFilterUpdate = false

        }

        txtApply?.setOnClickListener {
            isFilterUpdate = false
            applyFilter(bottomSheetDialog)
        }

        mTimeSeekBar?.setOnSeekBarRangedChangeListener(object :
            SeekBarRangedView.OnSeekBarRangedChangeListener {
            override fun onChanged(view: SeekBarRangedView, minValue: Float, maxValue: Float) {
                isFilterUpdate = true
                val start = Math.round(minValue)
                val end = Math.round(maxValue)
                Log.e("value", "$start $end")
                mStartTime = start
                if (end == 24) {
                    mEndTime = end - 1
                } else {
                    mEndTime = end
                }
            }

            override fun onChanging(view: SeekBarRangedView, minValue: Float, maxValue: Float) {
                val start = Math.round(minValue)
                val end = Math.round(maxValue)
                if (end - start < 4) {
                    Constants.time_active = false
                    Toast.makeText(
                        activity,
                        "Invalid Range. \n Minimum Job Duration is 4 Hours.",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    val start_time: String
                    val end_time: String
                    if (end == 24) {
                        start_time = Utilities.convertTimeToAmPm("$start:00:00").toString()
                        end_time = "11:59 PM"
                        txtTime?.setText("$start_time - $end_time")
                    } else {
                        start_time = Utilities.convertTimeToAmPm("$start:00:00").toString()
                        end_time = Utilities.convertTimeToAmPm("$end:00:00").toString()
                        txtTime?.setText("$start_time - $end_time")
                    }
                }
            }
        })

        mPaySeekbar?.setOnSeekBarRangedChangeListener(object :
            SeekBarRangedView.OnSeekBarRangedChangeListener {
            override fun onChanged(view: SeekBarRangedView, minValue: Float, maxValue: Float) {
                isFilterUpdate = true
            }

            override fun onChanging(view: SeekBarRangedView, minValue: Float, maxValue: Float) {
                if (txtCurrentJobType?.getText() == "On demand") {
                    Constants.pay_active = false
                } else {
                    val min = 5000 * Math.round(minValue)
                    val max = 5000 * Math.round(maxValue)
                    if (max > 50000) {
                        txtPay?.setText("\u20B9 " + min + " - " + "\u20B9 50000 +")
                    } else {
                        txtPay?.setText("\u20B9 " + min + " - " + "\u20B9 " + max)
                    }
                    mMinAmount = Math.round(minValue)
                    mMaxAmount = Math.round(maxValue)
                }
            }
        })

        txtClearFilter?.setOnClickListener { //                //Time filter reset
            Constants.time_active = false
            Constants.start_time = 0
            Constants.end_time = 0

            //Payment Filter
            Constants.pay_active = false
            Constants.Minimum_Pay = 0
            Constants.Maximum_Pay = 0

            //Job type filter
            Constants.job_type_active = false
            Constants.job_type_ids = null
            Constants.job_type_count = 0

            //Job exp filter
            Constants.exp_active = false
            Constants.exp_filters = null
            Constants.exp_filters_count = 0
            //Job cit filter
            Constants.city_active = false
            Constants.city_filters = null
            Constants.city_filters_count = 0
            bottomSheetDialog?.dismiss()
            CheckActiveFilters()
            Utilities.showSuccessToast(activity, "Filter Reset")
            FindJobsByType()
        }

        val params = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            (ScreenUtils.getScreenHeight() / 1)
        )
        view?.let { bottomSheetDialog?.setContentView(it, params) }
        bottomSheetDialog?.show()
    }

    private fun showAlert(bottomSheetDialog: BottomSheetDialog) {
        // Create custom dialog object
        val dialog = activity?.let { Dialog(it) }
        dialog?.setContentView(R.layout.dialog_box_custom)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val message = dialog?.findViewById<View>(R.id.txtMsg) as AppCompatTextView
        val cancel = dialog.findViewById<View>(R.id.txtCancel) as AppCompatTextView
        val confirm = dialog.findViewById<View>(R.id.txtConfirm) as AppCompatTextView
        message.setText(R.string.data_not_saved)
        cancel.text = "No"
        confirm.text = "Save"
        dialog.setCancelable(false)
        dialog.show()

        // if decline button is clicked, close the custom dialog
        confirm.setOnClickListener { // Close dialog
            dialog.dismiss()
            applyFilter(bottomSheetDialog)
        }

        // if decline button is clicked, close the custom dialog
        cancel.setOnClickListener { // Close dialog
            dialog.dismiss()
            bottomSheetDialog.dismiss()
        }
    }

    private fun setExpFilter() {
        val expFilterList: ArrayList<ExpFilter>? = ArrayList()
        var expFilter: ExpFilter
        expFilter = ExpFilter("Fresher", 40)
        expFilterList?.add(expFilter)
        expFilter = ExpFilter("1-2 yrs", 41)
        expFilterList?.add(expFilter)
        expFilter = ExpFilter("3-5 yrs", 42)
        expFilterList?.add(expFilter)
        expFilter = ExpFilter("5+ yrs", 43)
        expFilterList?.add(expFilter)

        for (i in (expFilterList?.indices)!!) {

            val expName = expFilterList.get(i)
            val chip = Chip(activity)
            chip.isCheckable = true
            chip.isCheckedIconVisible = false
            if (Constants.exp_filters != null) {
                if (Constants.exp_filters?.contains(expName.expId?.toString() + "") == true) {
                    chip.isChecked = true
                }
            }

            chip.text = expName.exp
            chip.id = expName.expId ?: 0
            chip.setChipBackgroundColorResource(R.color.bg_chips_drawable_light_white)
            chip.setChipStrokeColorResource(R.color.theme)
            chip.chipStrokeWidth = 1f
            chip.isClickable = true
            chip.isCheckedIconVisible = false

            cpgExperience?.addView(chip)
        }

        //for check filter makes any chnages or not
        alreadyExpSelected = ""
        var count = 0
        for (i in 0 until cpgExperience?.getChildCount()!!) {
            val chip = cpgExperience?.getChildAt(i) as Chip
            if (chip.isChecked) {
                count++
                if (alreadyExpSelected == "") {
                    alreadyExpSelected = alreadyExpSelected + chip.id
                } else {
                    alreadyExpSelected = alreadyExpSelected + "," + chip.id
                }
            }
        }
    }

    private fun applyFilter(bottomSheetDialog: BottomSheetDialog?) {

        val time: String = txtTime?.getText().toString().trim({ it <= ' ' })
        val pay: String = txtPay?.getText().toString().trim({ it <= ' ' })
        val timeArray: Array<String>
        val payArray: Array<String>
        var sTime = ""
        var eTime = ""
        var payMax = ""
        var payMin = ""

        if (mMaxAmount > -1) {
            Constants.Maximum_Pay = mMaxAmount
            Constants.pay_active = true
            mMaxAmount = -1
        }
        if (mMinAmount > -1) {
            Constants.Minimum_Pay = mMinAmount
            Constants.pay_active = true
            mMinAmount = -1
        }

        if (mStartTime > -1) {
            Constants.time_active = true
            Constants.start_time = mStartTime
            mStartTime = -1
        }
        if (mEndTime > -1) {
            Constants.time_active = true
            Constants.end_time = mEndTime
            mEndTime = -1
        }
        if (time != null && time.contains("-")) {
            timeArray = time.split("-".toRegex()).toTypedArray()
            sTime = timeArray[0]
            eTime = timeArray[1]
        }
        if (pay != null && pay.contains("-")) {
            payArray = pay.split("-".toRegex()).toTypedArray()
            payMax = payArray[1]
            payMin = payArray[0]
        }

        Constants.Active = true
        expSelected = ""
        var count = 0
        for (i in 0 until cpgExperience?.getChildCount()!!) {
            val chip = cpgExperience?.getChildAt(i) as Chip
            if (chip.isChecked) {
                count++
                if (expSelected == "") {
                    expSelected = expSelected + chip.id
                } else {
                    expSelected = expSelected + "," + chip.id
                }
            }
        }
        Constants.exp_filters_count = count
        if (expSelected.equals("")) {
            Constants.exp_filters = null
        } else {
            Constants.exp_filters = expSelected
        }

        if (count > 0) {
            Constants.exp_active = true
        }

        citySelected = ""
        var cityCount = 0
        for (i in 0 until cpgCities?.getChildCount()!!) {
            val chip = cpgCities?.getChildAt(i) as Chip
            if (chip.isChecked) {
                cityCount++
                if (citySelected == "") {
                    citySelected = citySelected + chip.id
                } else {
                    citySelected = citySelected + "," + chip.id
                }
            }
        }
        Constants.city_filters_count = cityCount
        if (citySelected.equals("")) {
            Constants.city_filters = null
        } else {
            Constants.city_filters = citySelected
        }
        if (cityCount > 0) {
            Constants.city_active = true
        }

        if (Constants.city_active || Constants.exp_active || Constants.pay_active || Constants.time_active) {
            imgFilter?.setImageResource(R.drawable.ic_group_470)
        } else {
            imgFilter?.setImageResource(R.drawable.ic_filter_svg)
        }
        OkayGoFirebaseAnalytics.find_jobs_filters_applied(
            Constants.exp_filters,
            Constants.city_filters,
            eTime,
            payMin,
            payMax
        )
        bottomSheetDialog?.dismiss()
        FindJobsByType()
    }


    fun FindJobsByType() {
        Constants.isOnDemand = null
        Constants.isPt = null
        Constants.isFt = null
        Constants.maxPay = null
        Constants.minMay = null
        Constants.jobType = null
        Constants.expFilter = null
        Constants.cityFilter = null
        Constants.jobAfter = null
        Constants.jobBefore = null

        val mHomeFilter = Preferences.prefs?.getString(Constants.HOME_JOB_FILTER, null)
        if (mHomeFilter.equals("On demand")) {
            Constants.isOnDemand = "1"
        }
        if (mHomeFilter.equals("Part time")) {
            Constants.isPt = "1"
        }
        if (mHomeFilter.equals("Full time")) {
            Constants.isFt = "1"
        }
        if (Constants.pay_active) {
            if (mHomeFilter.equals("On demand")) {
            } else {
                Constants.maxPay = if (Constants.Maximum_Pay < 11) {
                    "" + Constants.Maximum_Pay * 5000
                } else {
                    "" + 40 * 5000
                }
                Constants.minMay = "" + Constants.Minimum_Pay * 5000
            }
        }
        if (Constants.city_active) {
            Constants.cityFilter = Constants.city_filters
        }

        if (Constants.exp_active) {
            Constants.expFilter = Constants.exp_filters
        }

        if (Constants.time_active) {
            Constants.jobAfter = if (Constants.start_time < 10) {
                Constants.start_time.toString() + "0:00:00"
            } else {
                Constants.start_time.toString() + ":00:00"
            }
            Constants.jobBefore = if (Constants.end_time < 10) {
                Constants.end_time.toString() + "0:00:00"
            } else {
                Constants.end_time.toString() + ":00:00"
            }
        }
        val request: JobCategoryRequest? = JobCategoryRequest(
            userId,
            Constants.isOnDemand,
            Constants.isPt,
            Constants.isFt,
            Constants.maxPay,
            Constants.minMay,
            Constants.jobType,
            Constants.jobAfter,
            Constants.cityFilter,
            Constants.expFilter,
            Constants.jobBefore
        )
        viewModel.getJobCategories(request)
    }

    /**
     * commented on reivew hit api to refresh count
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun ApplyJobEventDeepLink(jobItems: ApplyJobEventDeepLink) {
        Log.e("Apply job", "deeplink")
        mDeeplinkJobData = jobItems
        Utilities.showLoader(activity)
//        mCvFileName = jobItems.cvFileName
//        isCVRequired = jobItems.content.detailsJobs[0].cvRequired
        val refered_by = Preferences.prefs?.getInt(Constants.REFERED_BY, 0)
        Constants.DEEPLINK_REFFER_BY = refered_by
//        viewModelJobList.applyJob(jobItems?.assignId, userId, refered_by)
//        showInterviewSlotsBottomSheet(jobItems.getAssignId() + "");
        viewModelJobList.getRequiredSkills(jobItems.content?.jobId, workerId)
    }


    fun CheckActiveFilters() {
        if (Constants.city_active || Constants.exp_active || Constants.pay_active || Constants.time_active) {
            imgFilter?.setImageResource(R.drawable.ic_group_470)
        } else {
            imgFilter?.setImageResource(R.drawable.ic_filter_svg)
        }
    }

    private fun initlize() {
        val filterName = Preferences.prefs?.getString(Constants.HOME_JOB_FILTER, null)
        when (filterName) {
            null -> {
                Preferences.prefs?.saveValue(Constants.HOME_JOB_FILTER, "All Jobs")
                txtCurrentJobType?.text = "All Jobs"
            }
            "On demand" -> txtCurrentJobType?.text = "On demand"
            "Part time" -> txtCurrentJobType?.text = "Part time"
            "Full time" -> txtCurrentJobType?.text = "Full time"
            "All Jobs" -> txtCurrentJobType?.text = "All Jobs"
        }

        findJobsByType()
    }

    private fun findJobsByType() {
        Constants.isOnDemand = null;
        Constants.isPt = null;
        Constants.isFt = null;
        Constants.maxPay = null;
        Constants.minMay = null;
        Constants.jobType = null;
        Constants.expFilter = null;
        Constants.cityFilter = null;
        Constants.jobAfter = null;
        Constants.jobBefore = null;

        val homeFilter = Preferences.prefs?.getString(Constants.HOME_JOB_FILTER, null)
        if (homeFilter == "On demand") {
            Constants.isOnDemand = "1"
        }
        if (homeFilter == "Part time") {
            Constants.isPt = "1"
        }
        if (homeFilter == "Full time") {
            Constants.isFt = "1"
        }
        if (Constants.pay_active) {
            if (homeFilter == "On demand") {
            } else {
                Constants.maxPay = if (Constants.Maximum_Pay < 11) {
                    "" + Constants.Maximum_Pay * 5000
                } else {
                    "" + 40 * 5000
                }
                Constants.minMay = "" + Constants.Minimum_Pay * 5000
            }
        }
        if (Constants.city_active) {
            Constants.cityFilter = Constants.city_filters
        }

        if (Constants.exp_active) {
            Constants.expFilter = Constants.exp_filters
        }

        if (Constants.time_active) {
            Constants.jobAfter = if (Constants.start_time < 10) {
                Constants.start_time.toString() + "0:00:00"
            } else {
                Constants.start_time.toString() + ":00:00"
            }
            Constants.jobBefore = if (Constants.end_time < 10) {
                Constants.end_time.toString() + "0:00:00"
            } else {
                Constants.end_time.toString() + ":00:00"
            }
        }
        val request: JobCategoryRequest? = JobCategoryRequest(
            userId,
            Constants.isOnDemand,
            Constants.isPt,
            Constants.isFt,
            Constants.maxPay,
            Constants.minMay,
            Constants.jobType,
            Constants.jobAfter,
            Constants.cityFilter,
            Constants.expFilter,
            Constants.jobBefore
        )
        viewModel.getJobCategories(request)
    }

    private val onOKClick: () -> Unit = {
    }

    /**
     * handle api reposne
     */
    private fun attachObservers() {
        viewModel.responseJobCat.observe(this, Observer {
            if (it?.code == Constants.SUCCESS) {
                if (mHeaderList == null) {
                    mHeaderList = ArrayList()
                } else {
                    mHeaderList?.clear()
                }
                it.response?.let {
                    it.experiencedTypeJobs?.let { it1 -> mHeaderList?.addAll(it1) }
                    it.interestedCatJobs?.let { it1 -> mHeaderList?.addAll(it1) }
                    it.otherJobs?.let { it1 -> mHeaderList?.addAll(it1) }
                }
                if (mHeaderList?.isEmpty() == true) {
                    linearNoResult?.visibility = View.VISIBLE
                } else {
                    linearNoResult?.visibility = View.GONE
                }
                mHeaderAdapter?.notifyDataSetChanged()
            }
        })

        viewModelPasswordModel.responseWorker.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (it.response?.content?.get(0)?.superToggle == 1) {
                        Constants.isToggle = true
                        OkayGoFirebaseAnalytics.availability_today_on(Utilities.getCurrentTime())
                        swAvailbaleToday?.setChecked(true)
                        txtAvailableText?.setText(R.string.time_availablilty)
                    } else {
                        swAvailbaleToday?.setChecked(false)
                        OkayGoFirebaseAnalytics.availability_today_off(Utilities.getCurrentTime())
                        txtAvailableText?.setText(R.string.are_you_free_today)
                    }
                }
            }
        })

        viewModelJobList.responseApplyJob.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    OkayGoFirebaseAnalytics.on_job_apply(
                        mDeeplinkJobData?.content?.workType?.toString(),
                        mDeeplinkJobData?.content?.workTypeKey,
                        mDeeplinkJobData?.content?.detailsJobs?.get(0)?.jobDetailsId?.toString(),
                        "deeplink"
                    )
                    Utilities.jobSuccessDialog(
                        activity,
                        activity?.resources?.getString(R.string.job_applied),
                        activity?.resources?.getString(R.string.applied_success_descriotion),
                        true, onOKClick
                    )
                    findJobsByType()
                }
            }
        })

        viewModelJobList.responseRequiredSkills.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    var isForExp = false
                    val intent: Intent? = Intent(activity, PostApplicationActivity::class.java)
                    var completeDataCount = 0
                    var requiredFalseCount = 0
                    for (i in 0..(it.response?.size ?: 0) - 1) {
                        if (it.response?.get(i)?.required == true) {
                            if (it.response.get(i).required != it.response.get(i).available) {

                                when (it.response.get(i).skillId) {
                                    Constants.USER_EDUCATION,
                                    Constants.USER_LANG,
                                    Constants.USER_LAPTOP,
                                    Constants.USER_SMARTPHONE,
                                    Constants.USER_WIFI,
                                    Constants.USER_BIKE,
                                    Constants.USER_DL,
                                    Constants.USER_RC,
                                    Constants.USER_ADHAAR,
                                    Constants.USER_PAN,
                                    Constants.USER_SKILLS -> {
                                        isForExp = true
                                    }
                                }
                                if (isForExp == true) {
                                    intent?.putExtra(
                                        Constants.POST_NAV_SCREEN,
                                        Constants.UNCOMPLETE_INFO
                                    )
                                } else {
                                    intent?.putExtra(
                                        Constants.POST_NAV_SCREEN,
                                        Constants.ADD_EXP
                                    )
                                }
                                break
                            } else {
                                completeDataCount = completeDataCount + 1
                            }
                        } else {
                            requiredFalseCount = requiredFalseCount + 1
                            completeDataCount = completeDataCount + 1
                        }
                    }

                    if (it.response?.size == requiredFalseCount) {
                        intent?.putExtra(Constants.POST_NAV_SCREEN, Constants.NO_REQUIREMENT)
                    } else
                        if (it.response?.size == completeDataCount) {
                            intent?.putExtra(Constants.POST_NAV_SCREEN, Constants.COMPLETE_INFO)
                        }
                    intent?.putExtra(Constants.POST_APPLY_REQUIRED_DATA, it)
                    intent?.putExtra(Constants.POST_Assign_id, mDeeplinkJobData?.assignId)
                    activity?.startActivity(intent)
                }
            }
        })

        viewModelJobList.responseSupperToggle.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (it.response.superToggle == 1) {
                        Constants.isToggle = true
                    } else {
                        Constants.isToggle = false
                    }
                }
            }
        })
        viewModelJobList.responseJobCities.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    val tempList: ArrayList<JobCity>? = ArrayList()
                    val set: HashSet<JobCity> = HashSet()
                    var jobCity: JobCity
                    for (i in (it.response?.indices)!!) {
                        jobCity = JobCity(
                            it.response.get(i).city_name, (it.response.get(i).new_city_id
                                ?: "0").toInt()
                        )
                        set.add(jobCity)
                    }
                    tempList?.clear()
                    tempList?.addAll(set)
                    for (i in (tempList?.indices)!!) {
                        val expName = tempList?.get(i)
                        val chip = Chip(activity)
                        chip.isCheckable = true
                        chip.isCheckedIconVisible = false
                        if (Constants.city_filters != null) {
                            if (Constants.city_filters?.contains(expName?.cityId?.toString() + "") == true) {
                                chip.isChecked = true
                            }
                        }

                        chip.text = expName.cityName
                        chip.id = expName.cityId ?: 0
                        chip.setChipBackgroundColorResource(R.color.bg_chips_drawable_light_white)
                        chip.setChipStrokeColorResource(R.color.theme)
                        chip.chipStrokeWidth = 1f
                        chip.isClickable = true
                        chip.isCheckedIconVisible = false
                        cpgCities?.addView(chip)
                    }

                    //for check filter makes any chnages or not
                    alreadyCitySelected = ""
                    var count = 0
                    for (i in 0 until cpgCities?.getChildCount()!!) {
                        val chip = cpgCities?.getChildAt(i) as Chip
                        if (chip.isChecked) {
                            count++
                            if (alreadyCitySelected == "") {
                                alreadyCitySelected = alreadyCitySelected + chip.id
                            } else {
                                alreadyCitySelected = alreadyCitySelected + "," + chip.id
                            }
                        }
                    }
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtCurrentJobType -> {
                setJobType()
            }
            R.id.imgFilter -> {
                OnFilterClick()
//                val intent: Intent? = Intent(activity, NavigationActivity::class.java)
//                intent?.putExtra(Constants.NAV_SCREEN, 25)
//                activity?.startActivity(intent)

            }
        }
    }
}