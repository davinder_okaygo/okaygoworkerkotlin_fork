package com.okaygo.worker.ui.fragments.on_boarding

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.okaygo.worker.R
import com.okaygo.worker.adapters.SearchAdapter
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.reponse.CitySearch
import com.okaygo.worker.data.modal.reponse.WorkerDetails
import com.okaygo.worker.data.modal.request.UserDetailRequest
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.spinner_date_picker.DatePicker
import com.okaygo.worker.spinner_date_picker.DatePickerDialogCustom
import com.okaygo.worker.spinner_date_picker.SpinnerDatePickerDialogBuilder
import com.okaygo.worker.ui.activity.city_state_search.SearchActivity
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.Preferences
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.OnBoardingModel
import com.openkey.guest.ui.fragments.verification.PasswordModel
import com.openkey.guest.ui.fragments.verification.ProfileModel
import com.openkey.guest.ui.fragments.verification.SearchModel
import kotlinx.android.synthetic.main.fragment_personal_detail.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

class PersonalDetailFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModelSearch: SearchModel
    private lateinit var viewModelWorker: PasswordModel  // this is for handle/get worker detail
    private lateinit var viewModel: OnBoardingModel
    private lateinit var viewModelProfile: ProfileModel

    //    private var mGender: Int? = 0
    private var mCityId: Int? = 0
    var myCalendar: Calendar? = null
    private var dob: String? = null
    private var isProfileUpdate: Boolean? = false
    private var mTempList: ArrayList<CitySearch>? = null
    private var mSearchAdapter: SearchAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        viewModelWorker = ViewModelProvider(this).get(PasswordModel::class.java)
        viewModel = ViewModelProvider(this).get(OnBoardingModel::class.java)
        viewModelProfile = ViewModelProvider(this).get(ProfileModel::class.java)
        viewModelSearch = ViewModelProvider(this).get(SearchModel::class.java)
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_personal_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        myCalendar = Calendar.getInstance()
        setListeners()
        handleForProfile()
    }

    private fun handleForProfile() {
        if (Constants.IS_FROM_PROFILE == true) {
            isProfileUpdate = true
            constraintBottom?.visibility = View.GONE
            imgBackIcon?.visibility = View.VISIBLE
            linearBottomBtns?.visibility = View.VISIBLE
            txtEdit?.visibility = View.VISIBLE
            lnrBottomLayout?.visibility = View.GONE

            edtFName?.setEnabled(false)
            edtLName?.isEnabled = false
            txtDateOfBirth?.isEnabled = false
            edtEmail?.isEnabled = false
            txtCityName?.isEnabled = false
            chMale?.isEnabled = false
            chFemale?.isEnabled = false

//            viewModelProfile.getWorkerByUserId(userId, false)
        } else {
            var langId = 1
            if (Preferences.prefs?.getString(Constants.SELECTED_LANG, "en").equals("en")) {
                langId = 2
            }
            viewModelProfile.updateLang(langId, userId)
        }
        viewModelProfile.getWorkerByUserId(userId, false)

    }

    /**
     * set listeners to required views
     */
    private fun setListeners() {
//        btnSaveContinue?.setOnClickListener(this)
        txtNext?.setOnClickListener(this)
        imgBackIcon?.setOnClickListener(this)
        imgBack?.setOnClickListener(this)
        txtCityName?.setOnClickListener(this)
        txtDateOfBirth?.setOnClickListener(this)
        txtEdit?.setOnClickListener(this)
        txtSave?.setOnClickListener(this)
        txtDiscard?.setOnClickListener(this)
//        handleEdtSearch()
    }

//    private fun getCityByString(text: String) {
//        if (!Utilities.isOnline(activity)) {
//            Utilities.showToast(activity, resources.getString(R.string.no_internet))
//            return
//        }
//        progressSearch?.visibility = View.VISIBLE
//        viewModelSearch.getCityWithText(text)                 //city api call
//    }
//
//    /**
//     * click listenr when click on any city
//     */
//    private val onCityClick: (CitySearch?, Int?) -> Unit = { it, pos ->
//        Log.e("Pstion", pos.toString())
//        Log.e("CityName", it?.district.toString() + "")
//        txtCityName?.setText(it?.district.toString())
//        mCityId = it?.city_id
////        EventBus.getDefault().post(it)
////        onBackPressed()
//    }

    var onDateSetListener: DatePickerDialogCustom.OnDateSetListener =
        object : DatePickerDialogCustom.OnDateSetListener {
            override fun onDateSet(
                view: DatePicker?,
                year: Int,
                monthOfYear: Int,
                dayOfMonth: Int
            ) {
                txtDateOfBirth?.setText(Utilities.getFormatedDate(year.toString() + "-" + (monthOfYear + 1) + "-" + dayOfMonth))
                if (monthOfYear + 1 < 10) {
                    if (dayOfMonth < 10) {
                        dob = year.toString() + "-0" + (monthOfYear + 1) + "-0" + dayOfMonth
                    } else {
                        dob = year.toString() + "-0" + (monthOfYear + 1) + "-" + dayOfMonth
                    }
                } else {
                    if (dayOfMonth < 10) {
                        dob = year.toString() + "-" + (monthOfYear + 1) + "-0" + dayOfMonth
                    } else {
                        dob = year.toString() + "-" + (monthOfYear + 1) + "-" + dayOfMonth
                    }
                }
            }
        }

    fun getGenderId(): String? {
        if (chMale.isChecked()) {
            return "26"
        }
        return if (chFemale.isChecked()) {
            "27"
        } else "26"
    }

    private fun setDataToViews(response: WorkerDetails?) {
        edtFName?.setText(response?.firstName ?: "")
        edtLName?.setText(response?.lastName ?: "")
        edtEmail?.setText(response?.emailId ?: "")
        txtCityName?.setText(response?.cityName ?: "")
        mCityId = response?.cityId
        dob = response?.birthDate
        txtDateOfBirth?.setText(Utilities.getFormatedDate(dob))
        if (response?.gender.equals("26")) {
            chMale.setChecked(true)
        } else if (response?.gender.equals("27")) {
            chFemale.setChecked(true)
        }
    }

//    /**
//     * handle search while typing
//     */
//    private fun handleEdtSearch() {
//        edtCityName?.addTextChangedListener(object : TextWatcher {
//            override fun beforeTextChanged(
//                s: CharSequence,
//                start: Int,
//                count: Int,
//                after: Int
//            ) {
//            }
//
//            override fun onTextChanged(
//                s: CharSequence,
//                start: Int,
//                before: Int,
//                count: Int
//            ) {
//            }
//
//            override fun afterTextChanged(s: Editable) {
//                if (s.toString().length > 1) {
//                    getCityByString(s.toString())
//                } else {
//                    txtNotFound?.visibility = View.GONE
//                    recylerSearch?.visibility = View.GONE
//                }
//            }
//        })
//    }

//    /**
//     * set adapter for searched list
//     */
//    private fun setCityAdapter() {
////        if (mTempList == null) {
////            mTempList = ArrayList<CitySearch>()
////        }
//        val linearLayoutManager = LinearLayoutManager(activity)
//        mSearchAdapter = activity?.let {
//            SearchAdapter(
//                it, mTempList,
//                onCityClick
//            )
//        }
//        recylerSearch?.layoutManager = linearLayoutManager
//        recylerSearch?.adapter = mSearchAdapter
//        scrollView?.scrollTo(0, scrollView?.getBottom() ?: 0)
//    }

    /**
     * handle api reposne
     */
    private fun attachObservers() {

//        viewModelWorker.responseWorker.observe(this, Observer {
//            it?.let {
//                if (it.code == Constants.SUCCESS && it.response?.content?.isEmpty()==false ) {
//                    Log.e("Worker id session api", it.response?.content?.get(0)?.workerId?.toString())
//                    Preferences.prefs?.saveValue(
//                        Constants.EMPLOYER_ID,
//                        it.response?.content?.get(0)?.workerId
//                    )
//                }
//            }
//        })

        viewModel.responsePersonalDetail.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    Preferences.prefs?.saveValue(
                        Constants.userName,
                        it.response?.content?.get(0)?.firstName + " " + it.response?.content?.get(0)?.lastName
                    )
                    if (Constants.IS_FROM_PROFILE) {
                        Utilities.showToast(activity, "Profile updated.")
                        handleForProfile()
                    } else {
                        OkayGoFirebaseAnalytics.on_boarding_personal_details()
//                        attachFragment(SelfyFragment(), false)
                        attachFragment(InterestedCatFragment(), true)
                    }
                }
            }
        })

        viewModelProfile.responseWorkerData.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS && it.response?.content?.isEmpty() == false) {
                    Preferences.prefs?.saveValue(
                        Constants.EMPLOYER_ID,
                        it.response.content.get(0).workerId
                    )
                    setDataToViews(it.response.content.get(0))

                }
            }
        })

//        viewModelSearch.response.observe(this, androidx.lifecycle.Observer {
//
//            if (it.code == Constants.SUCCESS) {
//                progressSearch?.visibility = View.GONE
//
//                txtNotFound?.visibility = View.GONE
//                recylerSearch?.visibility = View.VISIBLE
//                if (mTempList == null) {
//                    mTempList = ArrayList<CitySearch>()
//                } else {
//                    mTempList?.clear()
//                }
//                var i = 0
//                while (i < it.response?.size ?: 0) {
//                    it.response?.get(i)?.let { it1 -> mTempList?.add(it1) }
//                    i++
//                }
//                if (mTempList?.isEmpty() == true) {
//                    txtNotFound?.visibility = View.VISIBLE
//                    recylerSearch?.visibility = View.GONE
//                }
//
//                Log.e("City Set", mTempList.toString() + "")
////                setCityAdapter()
//            }
//        })
//
//        viewModelSearch.apiError.observe(this, androidx.lifecycle.Observer {
//            progressSearch?.visibility = View.GONE
//
//            txtNotFound?.visibility = View.VISIBLE
//            recylerSearch?.visibility = View.VISIBLE
//        })

        viewModel.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })

        viewModelProfile.apiError.observe(this, Observer {
            it?.let {
                Utilities.showToast(activity, it)
            }
        })

        viewModelProfile.isLoading.observe(this, Observer {
            it?.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    /**
     * handel save button click
     */
    private fun handleSaveClick() {
        val fName: String? = edtFName?.text.toString().trim()
        val lName: String? = edtLName?.text.toString().trim()
//        val dob: String? = txtDateOfBirth?.text.toString().trim()
        val email: String? = edtEmail?.text.toString().trim()
        val city: String? = txtCityName?.text.toString().trim()
        if (fName?.length ?: 0 < 1) {
            edtFName?.error = "Enter First Name"
        } else if (lName?.length ?: 0 < 1) {
            edtLName?.error = "Enter Last Name"
        } else if (dob == null || dob?.length ?: 0 < 8) {
            Utilities.showToast(activity, "Select date of birth")
        } else if (email?.isNotEmpty() == true && Utilities.isValidEmail(email) == false) {
            Utilities.showToast(activity, "Invalid Email id")
        } else if (chgGender?.checkedChipId == -1) {
            Utilities.showToast(activity, "Select gender")
        } else if (city?.length ?: 0 < 1) {
            Utilities.showToast(activity, "Select your city")
        } else {
            val request: UserDetailRequest? = UserDetailRequest(
                mCityId.toString(),
                dob,
                fName,
                lName,
                getGenderId(),
                userId.toString(),
                email,
                city,
                null
            )
            viewModel.saveUserDetail(request)                       //save user api call
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.txtSave,
            R.id.txtNext -> {
                if (userId == 0) {
                    Utilities.logoutUser(userId, activity)
                } else {
//                    isProfileUpdate = true
                    handleSaveClick()
                }
            }
            R.id.imgBackIcon,
            R.id.imgBack -> {
                activity?.onBackPressed()
            }
            R.id.txtEdit -> {
                handleEdit()
            }

            R.id.txtDiscard -> {
                handleForProfile()
            }

            R.id.txtCityName -> {
                val intent = Intent(activity, SearchActivity::class.java)
                startActivity(intent)
            }

            R.id.txtDateOfBirth -> {
                SpinnerDatePickerDialogBuilder()
                    .context(activity)
                    .callback(onDateSetListener)
                    .spinnerTheme(R.style.NumberPickerStyle)
                    .showTitle(true)
                    .showDaySpinner(true)
                    .defaultDate(
                        myCalendar?.get(Calendar.YEAR)?.minus(18) ?: 0,
                        myCalendar?.get(Calendar.MONTH) ?: 0,
                        myCalendar?.get(Calendar.DAY_OF_MONTH)?.minus(1) ?: 0
                    )

                    .minDate(
                        myCalendar?.get(Calendar.YEAR)?.minus(65) ?: 0,
                        myCalendar?.get(Calendar.MONTH) ?: 0,
                        myCalendar?.get(Calendar.DAY_OF_MONTH) ?: 0
                    )
                    .maxDate(
                        myCalendar?.get(Calendar.YEAR)?.minus(18) ?: 0,
                        myCalendar?.get(Calendar.MONTH) ?: 0,
                        myCalendar?.get(Calendar.DAY_OF_MONTH) ?: 0
                    )
                    .build()
                    .show()
            }
        }
    }

    private fun handleEdit() {
        constraintBottom?.visibility = View.VISIBLE
        txtEdit?.visibility = View.GONE
        edtFName?.setEnabled(true)
        edtLName?.isEnabled = true
        txtDateOfBirth?.isEnabled = true
        edtEmail?.isEnabled = true
        txtCityName?.isEnabled = true

        chMale?.isEnabled = true
        chFemale?.isEnabled = true
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun cityName(cityData: CitySearch) {
        Log.e("EventBus Cityname", cityData?.district)
        mCityId = cityData.city_id
        txtCityName?.requestFocus()
        txtCityName?.text = cityData.district.toString()
    }
}