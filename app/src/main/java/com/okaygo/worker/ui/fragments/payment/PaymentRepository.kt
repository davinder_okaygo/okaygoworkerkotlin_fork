package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.DueTransectionResponse
import com.okaygo.worker.data.modal.reponse.PaymentTransectionResponse
import com.okaygo.worker.data.modal.reponse.ReferralPaymentResponse
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object PaymentRepository {
    private val mService = ApiHelper.getService()

    /**
     *
     */
    fun getPaymentTransection(
        successHandler: (PaymentTransectionResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getPaymentTransection(userId, 100)
            ?.enqueue(object : Callback<PaymentTransectionResponse> {
                override fun onResponse(
                    call: Call<PaymentTransectionResponse>?,
                    response: Response<PaymentTransectionResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<PaymentTransectionResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("payment failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun getReferralTransection(
        successHandler: (ReferralPaymentResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getReferralTransection(userId, 0, 100)
            ?.enqueue(object : Callback<ReferralPaymentResponse> {
                override fun onResponse(
                    call: Call<ReferralPaymentResponse>?,
                    response: Response<ReferralPaymentResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<ReferralPaymentResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("payment failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun getDueTransection(
        successHandler: (DueTransectionResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getDueTransection(userId)?.enqueue(object : Callback<DueTransectionResponse> {
            override fun onResponse(
                call: Call<DueTransectionResponse>?,
                response: Response<DueTransectionResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<DueTransectionResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("due failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }
}