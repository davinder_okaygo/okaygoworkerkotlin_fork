package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.AppliedJobResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class PastJobModel(app: Application) : MyViewModel(app) {
    var responsePastJob = MutableLiveData<AppliedJobResponse>()

    /**
     *
     */
    fun getPastJobs(userId: Int?) {
        isLoading.value = true
        PastJobRepository.getPastJobs({
            isLoading.value = false
            responsePastJob.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

}