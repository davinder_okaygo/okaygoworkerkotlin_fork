package com.okaygo.worker.ui.fragments.notification

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.okaygo.worker.R
import com.okaygo.worker.adapters.NotificationAdapter
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.BottomNavScreenChange
import com.okaygo.worker.data.modal.reponse.NotificationResponse
import com.okaygo.worker.data.modal.reponse.Notifications
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.activity.NavigationActivity
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.Dialogs
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.JobListModel
import com.openkey.guest.ui.fragments.verification.NotificationModel
import kotlinx.android.synthetic.main.fragment_alert.*
import org.greenrobot.eventbus.EventBus

class AlertFragment : BaseFragment() {
    private lateinit var viewModel: NotificationModel
    private lateinit var viewModelJobList: JobListModel

    private var mList: ArrayList<Notifications>? = null
    private var mAdapter: NotificationAdapter? = null
    private var mAlertPosBtn: Int? = 0
    private var mNotificationData: Notifications? = null
    private var mBootmScreen: BottomNavScreenChange? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(NotificationModel::class.java)
        viewModelJobList = ViewModelProvider(this).get(JobListModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_alert, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        OkayGoFirebaseAnalytics.inapp_alert_view()
        setListeners()
    }

    override fun onResume() {
        super.onResume()
        Log.e("Alert", "true")
        viewModel.getNotifications(userId, 1)

    }

    private fun setListeners() {
        swipeRefresh?.setOnRefreshListener {
            swipeRefresh?.isRefreshing = true
            if (Utilities.isOnline(context)) {
//                currentPage = 0
                viewModel.getNotifications(userId, 1)
            } else {
                image?.visibility = View.VISIBLE
                image?.setImageResource(R.drawable.ic_no_wifi)
                warning?.visibility = View.VISIBLE
                warning?.text = "No Internet Connection"
                swipeRefresh?.isRefreshing = false
            }
        }
    }

    private fun setAdapter(reponse: NotificationResponse?) {
        if (mList == null) {
            mList = ArrayList()
        } else {
            mList?.clear()
        }
        reponse?.response?.content?.let { mList?.addAll(it) }
        mAdapter =
            activity?.let {
                NotificationAdapter(
                    it,
                    true,
                    mList,
                    OnNotificationClick,
                    OnPosBtnClick,
                    OnNagBtnClick
                )
            }
        val linearLayoutManager = LinearLayoutManager(activity)
        recylerNotification?.setAdapter(mAdapter)
        recylerNotification.setLayoutManager(linearLayoutManager)
    }

    /**
     * click listener when edit any exp
     */
    private val OnNotificationClick: (Notifications?, Int?) -> Unit = { it, pos ->

    }

    /**
     * click listener when edit any exp
     */
    private val OnPosBtnClick: (Notifications?, Int?) -> Unit = { it, clickFor ->
        when (clickFor) {
            1, 2, 11 -> {
                viewModel.deleteNotification(userId, it?.notificationId)
                mBootmScreen = BottomNavScreenChange(5)
//                EventBus.getDefault().post(5)
                activity?.onBackPressed()
            }
            11 -> {
                viewModel.deleteNotification(userId, it?.notificationId)
                mBootmScreen = BottomNavScreenChange(3)
//                EventBus.getDefault().post(3)
                activity?.onBackPressed()
            }
            12 -> {
                viewModel.deleteNotification(userId, it?.notificationId)
            }
            3 -> {
                viewModel.deleteNotification(userId, it?.notificationId)
                val intent: Intent? = Intent(activity, NavigationActivity::class.java)
                intent?.putExtra(Constants.NAV_SCREEN, Constants.AVAILABILITY_DASHBOARD)
                startActivity(intent)
            }
            4 -> Dialogs.alertDialog(
                activity,
                resources?.getString(R.string.our_team_call_solve_problem),
                "Yes",
                "No",
                onAlertPosBtnClick
            )
            8 -> Dialogs.alertDialog(
                activity,
                resources?.getString(R.string.nagitive_rating_for_job),
                "Yes",
                "No",
                onAlertPosBtnClick
            )
            9 -> {
                mNotificationData = it
                mAlertPosBtn = 9
                Dialogs.alertDialog(
                    activity,
                    resources?.getString(R.string.nagitive_rating_for_no_job),
                    "Yes",
                    "No",
                    onAlertPosBtnClick
                )
            }
            16 -> {
                mBootmScreen = BottomNavScreenChange(1)
//                EventBus.getDefault().post(1)
                activity?.onBackPressed()
            }
            17 -> {
                mNotificationData = it
                mAlertPosBtn = 17
                Dialogs.alertDialog(
                    activity,
                    resources?.getString(R.string.press_yes),
                    "Yes",
                    "No",
                    onAlertPosBtnClick
                )
            }
            18 -> {
                activity?.onBackPressed()
                val intent = Intent(activity, NavigationActivity::class.java)
                intent.putExtra(Constants.NAV_SCREEN, Constants.REFFERAL)
                startActivity(intent)
            }
        }
        if (mBootmScreen != null) {
            EventBus.getDefault().post(mBootmScreen)
        }
    }

    private val onAlertPosBtnClick: () -> Unit = {
        when (mAlertPosBtn) {
            9 -> viewModel.deleteNotification(userId, mNotificationData?.notificationId)
            17 -> viewModel.getAssignedJobDetail(userId, mNotificationData?.eventId)
        }
    }

    /**
     * click listener when edit any exp
     */
    private val OnNagBtnClick: (Notifications?, Int?) -> Unit = { it, clickFor ->
        when (clickFor) {
            11 -> {
                viewModel.deleteNotification(userId, it?.notificationId)
                mBootmScreen = BottomNavScreenChange(3)
//                EventBus.getDefault().post(3)
                EventBus.getDefault().post(mBootmScreen)

                activity?.onBackPressed()
            }
            5,
            17 -> {
                val intent: Intent? = Intent(activity, NavigationActivity::class.java)
                intent?.putExtra(Constants.NAV_SCREEN, Constants.JOB_DETAIL)
                intent?.putExtra(Constants.JOB_DETAIL_ID, it?.eventId)
                startActivity(intent)

            }
        }
    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.responseNotification.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (it?.response?.content?.isEmpty() == true) {
                        image?.visibility = View.VISIBLE
                        image?.setImageResource(R.drawable.ic_no_records)
                        warning?.visibility = View.VISIBLE
                        warning?.text = "No data found"
                        swipeRefresh?.setRefreshing(false)
                    } else {
                        setAdapter(it)
                        image?.visibility = View.GONE
                        warning?.visibility = View.GONE
                        swipeRefresh?.setRefreshing(false)
                    }
                }
            }
        })
        viewModel.responseDelNotification.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    viewModel.getNotifications(userId, 1)
                }
            }
        })
        viewModel.responseAssignedDetail.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
//                    OkayGoFirebaseAnalytics.on_job_apply(
//                        "unknown",
//                        it.getJobType(),
//                        it.getJobDetailId(),
//                        "alert"
//                    )

                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                    swipeRefresh?.setRefreshing(false)
                }
            }

        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
        viewModelJobList.responseApplyJob.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    viewModel.deleteNotification(
                        userId, mNotificationData?.notificationId
                    )
                    if (it?.response?.equals("Job already Accepted") == true) {
                        Utilities.showToast(context, "Already Applied.")

                    } else {
                        Utilities.showSuccessToast(activity, "Job Applied.")
                        activity?.onBackPressed()
                    }

                }
            }
        })

        viewModelJobList.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                    swipeRefresh?.setRefreshing(false)
                }
            }

        })

        viewModelJobList.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

}