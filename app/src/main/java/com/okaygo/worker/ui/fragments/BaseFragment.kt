package com.okaygo.worker.ui.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.okaygo.worker.R
import com.okaygo.worker.ui.activity.dashboard.DashBoardActivity
import com.openkey.guest.help.Preferences
import com.openkey.guest.help.utils.Constants

/**
 * @author Davinder Goel.
 *
 * Provide common features for every fragment.
 * This must be extended by every fragment
 */
open class BaseFragment : Fragment() {
    protected var userId: Int? = null
    protected var workerId: Int? = null

    private var mActivity: FragmentActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = activity
        if (Constants.ID.equals("") == false) {
            userId = Preferences.prefs?.getInt(Constants.ID, 0)
            workerId = Preferences.prefs?.getInt(Constants.EMPLOYER_ID, 0)
        }
    }

    /**
     * Attach fragment with or without addToBackStack
     *
     * @param fragment       which needs to be attached
     * @param addToBackStack is fragment needed to backstack
     */
    fun attachFragment(fragment: Fragment, addToBackStack: Boolean) {
        val tag: String = fragment::class.java.simpleName
        Log.e("tag", "" + tag)
        mActivity.let {
            val manager = mActivity?.supportFragmentManager
            val oldFragmentObject = manager?.findFragmentByTag(tag)
            val transaction = manager?.beginTransaction()
//            transaction?.setCustomAnimations(
//                com.openkey.guest.R.anim.anim_in, R.anim.anim_out, R.anim.anim_in_reverse,
//                com.openkey.guest.R.anim.anim_out_reverse
//            )

            if (addToBackStack) {
                transaction?.addToBackStack(tag)
            }
            transaction?.replace(R.id.container, fragment, tag)
                ?.commitAllowingStateLoss()
//            }
        }
    }


    /**
     * Attach fragment with or without addToBackStack
     *
     * @param fragment       which needs to be attached
     * @param addToBackStack is fragment needed to backstack
     */
    fun addFragment(fragment: Fragment, addToBackStack: Boolean) {
        val tag: String = fragment::class.java.simpleName
        Log.e("tag", "" + tag)
        mActivity.let {
            val manager = mActivity?.supportFragmentManager
            val oldFragmentObject = manager?.findFragmentByTag(tag)
            val transaction = manager?.beginTransaction()
//            transaction?.setCustomAnimations(
//                R.anim.anim_in, R.anim.anim_out, R.anim.anim_in_reverse,
//                R.anim.anim_out_reverse
//            )

            if (addToBackStack) {
                transaction?.addToBackStack(tag)
            }
            transaction?.add(R.id.container, fragment, tag)
                ?.commitAllowingStateLoss()
//            }
        }
    }

    /**
     * go back to previous screen
     */
    protected fun goBack() {
        mActivity.let {
            mActivity?.onBackPressed()
        }
    }

    /**
     * get Dashboard activity instance for all fragments
     */
    protected fun dashboardActivity(): DashBoardActivity? {
        return activity as? DashBoardActivity
    }
}