package com.openkey.guest.ui.fragments.verification

import android.util.Log
import com.okaygo.worker.data.modal.reponse.*
import com.okaygo.worker.data.modal.request.FindJobRequest
import com.openkey.guest.data.Api.ApiHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author Davinder Goel OkayGo.
 */
object JobListRepository {
    private val mService = ApiHelper.getService()

    /**
     *
     */
    fun getJobsAccordingToCat(
        successHandler: (FindJobResponse) -> Unit,
        failureHandler: (String) -> Unit,
        req: FindJobRequest?
    ) {
        mService.getJobsAccordingToCat(
            req?.user_id,
            req?.job_category_id,
            req?.filter_od,
            req?.filter_pt,
            req?.filter_ft,
            req?.max_pay,
            req?.min_pay,
            req?.job_types,
            req?.jobs_after,
            req?.jobs_before,
            req?.city,
            req?.experience,
            req?.page_no,
            req?.rows
        )?.enqueue(object : Callback<FindJobResponse> {
            override fun onResponse(
                call: Call<FindJobResponse>?,
                response: Response<FindJobResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<FindJobResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("FindJobRes  failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     *
     */
    fun matchJob(
        successHandler: (JobMatchResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?,
        jobId: Int?
    ) {
        mService.matchJob(jobId, userId?.toString())?.enqueue(object : Callback<JobMatchResponse> {
            override fun onResponse(
                call: Call<JobMatchResponse>?,
                response: Response<JobMatchResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<JobMatchResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("JobMatch failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     *
     */
    fun applyJob(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        asignId: Int?,
        userId: Int?,
        refferBy: Int?
    ) {
        mService.applyJob(asignId, userId, refferBy)?.enqueue(object : Callback<SuccessResponse> {
            override fun onResponse(
                call: Call<SuccessResponse>?,
                response: Response<SuccessResponse>?
            ) {
                response?.body()?.let {
                    successHandler(it)
                }
                response?.errorBody()?.let {
                    if (response.code() == 500) {
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    } else {
                        val error = ApiHelper.handleApiError(response.errorBody())
                        failureHandler(error)
                    }
                }
            }

            override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                t?.let {
                    Log.e("JobMatch failure::", it.message.orEmpty() + "")
                    failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                }
            }
        })
    }

    /**
     *
     */
    fun acceptInterviewSlot(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        asignId: Int?,
        preferred_interview_date: String?,
        refferBy: Int?
    ) {
        mService.acceptInterviewSlot(asignId, preferred_interview_date, refferBy)
            ?.enqueue(object : Callback<SuccessResponse> {
                override fun onResponse(
                    call: Call<SuccessResponse>?,
                    response: Response<SuccessResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("accept int failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }


    /**
     *
     */
    fun saveSuperAvailability(
        successHandler: (SuperAvailailityResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?,
        super_toggle: Int?,
        globally_available: Int?,
        workerId: Int?
    ) {
        val map: HashMap<String, String>? = HashMap()
        map?.put("requested_by", userId.toString())
        map?.put("worker_id", workerId?.toString() + "")
        if (globally_available != null) {
            map?.put("globally_available", globally_available.toString())
        } else {
            map?.put("super_toggle", super_toggle.toString())
        }

        mService.saveSuperAvailability(map)
            .enqueue(object : Callback<SuperAvailailityResponse> {
                override fun onResponse(
                    call: Call<SuperAvailailityResponse>?,
                    response: Response<SuperAvailailityResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuperAvailailityResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("JobMatch failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun getJobCities(
        successHandler: (JobCititesResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getJobCities(userId)
            ?.enqueue(object : Callback<JobCititesResponse> {
                override fun onResponse(
                    call: Call<JobCititesResponse>?,
                    response: Response<JobCititesResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<JobCititesResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("JobMatch failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun getRefferJobLink(
        successHandler: (RefferJobResponse) -> Unit,
        failureHandler: (String) -> Unit,
        job_detail_id: Int?,
        job_id: Int?,
        userId: Int?
    ) {
        mService.getRefferJobLink(job_detail_id, job_id, userId)
            ?.enqueue(object : Callback<RefferJobResponse> {
                override fun onResponse(
                    call: Call<RefferJobResponse>?,
                    response: Response<RefferJobResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<RefferJobResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("reffer failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun rejectJob(
        successHandler: (FindJobResponse) -> Unit,
        failureHandler: (String) -> Unit,
        assignId: Int?,
        userId: Int?
    ) {
        mService.rejectJob(assignId, userId)
            ?.enqueue(object : Callback<FindJobResponse> {
                override fun onResponse(
                    call: Call<FindJobResponse>?,
                    response: Response<FindJobResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<FindJobResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("reject failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     *
     */
    fun getRequiredSkills(
        successHandler: (ReqiredSkillsResponse) -> Unit,
        failureHandler: (String) -> Unit,
        jobId: Int?,
        WorkerId: Int?
    ) {
        mService.getRequiredSkills(jobId, WorkerId)
            .enqueue(object : Callback<ReqiredSkillsResponse> {
                override fun onResponse(
                    call: Call<ReqiredSkillsResponse>?,
                    response: Response<ReqiredSkillsResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<ReqiredSkillsResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("reject failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }
}
