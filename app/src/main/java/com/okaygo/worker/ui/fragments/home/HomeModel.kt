package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.api.response.FindJobCategories
import com.okaygo.worker.data.modal.request.JobCategoryRequest
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class HomeModel(app: Application) : MyViewModel(app) {
    var responseJobCat = MutableLiveData<FindJobCategories>()

    fun getJobCategories(req: JobCategoryRequest?) {
        isLoading.value = true
        HomeRepository.getJobCategories({
            isLoading.value = false
            responseJobCat.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, req)
    }
}