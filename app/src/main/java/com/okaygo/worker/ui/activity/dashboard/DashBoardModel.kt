package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.direction_api_response.DirectionApiResponse
import com.okaygo.worker.data.modal.reponse.AppliedJobResponse
import com.okaygo.worker.data.modal.reponse.ODLocationResponse
import com.okaygo.worker.data.modal.reponse.SuccessResponse
import com.okaygo.worker.data.modal.reponse.WhatsAppSubscriptionResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class DashBoardModel(app: Application) : MyViewModel(app) {
    var responseWhatsappSubscription = MutableLiveData<WhatsAppSubscriptionResponse>()
    var responseWhatsappSubscribed = MutableLiveData<WhatsAppSubscriptionResponse>()
    var responseNotificatonCount = MutableLiveData<SuccessResponse>()
    var responseODJobs = MutableLiveData<AppliedJobResponse>()
    var responseODSaveLocation = MutableLiveData<ODLocationResponse>()
    var responseDirection = MutableLiveData<DirectionApiResponse>()

    fun getWhatsAppSubscription(userId: Int?) {
        isLoading.value = true
        DashboardRepository.getWhatsAppSubscription({
            isLoading.value = false
            responseWhatsappSubscription.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

    fun whatsAppSubscribe(mobile: String?, permission: Int?, userId: Int?) {
        isLoading.value = true
        DashboardRepository.whatsAppSubscribe({
            isLoading.value = false
            responseWhatsappSubscribed.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, mobile, permission, userId)
    }

    fun getNotificationCount(userId: Int?) {
        isLoading.value = true
        DashboardRepository.getNotificationCount({
            isLoading.value = false
            responseNotificatonCount.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

    fun getUpcomingOdJobs(userId: Int?) {
        isLoading.value = false
        DashboardRepository.getUpcomingOdJobs({
            isLoading.value = false
            responseODJobs.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

    fun saveLocationForOD(
        userId: Int?,
        latitude: Double?,
        longitude: Double?,
        speed: Float?,
        workerName: String?,
        time_millies: Long?,
        jobId: Int?,
        eta: String?,
        distance: String?
    ) {
        isLoading.value = false
        DashboardRepository.saveLocationForOD({
            isLoading.value = false
            responseODSaveLocation.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId, latitude, longitude, speed, workerName, time_millies, jobId, distance, eta)
    }

    fun getDirectionDuration(
        origin: String?,
        dest: String?,
        key: String?
    ) {
        isLoading.value = false
        DashboardRepository.getDirectionDuration({
            isLoading.value = false
            responseDirection.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, origin, dest, key)
    }
}