package com.okaygo.worker.ui.fragments.job_prefrence

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.chip.Chip
import com.okaygo.worker.R
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.JobPreferenceModel
import com.openkey.guest.ui.fragments.verification.OnBoardingModel
import kotlinx.android.synthetic.main.fragment_job_preference.*

class JobPreferenceFragment : BaseFragment(), View.OnClickListener {
    private lateinit var viewModelOnBoarding: OnBoardingModel
    private lateinit var viewModel: JobPreferenceModel
    var mSelectedJobs: ArrayList<String>? = null
    var mSelectedJobTypes = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(JobPreferenceModel::class.java)
        viewModelOnBoarding = ViewModelProvider(this).get(OnBoardingModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_job_preference, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        viewModelOnBoarding.getSelectedJobCat(userId)
    }

    private fun setListeners() {
        txtEdit?.setOnClickListener(this)
        txtSave?.setOnClickListener(this)
        txtDiscard?.setOnClickListener(this)
        imgBackIcon?.setOnClickListener(this)
    }

    private fun handleEdit() {
        txtEdit?.visibility = View.GONE
        linearBottomBtns?.visibility = View.VISIBLE
        for (i in 0 until chgJobCat?.childCount!!) {
            chgJobCat?.getChildAt(i)?.isEnabled = true
        }
    }

    private fun handleSaveClick() {
        linearBottomBtns?.visibility = View.GONE
        txtEdit?.visibility = View.VISIBLE

        mSelectedJobs?.clear()
        for (i in 0 until chgJobCat?.childCount!!) {
            val chip: Chip? = chgJobCat?.findViewById(chgJobCat?.getChildAt(i)?.getId() ?: 0)
            if (chip?.isChecked == true) {
                mSelectedJobs?.add(chip.id.toString() + "")
            }
        }
        mSelectedJobTypes = ""
        if (mSelectedJobs?.size ?: 0 > 3) {
            Utilities.showToast(
                activity,
                "You can select maximum 3 job categories."
            )
        } else if (mSelectedJobs?.size == 0) {
            Utilities.showToast(
                activity,
                "Select atlest one job category."
            )
        } else {
            for (i in 0 until chgJobCat?.childCount!!) {
                chgJobCat?.getChildAt(i)?.setEnabled(false)
            }
            for (i in mSelectedJobs?.indices!!) {
                if (i != mSelectedJobs?.size ?: 0 - 1) {
                    mSelectedJobTypes = mSelectedJobTypes + mSelectedJobs?.get(i) + ","
                } else {
                    mSelectedJobTypes = mSelectedJobTypes + mSelectedJobs?.get(i)
                }
            }
            viewModel.saveJobCat(workerId, mSelectedJobTypes)

        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBackIcon -> activity?.onBackPressed()
            R.id.txtEdit -> handleEdit()
            R.id.txtSave -> handleSaveClick()
            R.id.txtDiscard -> {
                linearBottomBtns?.visibility = View.GONE
                txtEdit?.visibility = View.VISIBLE
                chgJobCat?.removeAllViews()
                viewModelOnBoarding.getSelectedJobCat(userId)

            }
        }
    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.responseSaveJobCat.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {

                }
            }
        })

        viewModelOnBoarding.responseSelectedJobCat.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (mSelectedJobs == null) {
                        mSelectedJobs = ArrayList()
                    } else {
                        mSelectedJobs?.clear()
                    }
                    val interestedCat = it.response?.content?.get(0)?.interested_cat
                    if (interestedCat != null && interestedCat.isEmpty() == false) {
                        if (interestedCat.contains(",") == true) {
                            val selectedCat: Array<String> =
                                interestedCat.split(",".toRegex()).toTypedArray()
                            mSelectedJobs?.addAll(selectedCat)
                        } else {
                            mSelectedJobs?.add(interestedCat)
                        }
                    }
                    viewModelOnBoarding.fetchInterestedJobTypes()
                }
            }
        })
        viewModelOnBoarding.responseIntrestedJob.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    for (i in 0 until it?.response?.content?.size!!) {
                        val tagName: String? = it?.response?.content?.get(i)?.typeDesc
                        val chip = Chip(activity)
//                        if (i == 0) {
//                            chip.isChecked = true
//                        }
                        chip.isCheckable = true
                        chip.isCheckedIconVisible = false
                        chip.text = tagName
                        chip.id = it?.response?.content?.get(i)?.id ?: 0
                        if (mSelectedJobs?.contains(it.response?.content?.get(i)?.id?.toString()) == true) {
                            chip.isChecked = true
                        }
                        chip.setChipBackgroundColorResource(R.color.bg_chips_drawable)
                        chip.setChipStrokeColorResource(R.color.theme)
                        chip.chipStrokeWidth = 1f
                        chip.isClickable = true
                        chip.isCheckedIconVisible = false
                        chip.isEnabled = false
                        if (tagName?.contains("Chef") == true) {
                            chip.closeIcon = resources.getDrawable(R.drawable.ic_cook)
                        } else if (tagName?.contains("Restaurant") == true) {
                            chip.closeIcon = resources.getDrawable(R.drawable.ic_resturants)
                        } else if (tagName?.contains("Hotel") == true) {
                            chip.closeIcon = resources.getDrawable(R.drawable.ic_hotel_services)
                        } else if (tagName?.contains("Office Job") == true) {
                            chip.closeIcon = resources.getDrawable(R.drawable.ic__office_job)
                        } else if (tagName?.contains("Delivery") == true) {
                            chip.closeIcon = resources.getDrawable(R.drawable.ic_delivery)
                        } else if (tagName?.contains("Warehouse") == true) {
                            chip.closeIcon = resources.getDrawable(R.drawable.ic_warehouse)
                        } else if (tagName?.contains("Event") == true) {
                            chip.closeIcon = resources.getDrawable(R.drawable.ic_events)
                        } else if (tagName?.contains("Sales") == true) {
                            chip.closeIcon = resources.getDrawable(R.drawable.ic_sales)
                        } else if (tagName?.contains("Customer Support") == true) {
                            chip.closeIcon = resources.getDrawable(R.drawable.ic_customer_support)
                        } else if (tagName?.contains("Education") == true) {
                            chip.closeIcon = resources.getDrawable(R.drawable.ic_education)
                        } else {
                            chip.closeIcon = resources.getDrawable(R.drawable.ic_other_jobs)
                        }
                        chip.isCloseIconVisible = true
                        chip.isCheckedIconVisible = false
//                        if (chip.text.toString()?.equals("Other") == false) {
                        chgJobCat?.addView(chip)
//                        }
                    }
                    for (i in 0 until chgJobCat?.childCount!!) {
                        chgJobCat?.getChildAt(i)?.isEnabled = false
                    }
                }
            }
        })

        viewModelOnBoarding.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }
        })

        viewModelOnBoarding.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }
}