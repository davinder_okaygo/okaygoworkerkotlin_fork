package com.okaygo.worker.ui.fragments.otp

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserCodeDeliveryDetails
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.VerificationHandler
import com.okaygo.worker.R
import com.okaygo.worker.cognito.AuthHelper
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.okaygo.worker.ui.fragments.choose_password.ChosePasswordFragment
import com.okaygo.worker.ui.fragments.login.model.AuthDetails
import com.okaygo.worker.ui.fragments.login.model.AuthenticationSuccess
import com.openkey.guest.help.Preferences
import com.openkey.guest.help.utils.Constants
import kotlinx.android.synthetic.main.fragment_otp.*
import java.util.*

class OtpFragment : BaseFragment() {
    private var userName: String? = null
    var fault = false


    var resendConfCodeHandler: VerificationHandler? = object : VerificationHandler {
        override fun onSuccess(cognitoUserCodeDeliveryDetails: CognitoUserCodeDeliveryDetails?) {
            Utilities.hideLoader()
        }

        override fun onFailure(exception: Exception) {
            Utilities.hideLoader()
            Utilities.showToast(activity, "Confirmation code request has failed")
        }
    }
    var confHandler1: GenericHandler? = object : GenericHandler {
        override fun onSuccess() {
            Utilities.hideLoader()

            showDialogMessage("Success!", "$userName has been confirmed!")
            AuthHelper.setPhoneVerified(true)
            signInUser()
        }

        override fun onFailure(exception: java.lang.Exception) {
            Utilities.hideLoader()
            showDialogMessage("Confirmation failed", AuthHelper.formatException(exception))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_otp, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        imgNext?.setOnClickListener {
            if (edtOtp?.getText()?.length ?: 0 > 5) {
                Utilities.showLoader(activity)
                sendConfCode()
            } else {
                if (edtOtp?.getText()?.length == 0) {
                    edtOtp?.setError("Enter OTP")
                } else {
                    edtOtp?.setError("Invalid OTP")
                }
            }
        }
    }

    private fun init() {
        val bundle: Bundle? = arguments
        userName = bundle?.getString("name")
        fault = bundle?.getBoolean("fault", false) ?: false
        if (fault) {
            Utilities.showLoader(activity)
            reqOTp()
        } else {
            if (bundle?.containsKey("destination") == true) {
                val dest = bundle?.getString("destination")
                val delMed = bundle?.getString("deliveryMed")
                if (dest != null && delMed != null && dest.length > 0 && delMed.length > 0) {
//                        otpText.setText("A confirmation code was sent to " + dest.replace("+","") + " via " + delMed);
                    txtOtpTitle?.setText(resources.getString(R.string.enter_otp_sent) + " " + userName)
                }
            }
        }
    }

    fun reqOTp() {
        Utilities.showToast(activity, "Requesting verification code...")
        AuthHelper.getPool().getUser(userName)
            .resendConfirmationCodeInBackground(resendConfCodeHandler)
    }

    private fun launchUser() {
        Utilities.hideLoader()
        val fragment: ChosePasswordFragment? = ChosePasswordFragment()
//        Preferences.prefs?.saveValue(Constants.SAVE_USERNAME,userName)
        val bundle: Bundle? = Bundle()
        bundle?.putString("name", userName)
        bundle?.putString("password", userName)
        fragment?.arguments = bundle
        fragment?.let {
            attachFragment(it, true)
        }

    }

    private fun signInUser() {
        Utilities.showLoader(activity)
        userName?.let {

            AuthHelper.setUser(it)
            AuthHelper.getPool().getUser(it).getSessionInBackground(
                Utilities.getAuthHandler(
                    onAuthSuccess,
                    onAuthDetails,
                    onAuthFailure
                )
            )
        }
    }

    /**
     * handle authentication success reposne for cognito
     */
    private val onAuthSuccess: (AuthenticationSuccess?) -> Unit = { it ->
        Utilities.hideLoader()
        Log.d("TAG", " -- Auth Success")
        AuthHelper.setCurrSession(it?.cognitoUserSession)
        AuthHelper.newDevice(it?.device)
        try {
            Preferences.prefs?.saveValue(
                Constants.ACCESS_TOKEN,
                AuthHelper.getCurrSession().accessToken.jwtToken
            )
        } catch (ex: java.lang.Exception) {
        }
//        Preferences.prefs?.saveValue(Constants.IS_CHOSE_PASS,true)
        launchUser()
    }

    private val onAuthDetails: (AuthDetails?) -> Unit = { it ->
        Utilities.hideLoader()
        Locale.setDefault(Locale.US)
        it?.authenticationContinuation?.let { it1 -> getUserAuthentication(it1, it?.username) }
    }

    /**
     * auth error for cognito
     */
    private val onAuthFailure: (Exception?) -> Unit = { it ->
        Utilities.hideLoader()
        Utilities.showToast(activity, "Sign-in failed")
    }

    private fun getUserAuthentication(continuation: AuthenticationContinuation, username: String?) {
        userName = username
        userName?.let {
            AuthHelper.setUser(it)
            val authenticationDetails = AuthenticationDetails(it, it, null)
            continuation.setAuthenticationDetails(authenticationDetails)
            continuation.continueTask()
        }
    }

    private fun showDialogMessage(title: String?, body: String?) {
        var alertDialog: AlertDialog? = null
        val builder = activity?.let { AlertDialog.Builder(it) }
        builder?.setTitle(title)?.setMessage(body)?.setNeutralButton("OK") { dialog, which ->
            try {
                alertDialog?.dismiss()
            } catch (e: java.lang.Exception) {
//                exit()
                activity?.onBackPressed()
            }
        }
        alertDialog = builder?.create()
        alertDialog?.show()
    }

//    private fun exit() {
//        val intent = Intent()
//        if (userName == null) userName = ""
//        intent.putExtra("name", userName)
//        setResult(Activity.RESULT_OK, intent)
//        finish()
//    }

    private fun sendConfCode() {
        userName?.let {
            AuthHelper.getPool().getUser(userName)
                .confirmSignUpInBackground(edtOtp?.getText().toString(), true, confHandler1)

        }
    }

}