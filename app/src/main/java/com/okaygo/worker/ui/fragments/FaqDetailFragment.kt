package com.okaygo.worker.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.okaygo.worker.R
import com.openkey.guest.help.utils.Constants
import kotlinx.android.synthetic.main.fragment_faq_detail.*

class FaqDetailFragment : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_faq_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txtTitle?.text = Constants.FAQ_TITLE
        txtDesc?.text = Constants.FAQ_DESC
        btnGotIt?.setOnClickListener { activity?.onBackPressed() }
        imgBack?.setOnClickListener { activity?.onBackPressed() }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Constants.FAQ_TITLE = ""
        Constants.FAQ_DESC = ""
    }
}