package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.OnBoardingStatusResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class SummaryModel(app: Application) : MyViewModel(app) {
    var response = MutableLiveData<OnBoardingStatusResponse>()

    /**
     * check onBoarding is complete or not
     */
    fun getOnBoardingStatus(userId: Int?) {
        isLoading.value = true
        SummaryRepository.getOnBoardingStatus({
            isLoading.value = false
            response.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }
}