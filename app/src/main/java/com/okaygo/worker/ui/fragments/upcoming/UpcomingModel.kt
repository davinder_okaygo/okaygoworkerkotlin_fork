package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.AppliedJobResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class UpcomingModel(app: Application) : MyViewModel(app) {
    var responseUpcomingJob = MutableLiveData<AppliedJobResponse>()

    /**
     *
     */
    fun getUpcomingJobs(userId: Int?, isLoader: Boolean? = true) {
        isLoading.value = isLoader
        UpcomingRepository.getUpcomingJobs({
            isLoading.value = false
            responseUpcomingJob.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

}