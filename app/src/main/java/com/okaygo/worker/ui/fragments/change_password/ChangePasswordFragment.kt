package com.okaygo.worker.ui.fragments.change_password

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler
import com.okaygo.worker.R
import com.okaygo.worker.cognito.AuthHelper
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_change_password.*

class ChangePasswordFragment : BaseFragment() {
    var callback: GenericHandler = object : GenericHandler {
        override fun onSuccess() {
            Utilities.hideLoader()
            Utilities.showToast(activity, "Password changed")
            edtCurrentPass?.setText("")
            edtNewPass?.setText("")
            edtConfPass?.setText("")
        }

        override fun onFailure(exception: Exception) {
            Utilities.hideLoader()
            showDialogMessage(
                "Password change failed",
                AuthHelper.formatException(exception)
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_change_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imgBackIcon?.setOnClickListener { activity?.onBackPressed() }
        txtChangePass?.setOnClickListener { changePaswordHandling() }
    }

    private fun changePaswordHandling() {
        if (edtCurrentPass?.text?.length ?: 0 <= 12 && edtCurrentPass?.text?.length ?: 0 >= 6) {
            if (edtNewPass?.text?.length ?: 0 <= 12 && edtNewPass?.text?.length ?: 0 >= 6) {
                if (edtConfPass?.text?.length ?: 0 <= 12 && edtConfPass?.text?.length ?: 0 >= 6 && edtConfPass?.text?.toString()
                        .equals(edtNewPass?.text?.toString())
                ) {
                    changePassword()
                } else {
                    if (edtConfPass?.text?.length ?: 0 == 0) {
                        Utilities.showToast(activity, "Confirm new password")
                    } else {
                        Utilities.showToast(activity, "Password does not match")
                    }
                }
            } else {
                if (edtNewPass?.text?.length ?: 0 == 0) {
                    Utilities.showToast(activity, "Enter new password")
                } else {
                    Utilities.showToast(activity, "New password : length between 6 - 12")
                }
            }
        } else {
            if (edtCurrentPass?.text?.length ?: 0 == 0) {
                Utilities.showToast(activity, "Enter Current Password")
            } else {
                Utilities.showToast(activity, "Current Password : Length between 6 - 12")
            }
        }
    }

    private fun changePassword() {
        Utilities.showLoader(activity)
        val userName = AuthHelper.getCurrUser()
        AuthHelper.getPool().getUser(userName).changePasswordInBackground(
            edtCurrentPass?.getText().toString(),
            edtNewPass?.getText().toString(),
            callback
        )
    }

    private fun showDialogMessage(
        title: String?,
        body: String?
    ) {
        var userDialog: AlertDialog? = null
        val builder = activity?.let { AlertDialog.Builder(it) }
        builder?.setTitle(title)?.setMessage(body)?.setNeutralButton("OK") { dialog, which ->
            userDialog?.dismiss()
        }
        userDialog = builder?.create()
        userDialog?.show()
    }
}