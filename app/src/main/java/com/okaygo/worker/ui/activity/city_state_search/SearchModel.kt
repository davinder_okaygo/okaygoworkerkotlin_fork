package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.CityStateResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class SearchModel(app: Application) : MyViewModel(app) {
    var response = MutableLiveData<CityStateResponse>()

    /**
     * fetch list of cities from local server accroding to text
     */
    fun getCityWithText(text: String?) {
        isLoading.value = true
        SearchRepository.getCityWithText({
            isLoading.value = false
            response.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, text)
    }
}