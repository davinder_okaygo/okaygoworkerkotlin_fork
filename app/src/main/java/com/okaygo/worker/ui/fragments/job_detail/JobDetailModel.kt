package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.JobDetailResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class JobDetailModel(app: Application) : MyViewModel(app) {
    var responseJobDetail = MutableLiveData<JobDetailResponse>()

    /**
     *
     */
    fun getJobDetail(jobId: Int?) {
        isLoading.value = true
        JobDetailRepository.getJobDetail({
            isLoading.value = false
            responseJobDetail.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, jobId)
    }
}