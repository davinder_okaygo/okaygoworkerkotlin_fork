package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.*
import com.okaygo.worker.data.modal.request.FindJobRequest
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class JobListModel(app: Application) : MyViewModel(app) {
    var responseFindJob = MutableLiveData<FindJobResponse>()
    var responseMatchJob = MutableLiveData<JobMatchResponse>()
    var responseApplyJob = MutableLiveData<SuccessResponse>()
    var responseSupperToggle = MutableLiveData<SuperAvailailityResponse>()
    var responseAcceptInterview = MutableLiveData<SuccessResponse>()
    var responseJobCities = MutableLiveData<JobCititesResponse>()
    var responseRefferLink = MutableLiveData<RefferJobResponse>()
    var responseRejectJob = MutableLiveData<FindJobResponse>()
    var responseRequiredSkills = MutableLiveData<ReqiredSkillsResponse>()

    fun getJobsAccordingToCat(req: FindJobRequest?) {
        isLoading.value = true
        JobListRepository.getJobsAccordingToCat({
            isLoading.value = false
            responseFindJob.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, req)
    }

    fun matchJob(userId: Int?, jobId: Int?) {
        isLoading.value = true
        JobListRepository.matchJob({
            isLoading.value = false
            responseMatchJob.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId, jobId)
    }

    fun applyJob(assignId: Int?, userId: Int?, refferedBy: Int?) {
        isLoading.value = true
        JobListRepository.applyJob({
            isLoading.value = false
            responseApplyJob.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, assignId, userId, refferedBy)
    }

    fun saveSuperAvailability(
        userId: Int?,
        super_toggle: Int?,
        globally_available: Int?,
        workerId: Int?
    ) {
        isLoading.value = true
        JobListRepository.saveSuperAvailability({
            isLoading.value = false
            responseSupperToggle.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId, super_toggle, globally_available, workerId)
    }

    fun acceptInterviewSlot(asignId: Int?, preferred_interview_date: String?, refferBy: Int?) {
        isLoading.value = true
        JobListRepository.acceptInterviewSlot({
            isLoading.value = false
            responseAcceptInterview.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, asignId, preferred_interview_date, refferBy)
    }

    fun getJobCities(userId: Int?) {
        isLoading.value = true
        JobListRepository.getJobCities({
            isLoading.value = false
            responseJobCities.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

    fun getRefferJobLink(job_detail_id: Int?, job_id: Int?, userId: Int?) {
        isLoading.value = true
        JobListRepository.getRefferJobLink({
            isLoading.value = false
            responseRefferLink.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, job_detail_id, job_id, userId)
    }

    fun rejectJob(assignId: Int?, userId: Int?) {
        isLoading.value = true
        JobListRepository.rejectJob({
            isLoading.value = false
            responseRejectJob.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, assignId, userId)
    }

    fun getRequiredSkills(jobId: Int?, workerId: Int?) {
        isLoading.value = true
        JobListRepository.getRequiredSkills({
            isLoading.value = false
            responseRequiredSkills.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, jobId, workerId)
    }
}