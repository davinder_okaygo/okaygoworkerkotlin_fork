package com.okaygo.worker.ui.fragments.notification

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.okaygo.worker.R
import com.okaygo.worker.adapters.NotificationAdapter
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.reponse.NotificationResponse
import com.okaygo.worker.data.modal.reponse.Notifications
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.NotificationModel
import kotlinx.android.synthetic.main.fragment_alert.*

class NotfFragment : BaseFragment() {
    private lateinit var viewModel: NotificationModel
    private var mNotificationData: Notifications? = null

    private var mList: ArrayList<Notifications>? = null
    private var mAdapter: NotificationAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(NotificationModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_alert, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        OkayGoFirebaseAnalytics.inapp_notification_view()
        setListeners()
    }


    override fun onResume() {
        super.onResume()
        Log.e("Notification", "true")
        viewModel.getNotifications(userId, 0)
    }

    private fun setListeners() {
        swipeRefresh?.setOnRefreshListener {
            swipeRefresh?.isRefreshing = true
            if (Utilities.isOnline(context)) {
                image?.visibility = View.GONE
                warning?.visibility = View.GONE
//                currentPage = 0
                viewModel.getNotifications(userId, 0)
            } else {
                image?.visibility = View.VISIBLE
                image?.setImageResource(R.drawable.ic_no_wifi)
                warning?.visibility = View.VISIBLE
                warning?.text = "No Internet Connection"
                swipeRefresh?.isRefreshing = false
            }
        }
    }

    private fun setAdapter(reponse: NotificationResponse?) {
        if (mList == null) {
            mList = ArrayList()
        } else {
            mList?.clear()
        }
        reponse?.response?.content?.let { mList?.addAll(it) }
        mAdapter =
            activity?.let {
                NotificationAdapter(
                    it,
                    false,
                    mList,
                    OnNotificationClick,
                    OnPosBtnClick,
                    OnNagBtnClick
                )
            }
        val linearLayoutManager = LinearLayoutManager(activity)
        recylerNotification?.setAdapter(mAdapter)
        recylerNotification.setLayoutManager(linearLayoutManager)
    }

    /**
     * click listener when edit any exp
     */
    private val OnNotificationClick: (Notifications?, Int?) -> Unit = { it, pos ->
//        mNotificationData = it
//        Dialogs.alertDialog(
//            activity,
//            "Are you sure you want to delete this notification?",
//            "Yes",
//            "No",
//            onAlertPosBtnClick
//        )
    }

    private val onAlertPosBtnClick: () -> Unit = {
        viewModel.deleteNotification(userId, mNotificationData?.notificationId)

    }

    /**
     * click listener when edit any exp
     */
    private val OnPosBtnClick: (Notifications?, Int?) -> Unit = { it, clickFor ->

    }

    /**
     * click listener when edit any exp
     */
    private val OnNagBtnClick: (Notifications?, Int?) -> Unit = { it, clickFor ->

    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.responseNotification.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (it.response?.content?.isEmpty() == true) {
                        image?.visibility = View.VISIBLE
                        image?.setImageResource(R.drawable.ic_no_records)
                        warning?.visibility = View.VISIBLE
                        warning?.text = "No data found"
                        swipeRefresh?.setRefreshing(false)
                    } else {
                        image?.visibility = View.GONE
                        warning?.visibility = View.GONE
                        swipeRefresh?.setRefreshing(false)
                        setAdapter(it)
                    }
                }
            }

        })
        viewModel.responseDelNotification.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    viewModel.getNotifications(userId, 0)
                }
            }
        })
        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                    swipeRefresh?.setRefreshing(false)
                }
            }

        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }
}