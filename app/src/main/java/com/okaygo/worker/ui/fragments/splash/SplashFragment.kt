package com.okaygo.worker.ui.fragments.splash

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.analytics.FirebaseAnalytics
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.AppResponse
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.activity.dashboard.DashBoardActivity
import com.okaygo.worker.ui.activity.login_onboarding.LoginOnBoardingActivity
import com.okaygo.worker.ui.activity.login_onboarding.OnBoardingActivity
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.Preferences
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.SplashModel

class SplashFragment : BaseFragment() {
    private lateinit var viewModel: SplashModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SplashModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Constants.IS_SPLASH = true
        viewModel.getInAppData(Preferences.prefs?.getInt(Constants.ID, 0))
    }

    /**
     * handl api's reposne
     */
    private fun attachObservers() {
        viewModel.response.observe(this, Observer {
            it?.let {
                val message = "Welcome : ${it.message}"
                Log.e("Auth SUccess::", message + "")
                setUserDataOnFireBase(it.response)
            }
        })

        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }

        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }

    /**
     * set in app data to firebase anayltics
     */
    private fun setUserDataOnFireBase(response: AppResponse?) {
        activity?.let {
            if (response?.user_id ?: 0 > 0) {
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("user_id", response?.user_id.toString() + "")
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_age", response?.worker_age.toString() + "")
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_gender", response?.worker_gender)
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_city", response?.worker_city)
                FirebaseAnalytics.getInstance(it).setUserProperty(
                    "worker_last_salary",
                    response?.worker_last_salary.toString() + ""
                )
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_english_level", response?.worker_english_level)
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_education", response?.worker_education)
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_job_type", response?.worker_job_type)
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_category", response?.worker_category)
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_exp", response?.worker_exp.toString() + "")
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty(
                        "worker_cv_status",
                        response?.worker_cv_status.toString() + ""
                    )
                FirebaseAnalytics.getInstance(it).setUserProperty(
                    "worker_onboard_status",
                    response?.worker_onboard_status.toString() + ""
                )
                FirebaseAnalytics.getInstance(it).setUserProperty(
                    "worker_job_apply_count",
                    response?.worker_job_apply_count.toString() + ""
                )
                FirebaseAnalytics.getInstance(it).setUserProperty(
                    "worker_interview_count",
                    response?.worker_interview_count.toString() + ""
                )
                FirebaseAnalytics.getInstance(it).setUserProperty(
                    "worker_selected_count",
                    response?.worker_selected_count.toString() + ""
                )
                FirebaseAnalytics.getInstance(it).setUserProperty(
                    "worker_joined_count",
                    response?.worker_joined_count.toString() + ""
                )
                FirebaseAnalytics.getInstance(it)
                    .setUserProperty("worker_last_activity", response?.worker_last_activity)
            }
            val isOnBoardingDone = Preferences.prefs?.getBoolean(Constants.DONE_ONBOARDING, false);
            val isLoggedin = Preferences.prefs?.getBoolean(Constants.IS_LOGGED_IN, false);

            if (isLoggedin == true && isOnBoardingDone == true) {
                startActivity(Intent(it, DashBoardActivity::class.java))
            } else if (isLoggedin == true && isOnBoardingDone == false) {
//                startActivity(Intent(it, SummaryActivity::class.java))
                startActivity(Intent(it, OnBoardingActivity::class.java))
            } else {
                startActivity(Intent(it, LoginOnBoardingActivity::class.java))
            }
            it.finish()
        }

    }
}