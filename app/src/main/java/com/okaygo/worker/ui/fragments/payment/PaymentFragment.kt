package com.okaygo.worker.ui.fragments.payment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.okaygo.worker.R
import com.okaygo.worker.adapters.PaymentAdapter
import com.okaygo.worker.data.modal.reponse.PaymentResponse
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.fragments.BaseFragment
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.PaymentModel
import kotlinx.android.synthetic.main.fragment_payment.*

class PaymentFragment : BaseFragment() {
    private var mAdapter: PaymentAdapter? = null
    private var mPaymentList: ArrayList<PaymentResponse>? = null
    private lateinit var viewModel: PaymentModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(PaymentModel::class.java)
        attachObservers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_payment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Constants.BOTTOM_MENU_ITEM_SELECTED = 2
        setAdapter()
    }

    override fun onResume() {
        super.onResume()
        if (Utilities.isOnline(activity)) {

            viewModel.getReferralTransection(userId)
            viewModel.getDueTransection(userId)
        } else {
            imgNoData?.visibility = View.VISIBLE
            cardTop?.visibility = View.GONE
            txtMsg?.setVisibility(View.VISIBLE)
        }
    }

    private fun setAdapter() {
        if (mPaymentList == null) {
            mPaymentList = ArrayList()
        }
        val linearLayoutManager = LinearLayoutManager(activity)
        mPaymentList?.let {
            mAdapter = PaymentAdapter(context, it)
            recylerPayment?.layoutManager = linearLayoutManager
            recylerPayment?.adapter = mAdapter
        }
    }

    /**
     * handle all api resposne
     */
    private fun attachObservers() {
        viewModel.responseTransection.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    if (mPaymentList == null) {
                        mPaymentList = ArrayList()
                    } else {
                        mPaymentList?.clear()
                    }
//                    viewModel.getPaymentTransection(userId)
                    if (it.response?.content != null && it.response.content.isEmpty() == false) {
                        mPaymentList?.addAll(it.response.content)
                        mAdapter?.notifyDataSetChanged()
                    }
                }
            }
        })

        viewModel.responseTransectionReferral.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    it.response?.content?.let { it1 -> mPaymentList?.addAll(it1) }
                    mAdapter?.notifyDataSetChanged()
                }
            }
        })

        viewModel.responseDueTransection.observe(this, Observer {
            it?.let {
                if (it.code == Constants.SUCCESS) {
                    txtAmountDue?.text =
                        "₹ ${Math.round(it.response?.amountDue?.replace("-", "")?.toFloat() ?: 0f)}"

                    if (it.response?.amountPaid?.equals("0") == false) {
                        txtLastTrans?.text =
                            "₹ ${it.response?.amountPaid} aapke Paytm pe bhej diye on ${Utilities.getFormatedDate(
                                it.response?.insertedOn?.split(" ")?.get(0)
                            )}"
                    }

                    viewModel.getPaymentTransection(userId)
                }
            }
        })

        viewModel.apiError.observe(this, Observer {
            if (isAdded) {
                it?.let {
                    Utilities.showToast(activity, it)
                }
            }
        })

        viewModel.isLoading.observe(this, Observer {
            it.let {
                if (it == true) {
                    Utilities.showLoader(activity)
                } else {
                    Utilities.hideLoader()
                }
            }
        })
    }
}