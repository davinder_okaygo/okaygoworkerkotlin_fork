package com.openkey.guest.ui.fragments.verification

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.okaygo.worker.data.modal.reponse.DueTransectionResponse
import com.okaygo.worker.data.modal.reponse.PaymentTransectionResponse
import com.okaygo.worker.data.modal.reponse.ReferralPaymentResponse
import com.openkey.guest.data.MyViewModel

/**
 * @author Davinder Goel OkayGo.
 */
class PaymentModel(app: Application) : MyViewModel(app) {
    var responseTransection = MutableLiveData<PaymentTransectionResponse>()
    var responseTransectionReferral = MutableLiveData<ReferralPaymentResponse>()
    var responseDueTransection = MutableLiveData<DueTransectionResponse>()

    /**
     *
     */
    fun getPaymentTransection(userId: Int?) {
        isLoading.value = false
        PaymentRepository.getPaymentTransection({
            isLoading.value = false
            responseTransection.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

    /**
     *
     */
    fun getReferralTransection(userId: Int?) {
        isLoading.value = false
        PaymentRepository.getReferralTransection({
            isLoading.value = false
            responseTransectionReferral.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }

    /**
     *
     */
    fun getDueTransection(userId: Int?) {
        isLoading.value = false
        PaymentRepository.getDueTransection({
            isLoading.value = false
            responseDueTransection.value = it
        }, {
            isLoading.value = false
            apiError.value = it
        }, userId)
    }
}