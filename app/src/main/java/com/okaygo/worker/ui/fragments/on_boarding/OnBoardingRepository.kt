package com.okaygo.worker.ui.fragments.on_boarding

import android.util.Log
import com.okaygo.worker.data.api.response.OtherSkills
import com.okaygo.worker.data.api.response.UserSkillsResponse
import com.okaygo.worker.data.modal.reponse.*
import com.okaygo.worker.data.modal.request.AddSkillsRequest
import com.okaygo.worker.data.modal.request.AvailbilityRequest
import com.okaygo.worker.data.modal.request.UserDetailRequest
import com.openkey.guest.data.Api.ApiHelper
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

/**
 * @author Davinder Goel OkayGo.
 */
object OnBoardingRepository {
    private val mService = ApiHelper.getService()

    /**
     * save personal detail on our server
     */
    fun savePersonalDetail(
        successHandler: (PersonalDetailResponse) -> Unit,
        failureHandler: (String) -> Unit,
        req: UserDetailRequest?
    ) {
        Log.e("UserDetailRequest", req.toString())
        val map: HashMap<String, String>? = HashMap()
        map?.put("user_id", req?.user_id ?: "0")
        if (req?.profile_photo == null) {
            map?.put("new_city_id", req?.new_city_id ?: "")
            map?.put("date_of_birth", req?.date_of_birth ?: "")
            map?.put("first_name", req?.first_name.toString())
            map?.put("last_name", req?.last_name.toString())
            map?.put("gender", req?.gender.toString())
            if (req?.email_id != null && req.email_id.isNotEmpty()) {
                map?.put("email_id", req?.email_id ?: "")
            }
            map?.put("city", req?.city ?: "")
        } else {
            map?.put("profile_photo", req.profile_photo ?: "0")
        }

        mService.savePersonalDetail(map)
            .enqueue(object : Callback<PersonalDetailResponse> {
                override fun onResponse(
                    call: Call<PersonalDetailResponse>?,
                    response: Response<PersonalDetailResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<PersonalDetailResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("save personal  failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     * save edu on local server
     */
    fun saveEducationLang(
        successHandler: (SaveEducationResponse) -> Unit,
        failureHandler: (String) -> Unit,
        req: SaveEducationRequest?
    ) {

        mService.saveEducationLang(
            req?.requested_by,
            req?.qualification_id,
            req?.english_known_level,
            req?.worker_id
        )
            ?.enqueue(object : Callback<SaveEducationResponse> {
                override fun onResponse(
                    call: Call<SaveEducationResponse>?,
                    response: Response<SaveEducationResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SaveEducationResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("SaveEdu  failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     * get edu from server
     */
    fun getEducation(
        successHandler: (QualificationResponse) -> Unit,
        failureHandler: (String) -> Unit
    ) {
        mService.getEducationQualification()
            ?.enqueue(object : Callback<QualificationResponse> {
                override fun onResponse(
                    call: Call<QualificationResponse>?,
                    response: Response<QualificationResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<QualificationResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("get edu  failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     * get all job cate from server
     */
    fun fetchInterestedJobTypes(
        successHandler: (JobCategoryResponse) -> Unit,
        failureHandler: (String) -> Unit
    ) {
        mService.fetchInterestedJobTypes()
            ?.enqueue(object : Callback<JobCategoryResponse> {
                override fun onResponse(
                    call: Call<JobCategoryResponse>?,
                    response: Response<JobCategoryResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<JobCategoryResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("intrested job failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     * get worker exp from local server
     */
    fun getWorkerExp(
        successHandler: (ExperienceResponse) -> Unit,
        failureHandler: (String) -> Unit,
        workerId: Int?
    ) {
        mService.getWorkerExp(workerId, 100)
            ?.enqueue(object : Callback<ExperienceResponse> {
                override fun onResponse(
                    call: Call<ExperienceResponse>?,
                    response: Response<ExperienceResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<ExperienceResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("getWorkerExp failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     * delete any exp from local server
     */
    fun deleteExp(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        expId: Int?
    ) {
        mService.deleteExp(expId)
            ?.enqueue(object : Callback<SuccessResponse> {
                override fun onResponse(
                    call: Call<SuccessResponse>?,
                    response: Response<SuccessResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("delete exp failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     * get file link after uplaod on local server
     */
    fun getDocLink(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        file: File? = null,
        userId: Int?
    ) {
        val userId = RequestBody.create(okhttp3.MediaType.parse("text/plain"), userId.toString())

        val extension: String? =
            file?.getAbsolutePath()?.substring(file?.getAbsolutePath()?.lastIndexOf(".") ?: 0)
        Log.e("exrension", extension)
        var fileType = "image/*"
        when (extension) {
            "doc",
            "docx" -> fileType = "application/msword"
            "jpg",
            "JPEG",
            "jpeg",
            "png" -> fileType = "image/*"
            "pdf" -> fileType = "application/pdf"
        }

        // Create a request body with file and image media type
        val fileReqBody = RequestBody.create(okhttp3.MediaType.parse(fileType), file)
        // Create MultipartBody.Part using file request-body,file name and part name
        val part = MultipartBody.Part.createFormData("file", file?.getName(), fileReqBody)

        mService.getDocLink(userId, part)
            ?.enqueue(object : Callback<SuccessResponse> {
                override fun onResponse(
                    call: Call<SuccessResponse>?,
                    response: Response<SuccessResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("doc link failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     * uplaod file link on db
     */
    fun uplaodCV(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        cvPath: String?,
        workerId: Int?,
        fileName: String?
    ) {

        mService.uploadCv(cvPath, workerId, fileName)
            ?.enqueue(object : Callback<SuccessResponse> {
                override fun onResponse(
                    call: Call<SuccessResponse>?,
                    response: Response<SuccessResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("doc link failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     * save seletced job cat
     */
    fun saveInterestedCat(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        workerId: Int?,
        catgories: String?
    ) {

        mService.saveInterestedCat(workerId, catgories)
            ?.enqueue(object : Callback<SuccessResponse> {
                override fun onResponse(
                    call: Call<SuccessResponse>?,
                    response: Response<SuccessResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("saveInterCat failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     * set 1 if their is no any exp
     */
    fun setNoExp(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        workerId: Int?
    ) {

        mService.setNoExp(1, workerId)
            ?.enqueue(object : Callback<SuccessResponse> {
                override fun onResponse(
                    call: Call<SuccessResponse>?,
                    response: Response<SuccessResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("set no exp failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    /**
     * update last salary on local server
     */
    fun updateLastSalary(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        lastSalary: Int?,
        workerId: Int?
    ) {
        mService.updateLastSalary(lastSalary, workerId)
            ?.enqueue(object : Callback<SuccessResponse> {
                override fun onResponse(
                    call: Call<SuccessResponse>?,
                    response: Response<SuccessResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("LastSalary failure::", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun getSelectedJobCat(
        successHandler: (SelectedJobCatResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getSelectedJobCat(userId)
            ?.enqueue(object : Callback<SelectedJobCatResponse> {
                override fun onResponse(
                    call: Call<SelectedJobCatResponse>?,
                    response: Response<SelectedJobCatResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SelectedJobCatResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("selectedJobCat failure", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun getUserSkills(
        successHandler: (UserSkillsResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?
    ) {
        mService.getUserSkills(userId)
            ?.enqueue(object : Callback<UserSkillsResponse> {
                override fun onResponse(
                    call: Call<UserSkillsResponse>?,
                    response: Response<UserSkillsResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<UserSkillsResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("selectedJobCat failure", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun getAllSkills(
        successHandler: (AllSkillResponse) -> Unit,
        failureHandler: (String) -> Unit,
        jobCatId: String?,
        jobTypeId: String?
    ) {
        mService.getAllSkills(jobCatId, jobTypeId)
            ?.enqueue(object : Callback<AllSkillResponse> {
                override fun onResponse(
                    call: Call<AllSkillResponse>?,
                    response: Response<AllSkillResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<AllSkillResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("selectedJobCat failure", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun saveSkills(
        successHandler: (AllSkillResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?,
        request: AddSkillsRequest?
    ) {
        mService.saveSkills(request, userId)
            ?.enqueue(object : Callback<AllSkillResponse> {
                override fun onResponse(
                    call: Call<AllSkillResponse>?,
                    response: Response<AllSkillResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<AllSkillResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("selectedJobCat failure", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun addSkills(
        successHandler: (OtherSkills) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?,
        skillName: String?
    ) {
        mService.addSkills(userId, skillName)
            ?.enqueue(object : Callback<OtherSkills> {
                override fun onResponse(
                    call: Call<OtherSkills>?,
                    response: Response<OtherSkills>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<OtherSkills>?, t: Throwable?) {
                    t?.let {
                        Log.e("selectedJobCat failure", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun saveAvailability(
        successHandler: (AvailabilityResponse) -> Unit,
        failureHandler: (String) -> Unit,
        request: AvailbilityRequest?
    ) {
        val map: HashMap<String, String>? = HashMap()
        map?.put("requested_by", request?.userId?.toString() ?: "")
        map?.put("workdays_details", request?.workdays_details?.toString() ?: "")
        map?.put("worker_id", request?.worker_id?.toString() ?: "")
        if (request?.availablity_id != null) {
            map?.put("availablity_id", request?.availablity_id?.toString() ?: "")
        }
        map?.put("login_time", request?.login_time ?: "")
        map?.put("logout_time", request?.logout_time ?: "")
        map?.put("active_status", request?.active_status?.toString() ?: "")

        mService.saveAvailability(map)
            ?.enqueue(object : Callback<AvailabilityResponse> {
                override fun onResponse(
                    call: Call<AvailabilityResponse>?,
                    response: Response<AvailabilityResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<AvailabilityResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("selectedJobCat failure", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun saveGlobalAvailability(
        successHandler: (SuperAvailailityResponse) -> Unit,
        failureHandler: (String) -> Unit,
        userId: Int?,
        gb: Int?,
        workerId: Int?
    ) {
        val map: HashMap<String, String>? = HashMap()
        map?.put("requested_by", userId.toString())
        map?.put("worker_id", workerId?.toString() + "")

        map?.put("globally_available", gb.toString())

        mService.saveGlobalAvailability(map)
            ?.enqueue(object : Callback<SuperAvailailityResponse> {
                override fun onResponse(
                    call: Call<SuperAvailailityResponse>?,
                    response: Response<SuperAvailailityResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuperAvailailityResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("globalAvail failure", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun getAvailability(
        successHandler: (GetAvailabilityResponse) -> Unit,
        failureHandler: (String) -> Unit,
        workerId: Int?,
        rows: Int?
    ) {
        mService.getAvailability(workerId, rows)
            ?.enqueue(object : Callback<GetAvailabilityResponse> {
                override fun onResponse(
                    call: Call<GetAvailabilityResponse>?,
                    response: Response<GetAvailabilityResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<GetAvailabilityResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("getAvailability failure", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun updateAvailabilityStatus(
        successHandler: (GetAvailabilityResponse) -> Unit,
        failureHandler: (String) -> Unit,
        availabilityId: Int?,
        userId: Int?,
        activeStatus: Int?
    ) {
        mService.updateAvailabilityStatus(availabilityId, userId, activeStatus)
            ?.enqueue(object : Callback<GetAvailabilityResponse> {
                override fun onResponse(
                    call: Call<GetAvailabilityResponse>?,
                    response: Response<GetAvailabilityResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<GetAvailabilityResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("updateAvail failure", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }

    fun deleteAvailability(
        successHandler: (SuccessResponse) -> Unit,
        failureHandler: (String) -> Unit,
        availabilityId: Int?
    ) {
        mService.deleteAvailability(availabilityId)
            ?.enqueue(object : Callback<SuccessResponse> {
                override fun onResponse(
                    call: Call<SuccessResponse>?,
                    response: Response<SuccessResponse>?
                ) {
                    response?.body()?.let {
                        successHandler(it)
                    }
                    response?.errorBody()?.let {
                        if (response.code() == 500) {
                            failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                        } else {
                            val error = ApiHelper.handleApiError(response.errorBody())
                            failureHandler(error)
                        }
                    }
                }

                override fun onFailure(call: Call<SuccessResponse>?, t: Throwable?) {
                    t?.let {
                        Log.e("deleteAvail failure", it.message.orEmpty() + "")
                        failureHandler("Oops! looks like we are having internal problems. Please try again later.")
                    }
                }
            })
    }
}