package com.okaygo.worker.ui.activity.city_state_search

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.okaygo.worker.R
import com.okaygo.worker.adapters.SearchAdapter
import com.okaygo.worker.callbacks.OnItemClick
import com.okaygo.worker.data.modal.reponse.CitySearch
import com.okaygo.worker.help.utils.Utilities
import com.okaygo.worker.ui.activity.BaseActivity
import com.openkey.guest.help.utils.Constants
import com.openkey.guest.ui.fragments.verification.SearchModel
import kotlinx.android.synthetic.main.activity_search.*
import org.greenrobot.eventbus.EventBus
import java.util.*

class SearchActivity : BaseActivity(), OnItemClick {
    private lateinit var viewModel: SearchModel

    private var mTempList: ArrayList<CitySearch>? = null
    private var mSearchAdapter: SearchAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        viewModel = ViewModelProvider(this).get(SearchModel::class.java)
        attachObservers()
        init()
        setAdapter()
        handleEdtSearch()
    }

    private fun init() {
        imgBack?.setOnClickListener { onBackPressed() }
    }

    /**
     * set adapter for searched list
     */
    private fun setAdapter() {
        if (mTempList == null) {
            mTempList = ArrayList<CitySearch>()
        }
        val linearLayoutManager = LinearLayoutManager(this)
        mSearchAdapter = SearchAdapter(
            this, mTempList,
            onCityClick
        )
        recylerSearch?.layoutManager = linearLayoutManager
        recylerSearch?.adapter = mSearchAdapter
    }

    /**
     * handle search while typing
     */
    private fun handleEdtSearch() {
        edtSearch?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {
                if (s.toString().length > 1) {
                    getCityByString(s.toString())
                } else {
                    txtNotFound?.visibility = View.GONE
                    recylerSearch?.visibility = View.GONE
                }
            }
        })
    }

    /**
     * handle api reposne
     */
    private fun attachObservers() {

        viewModel.response.observe(this, androidx.lifecycle.Observer {

            if (it.code == Constants.SUCCESS) {
                progressSearch?.visibility = View.GONE

                txtNotFound?.visibility = View.GONE
                recylerSearch?.visibility = View.VISIBLE
                if (mTempList == null) {
                    mTempList = ArrayList<CitySearch>()
                } else {
                    mTempList?.clear()
                }
                var i = 0
                while (i < it.response?.size ?: 0) {
                    it.response?.get(i)?.let { it1 -> mTempList?.add(it1) }
                    i++
                }

                Log.e("City Set", mTempList.toString() + "")
                mSearchAdapter?.notifyDataSetChanged()
            }
        })

        viewModel.apiError.observe(this, androidx.lifecycle.Observer {
            progressSearch?.visibility = View.GONE

            txtNotFound?.visibility = View.VISIBLE
            recylerSearch?.visibility = View.VISIBLE
        })
    }

    private fun getCityByString(text: String) {
        if (!Utilities.isOnline(this)) {
            Utilities.showToast(this, resources.getString(R.string.no_internet))
            return
        }
        progressSearch?.visibility = View.VISIBLE
        viewModel.getCityWithText(text)                 //city api call
    }

    /**
     * click listenr when click on any city
     */
    private val onCityClick: (CitySearch?, Int?) -> Unit = { it, pos ->
        Log.e("Pstion", pos.toString())
        Log.e("CityName", it?.district.toString() + "")
        EventBus.getDefault().post(it)
        onBackPressed()
    }
}