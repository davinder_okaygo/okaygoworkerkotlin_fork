package com.okaygo.worker.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.okaygo.worker.R
import com.okaygo.worker.help.LanguageHelper
import com.openkey.guest.help.Preferences
import com.openkey.guest.help.utils.Constants
import kotlinx.android.synthetic.main.fragment_language_selection.*

class LanguageSelectionFragment : BaseFragment(), View.OnClickListener {
    private var mLangList: ArrayList<String>? = null
    private var mSelectedLang: String? = "English"


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_language_selection, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setSpnAdapter()
        setListeners()
    }

    /**
     * set listeners for required views
     */
    private fun setListeners() {
        imgNext?.setOnClickListener(this)
        spnLang?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                mSelectedLang = parent.getItemAtPosition(position).toString()
            } // to close the onItemSelected

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }

    /**
     * set adapter for lan spinner
     */
    private fun setSpnAdapter() {
        mLangList = ArrayList()
        mLangList?.add("English")
        mLangList?.add("Hinglish")
        val spnAdapter: ArrayAdapter<String>? = activity?.let {
            ArrayAdapter<String>(
                it,
                android.R.layout.simple_spinner_dropdown_item,
                mLangList!!
            )
        }
        spnAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnLang.setAdapter(spnAdapter)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgNext -> {
                if (mSelectedLang.equals("English")) {
                    Preferences.prefs?.saveValue(Constants.SELECTED_LANG, "en")
                } else {
                    Preferences.prefs?.saveValue(Constants.SELECTED_LANG, "hi")
                }
                activity?.let { LanguageHelper.getDefaultLang(it) }
                Preferences.prefs?.saveValue(Constants.LANG_SELECTION, true)
                attachFragment(TutorialFragment(), false)
            }
        }
    }
}