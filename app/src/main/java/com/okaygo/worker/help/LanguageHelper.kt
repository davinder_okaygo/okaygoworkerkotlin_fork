package com.okaygo.worker.help

import android.content.Context
import android.util.Log
import androidx.annotation.StringDef
import com.openkey.guest.help.Preferences
import com.openkey.guest.help.utils.Constants
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.util.*

/**
 * Created by Davinder Goel
 */
object LanguageHelper {
    const val ENGLISH = "en"
    const val HINGLISH = "hi"
    fun getDefaultLang(context: Context) {
        val selectedLang: String? = Preferences.prefs?.getString(Constants.SELECTED_LANG, "en")
        Log.e("selectedLang", selectedLang + "")
        if (selectedLang?.isNotEmpty() == true) {
            setLocale(context, selectedLang)
        } else {
            setLocale(context, Locale.getDefault().displayLanguage)
        }
    }

    fun initialize(context: Context) {
        setLocale(context, ENGLISH)
    }

    fun initialize(
        context: Context,
        @LocaleDef defaultLanguage: String?
    ) {
        setLocale(context, defaultLanguage)
    }

    fun setLocale(
        context: Context,
        @LocaleDef language: String?
    ): Boolean {
        return updateResources(context, language)
    }

    fun setLocale(context: Context, languageIndex: Int): Boolean {
//        persist(context, language);
        return if (languageIndex >= LocaleDef.SUPPORTED_LOCALES.size) {
            false
        } else updateResources(
            context,
            LocaleDef.SUPPORTED_LOCALES[languageIndex]
        )
    }

    private fun updateResources(
        context: Context,
        language: String?
    ): Boolean {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val resources = context.resources
        val configuration = resources.configuration
        configuration.locale = locale
        resources.updateConfiguration(configuration, resources.displayMetrics)
        return true
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef(ENGLISH, HINGLISH)
    annotation class LocaleDef {
        companion object {
            var SUPPORTED_LOCALES =
                arrayOf(ENGLISH, HINGLISH)
        }
    }
}