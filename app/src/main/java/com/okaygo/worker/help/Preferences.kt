package com.openkey.guest.help

import android.content.Context
import com.openkey.guest.security.EncryptedPreferences

/**
 * @author Davinder Goel.
 */
class Preferences {
    companion object {
        var prefs: EncryptedPreferences? = null
        fun initPreferences(context: Context?) {
            prefs = EncryptedPreferences(context)
        }
    }
}