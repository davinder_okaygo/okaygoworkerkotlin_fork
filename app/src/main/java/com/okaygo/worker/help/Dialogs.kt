package com.openkey.guest.help

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import com.okaygo.worker.R


/**
 * @author Openkey Inc.
 */
object Dialogs {
    var dialog: Dialog? = null
    fun showAlert(activity: Activity?, msg: String?) {
        var alert: AlertDialog? = null
        val builder = activity?.let { AlertDialog.Builder(it) }
        builder?.setMessage(msg)
        builder?.setCancelable(true)
        builder?.setPositiveButton(
            "Yes",
            DialogInterface.OnClickListener { dialog, id ->

            })

        builder?.setNegativeButton(
            "No",
            DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })
        alert = builder?.create()
        alert?.show()
    }

    fun alertDialog(
        activity: Activity?,
        msg: String,
        posBtn: String,
        nagBtn: String,
        onPosClick: () -> Unit?
    ) {
        val builder1 = activity?.let { androidx.appcompat.app.AlertDialog.Builder(it) }
        builder1?.setMessage(msg)
        builder1?.setCancelable(true)

        builder1?.setPositiveButton(
            posBtn
        ) { dialog, id ->
//            okayGoFirebaseAnalytics.interview_cancel(
//                content.get(getAdapterPosition()).getJobId()
//            )
            dialog.cancel()
            onPosClick()
        }

        builder1?.setNegativeButton(
            nagBtn
        ) { dialog, id -> dialog.cancel() }

        val alert11 = builder1?.create()
        alert11?.show()
    }

    fun showAlertDialog(
        activity: Activity?,
        title: String?,
        msg: String?,
        posBtn: String?,
        nagBtn: String?,
        onPosClick: () -> Unit?,
        onNagClick: () -> Unit?
    ) {
        val myDialog = activity?.let { Dialog(it) }
        myDialog?.setContentView(R.layout.dialog_box_custom)
        myDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)

        myDialog?.setCancelable(false)

        val txtTitle = myDialog?.findViewById<AppCompatTextView>(R.id.txtTitle)
        val txtMsg = myDialog?.findViewById<AppCompatTextView>(R.id.txtMsg)
        val btnPos = myDialog?.findViewById<AppCompatTextView>(R.id.txtConfirm)
        val btnNag = myDialog?.findViewById<AppCompatTextView>(R.id.txtCancel)

        txtTitle?.text = title
        txtMsg?.text = msg
        btnPos?.text = posBtn
        btnNag?.text = nagBtn

        // if button is clicked, close the custom dialog
        btnPos?.setOnClickListener {
            myDialog.dismiss()
            onPosClick()
        }
        btnNag?.setOnClickListener {
            myDialog.dismiss()
            onNagClick()
        }
        myDialog?.show()
    }
//
//    fun GPSEnable(activity: Activity?) {
//        val dialogBuilder = activity?.let { AlertDialog.Builder(it, R.style.custom_dialog) }
//        dialogBuilder?.setMessage(activity.getString(R.string.enable_gps_message))
//            ?.setCancelable(false)
//            ?.setPositiveButton("Yes") { dialog, id
//                ->
//                activity?.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
//            }
//            ?.setNegativeButton("No") { dialog, id ->
//                dialog.cancel()
//            }
//        val alert = dialogBuilder?.create()
//        alert?.show()
//    }

    private fun hideDialog() {
        if (dialog != null && dialog?.isShowing == true) {
            try {
                dialog?.dismiss()
            } catch (e: IllegalArgumentException) {
                // Do nothing.
            } catch (e: Exception) {
                // Do nothing.
            }
        }
    }


}