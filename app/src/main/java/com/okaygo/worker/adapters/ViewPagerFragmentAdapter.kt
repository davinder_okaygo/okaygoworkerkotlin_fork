package com.okaygo.worker.adapters

import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.okaygo.worker.ui.fragments.notification.AlertFragment
import com.okaygo.worker.ui.fragments.notification.NotfFragment

class ViewPagerFragmentAdapter(@NonNull fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {
    @NonNull
    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> return AlertFragment()
            1 -> return NotfFragment()
        }
        return AlertFragment()
    }

    override fun getItemCount(): Int {
        return 2
    }
}