package com.okaygo.worker.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.okaygo.worker.R
import com.okaygo.worker.analytics.OkayGoFirebaseAnalytics
import com.okaygo.worker.data.modal.reponse.AppliedJob
import com.okaygo.worker.help.utils.Utilities
import com.openkey.guest.help.utils.Constants
import kotlinx.android.synthetic.main.item_my_job.view.*
import kotlinx.android.synthetic.main.layout_interview_detail.view.*
import kotlinx.android.synthetic.main.layout_joining_detail.view.*
import java.util.*

class MyJobAdapter(
    val context: Context?,
    val adapterFor: Int?,
    val mItemList: ArrayList<AppliedJob>?,
    val onInterviewTimeClick: (AppliedJob?, Int?) -> Unit?,
    val onJobDetailClick: (AppliedJob?, Int?) -> Unit?,
    val onJobRemoveClick: (AppliedJob?, Int?) -> Unit?,
    val onInterviewDetailClick: (AppliedJob?, Int?) -> Unit?,
    val onJoiningDetailClick: (AppliedJob?, Int?) -> Unit?,
    val onEnterOtpClick: (AppliedJob?, Int?) -> Unit?,
    val onAddressClick: (AppliedJob?, Int?) -> Unit?,
    val onYesClick: (AppliedJob?, Int?) -> Unit?,
    val onNoClick: (AppliedJob?, Int?) -> Unit?,
    val onDirectionClick: (AppliedJob?, Int?) -> Unit?,
    val onConstraintClick: (AppliedJob?, Int?) -> Unit?
) : RecyclerView.Adapter<MyJobAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(context)
        val viewHolder = inflater.inflate(R.layout.item_my_job, parent, false)
        return MyViewHolder(viewHolder)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data: AppliedJob? = mItemList?.get(position)
        holder.bind(data)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return mItemList?.size ?: 0
    }

    override fun onViewRecycled(myJobHolder: MyViewHolder) {
        super.onViewRecycled(myJobHolder)
        myJobHolder.itemView.txtEnterOtp.setVisibility(View.GONE)
        myJobHolder.itemView.txtCongratsMsg.setVisibility(View.GONE)
        myJobHolder.itemView.linearYesNo.setVisibility(View.GONE)
        myJobHolder.itemView.txtStatus.setVisibility(View.GONE)
        myJobHolder.itemView.txtRemove.setVisibility(View.GONE)
        myJobHolder.itemView.txtInterviewTime.setVisibility(View.GONE)
        myJobHolder.itemView.txtInterviewDetail.setVisibility(View.GONE)
        myJobHolder.itemView.txtJoiningDetailTitle.setVisibility(View.GONE)
        myJobHolder.itemView.layoutInterviewDetail.setVisibility(View.GONE)
        myJobHolder.itemView.layoutJoiningDetail.setVisibility(View.GONE)
        myJobHolder.itemView.txtDirection.setVisibility(View.GONE)
        myJobHolder.itemView.txtWaitingForSlots.setVisibility(View.GONE)
        myJobHolder.itemView.txtSalary.setVisibility(View.GONE)
        myJobHolder.CURRENT_STATUS = 0
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var CURRENT_STATUS = 0

        fun bind(item: AppliedJob?) {

            if (item?.jobTitle?.isEmpty() == true) {
                itemView.txtJobName?.text = item.jobType
            } else {
                itemView.txtJobName?.text = item?.jobTitle
            }
            if (adapterFor == 1) {
                setDataForApplied(item, itemView)
            } else if (adapterFor == 2) {
                setDataForUpcoming(item, itemView)
            } else if (adapterFor == 3) {
                setDataForPastJob(item, itemView)
            }

            setJobType(item?.workType, itemView)
            if (item?.interview_mode == 2 && adapterFor == 1) {
                val sDate: String? = Utilities.getDateFromMili(item?.from_date ?: 0)
                val eDate: String? = Utilities.getDateFromMili(item.to_date ?: 0)
                if (sDate == eDate) {
                    itemView.txtDate.setText(sDate)
                } else {
                    itemView.txtDate.setText("$sDate - $eDate")
                }
                itemView.txtInterviewDetail.setVisibility(View.VISIBLE)
                itemView.imgTime.setVisibility(View.GONE)
                itemView.txtTimeTitle.setVisibility(View.GONE)
                itemView.txtTime.setVisibility(View.GONE)
                itemView.txtAddressTitle.setVisibility(View.GONE)
                itemView.imgMap.setVisibility(View.GONE)
                itemView.txtAddress.setVisibility(View.GONE)
            } else if (item?.interview_mode == 3 && adapterFor == 1) {
                itemView.txtInterviewDetail.setVisibility(View.VISIBLE)
                itemView.txtAddressTitle.setVisibility(View.VISIBLE)
                itemView.txtAddress.setVisibility(View.VISIBLE)
                if (item?.interviewStartTime?.contains(" ") == true) {
                    val interviewTime: List<String> =
                        item?.interviewStartTime?.split(" ")
                    itemView.txtDate.setText(
                        Utilities.convertStringDateToDesireFormat(interviewTime[0], "MMM dd")
                    )
                    if (item?.interviewEndTime?.contains(" ") == true) {
                        val interviewETime: List<String> =
                            item?.interviewEndTime?.split(" ")
                        itemView.txtTime.setText(
                            Utilities.convertTimeToAmPm(interviewTime[1])
                                .toString() + " - " + Utilities.convertTimeToAmPm(interviewETime[1])
                        )
                    }
                }
            } else {
                itemView.layoutInterviewDetail.setVisibility(View.GONE)
                itemView.txtInterviewDetail.setVisibility(View.GONE)
            }
            if (item?.isJobOffered == 1) {
                itemView.layoutInterviewDetail.setVisibility(View.GONE)
                itemView.txtInterviewDetail.setVisibility(View.GONE)
            }

            if (item?.contactCandidateName?.isEmpty() == false) {
                itemView.txtContactPerson.setVisibility(View.VISIBLE)
                itemView.imgPersob.setVisibility(View.VISIBLE)
                itemView.txtContactPersonTitle.setVisibility(View.VISIBLE)
                itemView.txtContactPersonTitleJ.setVisibility(View.VISIBLE)
                itemView.txtContactPersonJoining.setVisibility(View.VISIBLE)
                itemView.imgPersonJoining.setVisibility(View.VISIBLE)
                itemView.txtContactPerson.setText(item?.contactCandidateName)
                itemView.txtContactPersonJoining.setText(item?.contactCandidateName)
            } else {
                itemView.txtContactPerson.setVisibility(View.GONE)
                itemView.imgPersob.setVisibility(View.GONE)
                itemView.txtContactPersonTitle.setVisibility(View.GONE)
                itemView.txtContactPersonTitleJ.setVisibility(View.GONE)
                itemView.txtContactPersonJoining.setVisibility(View.GONE)
                itemView.imgPersonJoining.setVisibility(View.GONE)

            }

            itemView.txtAddress.setText(item?.address ?: item?.city)
            itemView.txtInterviewTime?.setOnClickListener {
                onInterviewTimeClick(
                    item,
                    absoluteAdapterPosition
                )
            }
            itemView.txtJobDetail?.setOnClickListener {
                OkayGoFirebaseAnalytics.job_detail_view(
                    item?.workType,
                    item?.jobId?.toString(),
                    item?.jobType?.toString(),
                    "",
                    "myjobs"
                )

                onJobDetailClick(item, absoluteAdapterPosition)
            }
            itemView.txtRemove?.setOnClickListener {
                onJobRemoveClick(
                    item,
                    absoluteAdapterPosition
                )
            }
            itemView.txtInterviewDetail?.setOnClickListener {
                onInterviewDetailClick(
                    item,
                    absoluteAdapterPosition
                )
            }
            itemView.txtJoiningDetail?.setOnClickListener {
                onJoiningDetailClick(
                    item,
                    absoluteAdapterPosition
                )
            }

            itemView.txtEnterOtp?.setOnClickListener {
                onEnterOtpClick(
                    item,
                    absoluteAdapterPosition
                )
            }
            itemView.constraintRoot?.setOnClickListener { onConstraintClick(item, CURRENT_STATUS) }
            itemView.txtDirection?.setOnClickListener {
                onDirectionClick(
                    item,
                    absoluteAdapterPosition
                )
            }
            itemView.txtAddress?.setOnClickListener {
                onAddressClick(
                    item,
                    absoluteAdapterPosition
                )
            }
            itemView.txtYes?.setOnClickListener { onYesClick(item, absoluteAdapterPosition) }
            itemView.txtNo?.setOnClickListener {
                OkayGoFirebaseAnalytics.interview_cancel(
                    item?.jobId?.toString()
                )
                onNoClick(item, absoluteAdapterPosition)
            }
        }


        private fun setJobType(
            jobType: String?,
            itemView: View?
        ) {
            itemView?.txtJobType?.setText(jobType)
            var colorCode = context!!.resources.getColor(R.color.kprogresshud_grey_color)
            colorCode = when (jobType?.toUpperCase()) {
                "ON DEMAND" -> context.resources.getColor(R.color.on_demand_color)
                "PART TIME" -> context.resources.getColor(R.color.part_time_color_new)
                "FULL TIME" -> context.resources.getColor(R.color.full_time_color)
                else -> context.resources.getColor(R.color.full_time_color)
            }
            itemView?.txtJobType?.setBackgroundColor(colorCode)
        }

        private fun setDataForPastJob(data: AppliedJob?, itemView: View) {
            if (adapterFor == 3) {
                if (data?.workType?.equals("On Demand") == true) {
                    itemView.txtSalary.setText(
                        "₹ " + Math.round(
                            data?.amount ?: 0.0
                        ) + "/" + data?.amountPer
                    )
                } else {
                    itemView.txtSalary.setText(
                        "₹ " + Math.round(
                            data?.job_offered_amount?.toDouble() ?: 0.0
                        ) + "/" + data?.amountPer
                    )
                }
                itemView.txtJobType.setText(data?.jobType)
                itemView.txtCompany.setText(data?.brandName ?: data?.company)


                itemView.txtJobType.setText(data?.workType ?: "")
                if (data?.joiningStatus?.isEmpty() == false) {
                    itemView.txtStatus.setVisibility(View.VISIBLE)
                    itemView.txtStatus.setText(data?.joiningStatus?.replace("_", " "))
                } else {
                    itemView.txtStatus.setVisibility(View.GONE)
                }
            }
        }

        private fun setDataForApplied(data: AppliedJob?, itemView: View) {
            if (data?.workType.equals("On Demand")) {
                if (data?.isAccepted == 1) {
                    itemView.txtStatus?.setVisibility(View.VISIBLE)
                    if (data.isRejected == 1 || data.isConfirmed == 2) {
                        itemView.txtStatus.setText("REJECTED")
                        CURRENT_STATUS = Constants.OD_REJECTED
                        itemView.txtRemove.setVisibility(View.VISIBLE)
                        itemView.txtDirection.setVisibility(View.GONE)
                    } else {
                        itemView.txtStatus.setText("JOB APPLIED")

                        if (data?.employee_hired == data.employee_required) {
                            itemView.txtStatus.setText("Application Rejected")
                            itemView.txtRemove.setVisibility(View.VISIBLE)
                            itemView.txtDirection.setVisibility(View.GONE)
                        }
                        CURRENT_STATUS = Constants.OD_APPLIED
                    }
                }
            } else if (data?.workType.equals("Part Time") || data?.workType.equals("Full Time")) {
                Log.e(
                    "Days diff",
                    Utilities.getDaysDiff(data?.startDate).toString() + ""
                )
                if (data?.isJobOffered != null) {
                    if (data.isJobOffered == 0) {
                        if (data.resultAwaited == 0) {
                            if (data.interviewSlot == 0) {
                                itemView.txtStatus.setVisibility(View.VISIBLE)
                                if (data.isConfirmed == 0) {
                                    CURRENT_STATUS = Constants.PT_FT_APPLIED
                                    itemView.txtStatus.setText("JOB APPLIED")
                                } else if (data.isConfirmed == 1) {
                                    if (data.interviewStartTime?.isEmpty() == true && data.interviewEndTime?.isEmpty() == true) {
                                        CURRENT_STATUS = Constants.PT_FT_SHORTLISTED
                                        itemView.txtStatus.setText("Application Approved")
                                    } else {
                                        if (data.interviewStatus == 0) {
                                            CURRENT_STATUS = Constants.PT_FT_INTERVIEW_CANCELLED
                                            itemView.txtStatus.setText("INTERVIEW CANCELLED")
                                        } else {
                                            CURRENT_STATUS = Constants.PT_FT_SELECT_A_SLOT
                                            itemView.txtStatus.setText("Application Approved")
                                            itemView.txtInterviewTime.setVisibility(View.VISIBLE)
                                        }
                                    }
                                } else if (data.isConfirmed == 2) {
                                    CURRENT_STATUS = Constants.PT_FT_REJECTED
                                    itemView.txtStatus.setVisibility(View.VISIBLE)
                                    itemView.txtStatus.setText("Application Rejected")
                                    itemView.txtRemove.setVisibility(View.VISIBLE)
                                    itemView.txtDirection.setVisibility(View.GONE)
                                }
                            } else {
                                if (data.isRejected == 1) {
                                    itemView.txtStatus.setVisibility(View.VISIBLE)
                                    CURRENT_STATUS = Constants.PT_FT_REJECTED
                                    itemView.txtStatus.setText("REJECTED")
                                    itemView.txtRemove.setVisibility(View.VISIBLE)
                                    itemView.txtDirection.setVisibility(View.GONE)
                                } else {
                                    if (data.interviewStatus == 0) {
                                        itemView.txtStatus.setVisibility(View.VISIBLE)
                                        CURRENT_STATUS = Constants.PT_FT_INTERVIEW_CANCELLED
                                        itemView.txtStatus.setText("INTERVIEW CANCELLED")
                                    } else {
                                        itemView.txtStatus.setVisibility(View.GONE)
                                        if (data.pt_ft_is_no_show != null && data.pt_ft_is_no_show == 1) {
                                            CURRENT_STATUS = Constants.PT_FT_REJECTED
                                            itemView.txtInterviewDetail.setVisibility(View.GONE)
                                            itemView.layoutInterviewDetail.setVisibility(View.GONE)
                                        } else {
                                            CURRENT_STATUS = Constants.PT_FT_SLOT_SELECTED
                                            if (data.feedbackStatus?.isNotEmpty() == true) {
                                                itemView.txtStatus.setVisibility(View.VISIBLE)
                                                itemView.txtStatus.setText(
                                                    data.feedbackStatus.replace("_", " ")
                                                )
                                                if (data.feedbackStatus.toUpperCase()
                                                        .replace("_", " ")
                                                        .equals("REJECTED") || data.feedbackStatus
                                                        .toUpperCase().replace("_", " ")
                                                        .equals("CANDIDATE DROPPED") || data.feedbackStatus
                                                        .toUpperCase().replace("_", " ")
                                                        .equals("ROUNDS PENDING")
                                                ) {
                                                    itemView.txtRemove.setVisibility(View.VISIBLE)
                                                    itemView.txtDirection.setVisibility(
                                                        View.GONE
                                                    )
                                                }
                                            } else {
                                                itemView.txtStatus.setVisibility(View.GONE)
                                            }
                                            itemView.layoutInterviewDetail.setVisibility(
                                                View.VISIBLE
                                            )
                                            if (data.interviewProcess?.isEmpty() == false || data.instructionsCandidate?.isEmpty() == false) {
                                                itemView.txtInterviewDetail.setVisibility(View.VISIBLE)
                                            }
                                            if (data.is_interview_otp_verified != null && data.is_interview_otp_verified == 0
                                            ) {
                                                itemView.txtEnterOtp.setVisibility(View.VISIBLE)
                                            } else {
                                                itemView.txtEnterOtp.setVisibility(View.GONE)
                                            }
                                        }
                                    }
                                }
                            }
                        } else if (data.resultAwaited == 1) {
                            itemView.txtStatus.setVisibility(View.VISIBLE)
                            CURRENT_STATUS = Constants.PT_FT_INTERVIEW_DONE_RESULT_AWAITED
                            if (data.feedbackStatus?.isEmpty() == false) {
                                itemView.txtStatus.setText(
                                    data.feedbackStatus.replace("_", " ")
                                )
                                if (data.feedbackStatus.toUpperCase().replace("_", " ")
                                        .equals("REJECTED") || data.feedbackStatus
                                        .toUpperCase().replace("_", " ")
                                        .equals("CANDIDATE DROPPED") || data.feedbackStatus
                                        .toUpperCase().replace("_", " ")
                                        .equals("ROUNDS PENDING")
                                ) {
                                    itemView.txtRemove.setVisibility(View.VISIBLE)
                                    itemView.txtDirection.setVisibility(View.GONE)
                                }
                            } else {
                                itemView.txtStatus.setText("RESULT AWAITED")
                            }
                        }
                    } else if (data.isJobOffered == 1) {
                        if (data.isOfferAccepted == 0) {
                            CURRENT_STATUS = Constants.PT_FT_JOB_OFFERED
                            itemView.layoutJoiningDetail.setVisibility(View.VISIBLE)

                            itemView.txtCongratsMsg.setVisibility(View.VISIBLE)
                            itemView.txtSalary.setVisibility(View.GONE)
                            itemView.linearYesNo.setVisibility(View.VISIBLE)
                            itemView.txtStatus.setVisibility(View.GONE)
                            itemView.txtJoiningDate.setText(
                                Utilities.getFormatedDate(data.joining_date?.split(" ")?.get(0))
                                    .toString() + ""
                            )
                            itemView.txtSelectedSalary.setText(
                                "\u20B9 " + data.job_offered_amount
                                    .toString() + "/" + data.amountPer
                            )
                        } else if (data.isOfferAccepted == 2) {
                            itemView.txtStatus.setVisibility(View.VISIBLE)
                            itemView.layoutJoiningDetail.setVisibility(View.VISIBLE)

                            itemView.txtStatus.setText("OFFER REJECTED")
                            CURRENT_STATUS = Constants.PT_FT_REJECTED
                            itemView.txtJoiningDate.setText(
                                Utilities.getFormatedDate(data.joining_date?.split(" ")?.get(0))
                                    .toString() + ""
                            )
                            itemView.txtSelectedSalary.setText(
                                "\u20B9 " + data.job_offered_amount + "/" + data.amountPer
                            )
                            itemView.txtSalary.setVisibility(View.GONE)
                            itemView.txtRemove.setVisibility(View.VISIBLE)
                            itemView.txtDirection.setVisibility(View.GONE)

                        }
                    }
                } else {
                    if (data?.resultAwaited == 0) {
                        if (data.interviewSlot != null) {
                            if (data.interviewSlot == 0) {
                                if (data.isConfirmed == 0) {
                                    CURRENT_STATUS = Constants.PT_FT_APPLIED
                                    itemView.txtStatus.setVisibility(View.VISIBLE)
                                    itemView.txtStatus.setText("JOB APPLIED")
                                    /*itemView.cancel.setVisibility(View.VISIBLE);*/
                                } else if (data.isConfirmed == 1) {
                                    if (data.interviewStatus == 0) {
                                        CURRENT_STATUS = Constants.PT_FT_INTERVIEW_CANCELLED
                                        itemView.txtStatus.setVisibility(View.VISIBLE)
                                        itemView.txtStatus.setText("INTERVIEW CANCELLED")
                                    } else {
                                        CURRENT_STATUS = Constants.PT_FT_SELECT_A_SLOT
                                        itemView.txtStatus.setVisibility(View.VISIBLE)
                                        itemView.txtStatus.setText("Application Approved")
                                        itemView.txtInterviewTime.setVisibility(View.VISIBLE)
                                    }
                                }
                            } else {
                                if (data.interviewStatus == 0) {
                                    CURRENT_STATUS = Constants.PT_FT_INTERVIEW_CANCELLED
                                    itemView.txtStatus.setVisibility(View.VISIBLE)
                                    itemView.txtStatus.setText("INTERVIEW CANCELLED")
                                } else {
                                    itemView.txtStatus.setVisibility(View.GONE)
                                    if (data.pt_ft_is_no_show != null && data.pt_ft_is_no_show == 1) {
                                        CURRENT_STATUS = Constants.PT_FT_REJECTED
                                        itemView.txtInterviewDetail.setVisibility(View.GONE)
                                        itemView.layoutInterviewDetail.setVisibility(View.GONE)
                                    } else {
                                        CURRENT_STATUS = Constants.PT_FT_SLOT_SELECTED
                                        if (data.feedbackStatus?.isEmpty() == false) {
                                            itemView.txtStatus.setVisibility(View.VISIBLE)
                                            itemView.txtStatus.setText(
                                                data.feedbackStatus?.replace("_", " ")
                                            )
                                        } else {
                                            itemView.txtStatus.setVisibility(View.GONE)
                                        }
                                        itemView.layoutInterviewDetail.setVisibility(View.VISIBLE)
                                        if (data?.interviewProcess?.isEmpty() == false || data?.instructionsCandidate?.isEmpty() == false) {
                                            itemView.txtInterviewDetail.setVisibility(View.VISIBLE)
                                        }
                                        if (data.is_od_otp_verified != null && data.is_od_otp_verified == 0) {
                                            itemView.txtEnterOtp.setVisibility(View.VISIBLE)
                                        } else {
                                            itemView.txtEnterOtp.setVisibility(View.GONE)
                                        }
                                    }
                                }
                            }
                        } else {
                            itemView.txtStatus.setVisibility(View.VISIBLE)
                            if (data.isConfirmed == 0) {
                                CURRENT_STATUS = Constants.PT_FT_APPLIED
                                itemView.txtStatus.setText("JOB APPLIED")

//                                    itemView.cancel.setVisibility(View.VISIBLE);
                            } else if (data.isConfirmed == 1) {
                                if (data.interviewStartTime?.isEmpty() == true && data.interviewEndTime?.isEmpty() == true) {
                                    CURRENT_STATUS = Constants.PT_FT_SHORTLISTED
                                    itemView.txtStatus.setText("Application Approved")
                                    //                                    itemView.txtWaitingForSlots.setVisibility(View.VISIBLE);
                                } else {
                                    if (data.interviewStatus == 0) {
                                        CURRENT_STATUS = Constants.PT_FT_INTERVIEW_CANCELLED
                                        itemView.txtStatus.setText("INTERVIEW CANCELLED")
                                    } else {
                                        CURRENT_STATUS = Constants.PT_FT_SELECT_A_SLOT
                                        itemView.txtStatus.setText("Application Approved")
                                        itemView.txtInterviewTime.setVisibility(View.VISIBLE)
                                    }
                                }
                            } else if (data.isConfirmed == 2) {
                                CURRENT_STATUS = Constants.PT_FT_REJECTED
                                itemView.txtStatus.setText("Application Rejected")
                                itemView.txtRemove.setVisibility(View.VISIBLE)
                                itemView.txtDirection.setVisibility(View.GONE)

//                                    itemView.cancel.setVisibility(View.VISIBLE);
                            }
                        }
                    } else if (data?.resultAwaited == 1) {
                        itemView.txtStatus.setVisibility(View.VISIBLE)
                        CURRENT_STATUS = Constants.PT_FT_INTERVIEW_DONE_RESULT_AWAITED
                        if (data.feedbackStatus?.isEmpty() == false) {
                            itemView.txtStatus.setText(
                                data?.feedbackStatus?.replace("_", " ")
                            )
                            if (data.feedbackStatus.toUpperCase().replace("_", " ")
                                    .equals("REJECTED") || data.feedbackStatus.toUpperCase()
                                    .replace("_", " ")
                                    .equals("CANDIDATE DROPPED") || data.feedbackStatus.replace(
                                    "_",
                                    " "
                                ).toUpperCase().equals("ROUNDS PENDING")
                            ) {
                                itemView.txtRemove.setVisibility(View.VISIBLE)
                                itemView.txtDirection.setVisibility(View.GONE)
                            }
                        } else {
                            itemView.txtStatus.setText("RESULT AWAITED")
                        }
                    }
                }
            }
            if (data?.brandName != null && data?.brandName?.length ?: 0 > 0) {
                itemView.txtCompany.setText(data.brandName)
            } else {
                itemView.txtCompany.setText(data?.company)
            }
            itemView.txtSalary.setText(
                "₹ " + Math.round(
                    data?.amount ?: 0.0
                ) + "/" + data?.amountPer
            )
            itemView.txtJobType.setText(data?.workType)
            itemView.txtInterviewTime?.visibility = View.GONE
        }

        private fun setDataForUpcoming(data: AppliedJob?, itemView: View) {
            if (adapterFor == 2) {
                itemView.txtDirection.setVisibility(View.GONE)
                if (data?.joiningStatus?.isEmpty() == false) {
                    itemView.txtStatus.setVisibility(View.VISIBLE)
                    itemView.txtStatus.setText(
                        data?.joiningStatus?.replace("_", " ")
                    )
                    if (data?.joiningStatus?.toUpperCase().replace("_", " ")
                            .equals("NOT JOINING")
                    ) {
                        itemView.txtRemove.setVisibility(View.VISIBLE)
                        itemView.txtDirection.setVisibility(View.GONE)
                    }
                }
                if (adapterFor == 2 && data?.workType?.equals("On Demand") == true) {
                    if (data?.od_is_no_show == 1) {
                        itemView.txtSalary.setText(
                            "₹ " + Math.round(
                                data.amount ?: 0.0
                            ) + "/" + data?.amountPer
                        )
                    } else {
                        itemView.txtEnterOtp.setVisibility(View.VISIBLE)
                        itemView.txtStatus.setVisibility(View.GONE)
                        itemView.txtSalary.setText(
                            "₹ " + Math.round(
                                data?.amount ?: 0.0
                            ) + "/" + data?.amountPer
                        )
                    }
                } else {
                    itemView.layoutJoiningDetail?.setVisibility(View.VISIBLE)
                    itemView.txtJoiningDate.setText(
                        Utilities.getFormatedDate(
                            (data?.joining_date)?.split(" ")?.get(0)
                        )
                    )

                    itemView.txtSelectedSalary.setText(
                        "₹ " + Math.round(
                            data?.job_offered_amount?.toDouble() ?: 0.0
                        ) + "/" + data?.amountPer
                    )
                }
                itemView.txtJobType.setText(data?.jobType)
                itemView.txtCompany.setText(data?.brandName ?: data?.company)

                if (itemView.txtRemove.getVisibility() == View.VISIBLE) {
                    itemView.txtDirection.setVisibility(View.GONE)
                } else {
                    itemView.txtDirection.setVisibility(View.VISIBLE)
                }
            }
        }
    }
}