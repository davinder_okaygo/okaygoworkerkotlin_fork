package com.okaygo.worker.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.okaygo.worker.R
import com.okaygo.worker.adapters.PaymentAdapter.PaymentHolder
import com.okaygo.worker.data.modal.reponse.PaymentResponse
import com.okaygo.worker.help.utils.Utilities
import kotlinx.android.synthetic.main.item_payment.view.*

class PaymentAdapter(var context: Context?, val mList: ArrayList<PaymentResponse>?) :
    RecyclerView.Adapter<PaymentHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentHolder {
        val inflater = LayoutInflater.from(context)
        val myownholder: View = inflater.inflate(R.layout.item_payment, parent, false)
        return PaymentHolder(myownholder)
    }

    override fun onBindViewHolder(holder: PaymentHolder, position: Int) {
        holder.bind(mList?.get(position))
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class PaymentHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: PaymentResponse?) {
            itemView.txtBrandName?.text = item?.brandName ?: item?.companyName
            itemView.txtDate?.text = "${Utilities.getFormatedDate(
                item?.insertedOn?.split(" ")?.get(0)
            )} ${Utilities.convertTimeInAmPm(item?.insertedOn?.split(" ")?.get(1))}"

            itemView.txtAmount?.text = "₹ ${Math.round(item?.totalAmount?.toFloat() ?: 0F)}"

            if (item?.isWorkerPaid.equals("1")) {
                itemView.imgStatus?.setImageResource(R.drawable.verified)
            } else {
                itemView.imgStatus?.setImageResource(R.drawable.ic_info_yellow_24dp)
            }

            if (item?.companyLogo != null) {
                Utilities.setImageByGlideRounded(
                    context as FragmentActivity,
                    item.companyLogo,
                    itemView.imgCompany
                )
            } else {
                Utilities.setImageByGlideRounded(
                    context as FragmentActivity,
                    context?.resources?.getDrawable(R.drawable.ic_group_431),
                    itemView.imgCompany
                )
            }
        }
    }
}