package com.okaygo.worker.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager.widget.PagerAdapter
import com.okaygo.worker.R


class MyCustomPagerAdapter(
    var context: Context?, var images: ArrayList<String>?,
    private val onYesClick: (Int?) -> Unit?
) :
    PagerAdapter() {
    var layoutInflater: LayoutInflater
    override fun getCount(): Int {
        return images?.size ?: 0
    }

    override fun isViewFromObject(
        view: View,
        obj: Any
    ): Boolean {
        return view === obj as ConstraintLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView: View = layoutInflater.inflate(R.layout.item_questionare, container, false)

        val textView = itemView.findViewById<AppCompatTextView>(R.id.txtQuestion);
        val txtYes = itemView.findViewById<AppCompatTextView>(R.id.txtYes);
        textView?.text = images?.get(position)
//        imageView.setImageResource(images[position]);
//
//        container.addView(itemView);
//
//        //listening to image click
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(context, "you clicked image " + (position + 1), Toast.LENGTH_LONG).show();
//            }
//        });
//
        txtYes?.setOnClickListener {
            onYesClick(position)
        }
        return itemView
    }

    override fun destroyItem(
        container: ViewGroup,
        position: Int,
        obj: Any
    ) {
        container.removeView(obj as ConstraintLayout)
    }

    init {
        layoutInflater =
            context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}