package com.okaygo.worker.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.BottomNavScreenChange
import com.okaygo.worker.data.modal.reponse.Notifications
import com.okaygo.worker.help.utils.Utilities
import kotlinx.android.synthetic.main.item_notification.view.*
import org.greenrobot.eventbus.EventBus
import java.util.*

class NotificationAdapter(
    private val mActivity: Activity,
    private val isForAlert: Boolean,
    private val mList: ArrayList<Notifications>?,
    private val OnNotificationClick: (Notifications?, Int?) -> Unit?,
    private val OnPosBtnClick: (Notifications?, Int?) -> Unit?,
    private val OnNagBtnClick: (Notifications?, Int?) -> Unit?

) : RecyclerView.Adapter<NotificationAdapter.MyJobHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyJobHolder {
        val inflater = LayoutInflater.from(mActivity)
        val viewHolder: View = inflater.inflate(R.layout.item_notification, parent, false)
        return MyJobHolder(viewHolder)
    }

    override fun onBindViewHolder(holder: MyJobHolder, position: Int) {
        val data: Notifications? = mList?.get(position)
        holder.bind(data)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class MyJobHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var clickFor = 0
        fun bind(item: Notifications?) {
            itemView.txtTitle?.text = item?.title
            itemView.txtBody?.text = item?.message
            itemView.txtDateTime?.text = "${Utilities.getFormatedDate(
                item?.insertedOn?.split(" ")?.get(0)
            )} ${Utilities.convertTimeInAmPm(item?.insertedOn?.split(" ")?.get(1))}"

            if (isForAlert) {
                when (item?.eventType) {
                    171,
                    1760,
                    1929 -> {
                        itemView.txtAccept?.setText("Update")
                        itemView.txtReject?.setVisibility(View.GONE)
                        clickFor = 1
                    }
                    172,
                    1761,
                    1930 -> {
                        itemView.txtAccept?.setText("Update")
                        itemView.txtReject?.setVisibility(View.GONE)
                        clickFor = 2
                    }
                    173,
                    1762,
                    1931 -> {
                        itemView.txtAccept?.setText("Update time availability")
                        itemView.txtReject?.setVisibility(View.GONE)
                        clickFor = 3
                    }
                    174,
                    1763,
                    1932 -> {
                        clickFor = 4
                    }
                    175,
                    1764,
                    1933 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_new_job)
                        itemView.txtAccept?.setText("Apply")
                        itemView.txtReject?.setText("View details")
                        itemView.txtBody?.setOnClickListener {
                            val bottomScreen = BottomNavScreenChange(1)
                            EventBus.getDefault().post(bottomScreen)
                            mActivity?.onBackPressed()
                        }
                        clickFor = 17
                    }
                    176,
                    1765,
                    1934 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_notification)
                        itemView.txtAccept?.setText("Apply")
                        itemView.txtReject?.setText("View details")
                        clickFor = 5
                    }
                    179,
                    1768,
                    1937 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_notification)
                        clickFor = 6

                    }
                    181,
                    1770,
                    1939 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_od_jobs)
                        clickFor = 7

                    }
                    183,
                    1772,
                    1941 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_od_jobs)
                        clickFor = 8
                        itemView.txtAccept?.setText("OK")
                        itemView.txtReject?.setVisibility(View.GONE)
                    }
                    184,
                    1773,
                    1942 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_od_jobs)
                        clickFor = 9
                        itemView.txtAccept?.setText("OK")
                        itemView.txtReject?.setVisibility(View.GONE)
                    }
                    185,
                    1774,
                    1943 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_interview)
                        clickFor = 10
                        itemView.txtReject?.setVisibility(View.GONE)
                        itemView.txtAccept?.setVisibility(View.GONE)
                        itemView.txtBody?.setOnClickListener {
                            val bottomScreen = BottomNavScreenChange(3)
                            EventBus.getDefault().post(bottomScreen)
                            mActivity.onBackPressed()
                        }
                    }
                    186,
                    1775,
                    1944 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_interview)
                        clickFor = 11
                    }
                    187,
                    1776,
                    1945 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_interview)
                        clickFor = 12
                        itemView.txtAccept?.setText("OK")
                        itemView.txtReject?.setVisibility(View.GONE)
                    }
                    190,
                    1778,
                    1947 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_interview)
                        clickFor = 13
                        itemView.txtAccept?.setText("OK")
                        itemView.txtReject?.setVisibility(View.GONE)
                    }
                    322,
                    1784,
                    1953 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_interview)
                        clickFor = 14
                        itemView.txtAccept?.setText("Enter OTP")
                        itemView.txtReject?.setVisibility(View.GONE)
                    }
                    323,
                    1785,
                    1954 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_interview)
                        clickFor = 15
                        itemView.txtAccept?.setText("Enter OTP")
                        itemView.txtReject?.setVisibility(View.GONE)
                    }

                    2160,
                    2161,
                    1839,
                    1840 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_interview)
                        clickFor = 18
                        itemView.txtAccept?.setText("Claim")
                        itemView.txtReject?.setVisibility(View.GONE)
                    }
                    else -> {
                        clickFor = 16
                        itemView.txtReject?.setVisibility(View.GONE)
                        if (item?.title?.contains("Matching") == true) {
                            itemView.imgIcon?.setImageResource(R.drawable.ic_pt_ft_jobs)
                            itemView.txtAccept?.setText("Apply")
                        } else {
                            itemView.imgIcon?.setImageResource(R.drawable.ic_interview)
                            itemView.txtAccept?.setVisibility(View.GONE)
                            itemView.txtReject?.setVisibility(View.GONE)

                        }
                    }
                }
            } else {
                when (item?.eventType) {
                    171,
                    1760,
                    1929 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_profile)
                        itemView.txtBody?.setOnClickListener { mActivity.onBackPressed() }
                    }
                    174,
                    1763,
                    1932 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_profile)
                    }

                    177,
                    1766,
                    1935, 178, 1767, 1936 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_interview)
                        itemView.txtBody?.setOnClickListener { mActivity.onBackPressed() }
                    }
                    180,
                    1769,
                    1938 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_od_jobs)
                        itemView.txtBody?.setOnClickListener { mActivity.onBackPressed() }
                    }
                    182, 1771, 1940 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_od_jobs)
                        itemView.txtBody?.setOnClickListener { mActivity.onBackPressed() }
                    }
                    189, 1777, 1946 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_pt_ft_jobs)
                        itemView.txtBody?.setOnClickListener { mActivity.onBackPressed() }
                    }
                    195, 1783, 1952 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_od_jobs)
                        itemView.txtBody?.setOnClickListener { mActivity.onBackPressed() }
                    }

                    322,
                    1784,
                    1953 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_interview)
                        itemView.txtBody?.setOnClickListener { mActivity.onBackPressed() }

                    }
                    323,
                    1785,
                    1954 -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_od_jobs)
                        itemView.txtBody?.setOnClickListener { mActivity.onBackPressed() }
                    }
                    else -> {
                        itemView.imgIcon?.setImageResource(R.drawable.ic_notification)
                    }

                }
                itemView.txtAccept?.setVisibility(View.GONE)
                itemView.txtReject?.setVisibility(View.GONE)
                itemView.constraintRoot?.setOnClickListener {
                    OnNotificationClick(
                        item,
                        adapterPosition
                    )
                }

            }
            itemView.txtAccept?.setOnClickListener { OnPosBtnClick(item, clickFor) }
            itemView.txtReject?.setOnClickListener { OnNagBtnClick(item, clickFor) }

        }
    }

}