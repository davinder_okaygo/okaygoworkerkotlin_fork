package com.okaygo.worker.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.okaygo.worker.R
import com.okaygo.worker.adapters.FaqAdapter.FaqHolder
import com.okaygo.worker.data.modal.reponse.FAQ
import kotlinx.android.synthetic.main.item_faq.view.*

class FaqAdapter(
    private var mContext: Context?,
    private var mList: ArrayList<FAQ>?,
    private val onFaqClick: (FAQ?, Int?) -> Unit?
) : RecyclerView.Adapter<FaqHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FaqHolder {
        val inflater = LayoutInflater.from(mContext)
        val myownholder: View = inflater.inflate(R.layout.item_faq, parent, false)
        return FaqHolder(myownholder)
    }

    override fun onBindViewHolder(holder: FaqHolder, position: Int) {
        holder.bind(mList?.get(position))
    }

    override fun getItemCount(): Int {
        return mList?.size ?: 0
    }

    inner class FaqHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(item: FAQ?) {
            itemView.txtTitleFaq?.text = item?.typeValue
            itemView.constraintRoot?.setOnClickListener {
                onFaqClick(mList?.get(adapterPosition), adapterPosition)
            }
        }
    }
}