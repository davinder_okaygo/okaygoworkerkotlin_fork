package com.okaygo.worker.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.okaygo.worker.R
import com.okaygo.worker.data.modal.reponse.ReferralData
import com.okaygo.worker.data.modal.reponse.ReferralWorkerData
import kotlinx.android.synthetic.main.item_referral_header.view.*

class ReferralHeaderAdtapter(
    val context: Context?, val mHeaderList: ArrayList<ReferralData>?,
    private val onExpandClick: (ReferralData?, Int?) -> Unit?,
    private val onRaiseEnqueryClick: (ReferralWorkerData?, Int?) -> Unit?,
    private val onClaimClick: (ReferralWorkerData?, Int?) -> Unit?

) : RecyclerView.Adapter<ReferralHeaderAdtapter.MyViewHolder>() {

    private var mSelectedItem = 0
    private var view: View? = null

    init {
        mSelectedItem = -1
    }

    private var mReferralAdapter: ReferralAdtapter? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_referral_header, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return mHeaderList?.size ?: 0
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(mHeaderList?.get(position))
    }

    fun setChildAdapter(childList: ArrayList<ReferralWorkerData>?) {

        val linearLayoutManager = LinearLayoutManager(context)
        mReferralAdapter = ReferralAdtapter(context, childList, onRaiseEnqueryClick, onClaimClick)
        view?.reclerChild?.layoutManager = linearLayoutManager
        view?.reclerChild?.adapter = mReferralAdapter
    }


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: ReferralData?) {
            view = itemView
            itemView?.imgDropDown?.setImageResource(R.drawable.ic_drop_down)
            itemView?.reclerChild?.visibility = View.GONE
            itemView.txtJobCategory?.text = item?.jobTitle
            itemView.txtBrandName?.text = item?.brandName ?: item?.company
            itemView.txtAppliedCount?.text = item?.count?.toString() + " Applied"

            if (mSelectedItem == absoluteAdapterPosition) {
                itemView?.imgDropDown?.setImageResource(R.drawable.ic_drop_up)
                itemView?.reclerChild?.visibility = View.VISIBLE
            }

            itemView.constraintRoot?.setOnClickListener {
                if (mSelectedItem == absoluteAdapterPosition) {
                    mSelectedItem = -1
                } else {
                    val previousItem = mSelectedItem
                    mSelectedItem = absoluteAdapterPosition
                    notifyItemChanged(previousItem)
                }
                notifyItemChanged(absoluteAdapterPosition)
                onExpandClick(item, absoluteAdapterPosition)
            }
        }
    }
}