package com.openkey.guest.application

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
//import com.downloader.PRDownloader
import com.facebook.appevents.AppEventsLogger
import com.google.firebase.analytics.FirebaseAnalytics
import com.okaygo.worker.cognito.AuthHelper
import com.openkey.guest.help.Preferences

/**
 * @author Davinder Goel.
 */
class OkayGo : MultiDexApplication() {

//
//    fun getFirebaseAnalytics(): FirebaseAnalytics? {
//        return firebaseAnalytics
//    }
//
//    fun getFbLogger(): AppEventsLogger? {
//        return fbLogger
//    }

    override fun onCreate() {
        super.onCreate()
        appContext = this
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        fbLogger = AppEventsLogger.newLogger(this)
//        PRDownloader.initialize(this)

        Preferences.initPreferences(this)
        AuthHelper.init(appContext)
//        Stetho.initializeWithDefaults(this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    companion object {
        lateinit var appContext: Application
        var firebaseAnalytics: FirebaseAnalytics? = null
        var fbLogger: AppEventsLogger? = null
    }

    override fun onTerminate() {
        super.onTerminate()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
    }

    override fun onLowMemory() {
        super.onLowMemory()
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
    }
}