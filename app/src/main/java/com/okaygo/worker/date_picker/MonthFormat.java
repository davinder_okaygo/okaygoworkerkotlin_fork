package com.okaygo.worker.date_picker;

public enum MonthFormat {
    /**
     * Gets short month strings. For example: "Jan", "Feb", etc.
     */
    SHORT,
    /**
     * Gets month strings. For example: "January", "February", etc.
     */
    LONG
}
