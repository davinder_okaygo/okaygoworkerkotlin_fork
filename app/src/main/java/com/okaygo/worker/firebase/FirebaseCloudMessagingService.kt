package com.okaygo.worker.firebase

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.freshchat.consumer.sdk.Freshchat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.okaygo.worker.R
import com.okaygo.worker.ui.activity.dashboard.DashBoardActivity

class FirebaseCloudMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.d("Remote Message", remoteMessage.notification?.body)
        val REQUEST_CODE = System.currentTimeMillis().toInt()
        if (Freshchat.isFreshchatNotification(remoteMessage)) {
            Freshchat.handleFcmMessage(applicationContext, remoteMessage)
        } else {
            val click_action = remoteMessage.notification?.clickAction
            val title = remoteMessage.notification?.title
            val message = remoteMessage.notification?.body
            val notificationIntent = Intent(this, DashBoardActivity::class.java)
            notificationIntent.putExtra("source", 1)
            notificationIntent.flags =
                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            val mNotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val activityPendingIntent = PendingIntent.getActivity(
                this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_ONE_SHOT
            )
            val builder =
                NotificationCompat.Builder(
                    this,
                    CHANNEL_ID
                )
                    .setContentIntent(activityPendingIntent)
                    .setContentText(message)
                    .setContentTitle(title)
                    .setOngoing(false)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setWhen(System.currentTimeMillis())
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                builder.setChannelId(CHANNEL_ID) // Channel ID
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val name: CharSequence = getString(R.string.app_name)
                // Create the channel for the notification
                val mChannel = NotificationChannel(
                    CHANNEL_ID,
                    name,
                    NotificationManager.IMPORTANCE_DEFAULT
                )

                // Set the Notification Channel for the Notification Manager.
                mNotificationManager.createNotificationChannel(mChannel)
            }
            val Notification_id = System.currentTimeMillis().toInt()
            mNotificationManager.notify(Notification_id, builder.build())
        }
    }

    override fun onNewToken(s: String) {
        super.onNewToken(s)
    }

    companion object {
        private const val CHANNEL_ID = "channel_03"
    }
}